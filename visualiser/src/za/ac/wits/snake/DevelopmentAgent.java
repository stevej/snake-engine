package za.ac.wits.snake;


import za.ac.wits.snake.agent.Agent;
import za.ac.wits.snake.agent.SnakeAgent;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Steve James on 07-Apr-16.
 */
public abstract class DevelopmentAgent extends SnakeAgent implements Runnable {

    private PipedOutputStream pipeToAgent;
    private PipedInputStream pipeFromAgent;
    private BufferedReader br;
    private static int numObjects = 0;

    private Thread thread;
    private boolean isBroken = false;

    protected static void start(DevelopmentAgent agent, String[] args) {

        //hack for singleton
        if (numObjects > 0) {
            throw new IllegalStateException("Can only have one development agent at a time");
        }
        ++numObjects;

        if (args.length > 0 && args[0].equals("-develop")) {
            try {
                agent.pipeToAgent = new PipedOutputStream();
                PipedInputStream input = new PipedInputStream();
                System.setIn(input);
                agent.pipeToAgent.connect(input);

                agent.pipeFromAgent = new PipedInputStream();
                PipedOutputStream output = new PipedOutputStream();
                System.setOut(new PrintStream(output));
                agent.pipeFromAgent.connect(output);
                agent.br = new BufferedReader(new InputStreamReader(agent.pipeFromAgent));
            } catch (IOException e) {
                e.printStackTrace();
            }

            String[] temp = new String[args.length - 1];
            System.arraycopy(args, 1, temp, 0, temp.length);
            List<Agent> agents = new ArrayList<>();
            agents.add(agent);
            SnakeVisualiser visualiser = new SnakeVisualiser(agents, temp);
            visualiser.start();
        } else {
            agent.run();
        }

    }

    public DevelopmentAgent() {
        super(-1, "DevelopmentAgent");
    }

    @Override
    public final void startEpisode(int gameId, String input) {
        super.startEpisode(gameId, input);
        try {
            pipeToAgent.write(input.getBytes());
            pipeToAgent.flush();
            thread = new Thread(this);
            thread.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public final void acceptState(String input) {
        super.acceptState(input);
        try {
            pipeToAgent.write(input.getBytes());
            pipeToAgent.flush();
        } catch (IOException e) {
            if (!isBroken) {
                //the agent has crashed. No need to spit out this error more that once!
                e.printStackTrace();
            }
            isBroken = true;
        }
    }

    @Override
    public final Direction getMove() {
        try {
            String output = getContent();
            String move = getMove(output.split("\n"));
            Direction dir = Direction.fromString(move);
            return dir == Direction.NONE ? Direction.STRAIGHT : dir;
        } catch (IOException e) {
            e.printStackTrace();
            return Direction.NONE;
        }
    }

    private String getMove(String[] lines) {
        String move = "-1";
        for (String line : lines) {
            line = line.trim();
            if (line.startsWith("log")) {
                System.err.println(line.substring(3));
                continue;
            }
            move = line;
        }
        return move;
    }

    private String getContent() throws IOException {
        StringBuilder sb = new StringBuilder();
        while (br.ready()) {
            int c = br.read();
            if (c == -1) {
                break;
            }
            sb.append((char) c);
        }
        return sb.toString();

    }

    @Override
    public final void endEpisode() {
        thread.interrupt();
        thread.stop();
    }

    @Override
    public final String toString() {
        return super.toString();
    }

    @Override
    public final int getGameId() {
        return super.getGameId();
    }

    @Override
    public final boolean isAlive() {
        return super.isAlive();
    }

    @Override
    public final int getKills() {
        return super.getKills();
    }

    @Override
    public final int getId() {
        return super.getId();
    }

    @Override
    public final double getWeightedScore() {
        return super.getWeightedScore();
    }

    @Override
    public final double getEloRating() {
        return super.getEloRating();
    }

    @Override
    public final void incrementScore(double score) {
        super.incrementScore(score);
    }

    @Override
    public final void setGameId(int id) {
        super.setGameId(id);
    }

    @Override
    public final void setRank(int rank) {
        super.setRank(rank);
    }

    @Override
    public final int getRank() {
        return super.getRank();
    }

    @Override
    public final int getLongest() {
        return super.getLongest();
    }

    @Override
    public final void setLongest(int longest) {
        super.setLongest(longest);
    }

    @Override
    public final String getLogs() {
        return super.getLogs();
    }

    @Override
    public final String getErrorLogs() {
        return super.getErrorLogs();
    }

    @Override
    public final int compareTo(SnakeAgent other) {
        return super.compareTo(other);
    }

    @Override
    public final void updateElo(double result, SnakeAgent opponent) {
        super.updateElo(result, opponent);
    }
}
