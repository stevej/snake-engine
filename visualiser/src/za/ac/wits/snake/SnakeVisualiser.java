package za.ac.wits.snake;

import za.ac.wits.snake.agent.Agent;
import za.ac.wits.snake.agent.ExternalAgent;
import za.ac.wits.snake.agent.HumanAgent;
import za.ac.wits.snake.config.Config;
import za.ac.wits.snake.config.LocalConfig;

import javax.swing.*;
import javax.swing.Timer;
import java.awt.*;
import java.io.File;
import java.util.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.awt.Color.WHITE;

/**
 * Created by Dean Wookey on 07-Apr-12.
 */
public class SnakeVisualiser extends JFrame {

    private static final int SCREEN_WIDTH = 540;
    private static final int SCREEN_HEIGHT = 565;
    private static final int X_OFFSET = 10;
    private static final int Y_OFFSET = 10;

    private final SnakeGame game;

    private GamePane pane;

    private Parser parser;

    private String[] standings;
    private String previousStandings;

    private static final Color[] COLOURS = {
            Color.RED,
            Color.GREEN,
            Color.BLUE,
            Color.ORANGE,
            Color.PINK,
            Color.MAGENTA,
            Color.CYAN,
            Color.YELLOW
    };

    public SnakeVisualiser(List<Agent> agents, SnakeGame game) {
        if (agents.size() <= 1) {
            throw new IllegalArgumentException("Must have at least 2 agents!");
        }
        int N = LocalConfig.getInstance().getSnakes();
        if (agents.size() > N) {
            throw new IllegalArgumentException("Expected at most " + N + " agents, but received " + agents.size());
        }
        this.game = game;
        initComponents(agents);
    }

    public SnakeVisualiser(List<Agent> agents, String... args) {
        if (args.length % 2 != 0) {
            System.err.println("Invalid number of parameters.");
            game = null;
            return;
        }
        parseArgs(agents, args);
        int N = LocalConfig.getInstance().getSnakes();
        if (agents.size() > N) {
            throw new IllegalArgumentException("Expected at most " + N + " agents, but received " + agents.size());
        }
        if (N <= 1) {
            throw new IllegalArgumentException("Must have at least 2 agents!");
        }
        switch (LocalConfig.getInstance().getGameMode()) {
            case GROW:
                game = new SnakeGameImpl(1, agents);
                break;
            case SURVIVE:
                game = new TronGame(1, agents);
                break;
            default:
                throw new UnsupportedOperationException("Mode " + LocalConfig.getInstance().getGameMode() + " not yet implemented");
        }
        initComponents(agents);
    }

    private void parseArgs(List<Agent> agents, String... args) {
        int agentNum = 0;
        for (int i = 0; i < args.length - 1; i = i + 2) {
            switch (args[i].toLowerCase()) {
                case "-human":
                case "-h": {
                    String keys = args[i + 1];
                    if (keys.equalsIgnoreCase("normal")) {
                        agents.add(new HumanAgent(agentNum, "Human " + agentNum, "normal"));
                        agentNum++;
                    } else if (keys.length() == 4) {
                        agents.add(new HumanAgent(agentNum, "Human " + agentNum, keys.toLowerCase()));
                        agentNum++;
                    } else {
                        throw new IllegalArgumentException("Must specify if the human player is using 'normal' arrow keys or specify up/down/left/right keys.");
                    }
                    break;
                }
                case "-java":
                case "-j": {
                    File file = new File(args[i + 1]);
                    file = file.getAbsoluteFile();
                    if (!file.isFile()) {
                        throw new IllegalArgumentException(args[i + 1] + "is not a valid file location.");
                    }
                    String filename = file.getName();
                    String path = file.getParentFile().getPath();

                    String name = "java_" + agentNum;
                    agents.add(ExternalAgent.load(filename + "%" + path + "%" + "1" + "%" + "java~-jar~" + path + "/" + filename + "*0>" + name + ">0>0>0>0", 0));
                    agentNum++;
                    break;
                }
                case "-python":
                case "-p": {
                    File file = new File(args[i + 1]);
                    file = file.getAbsoluteFile();
                    if (!file.isFile()) {
                        throw new IllegalArgumentException(args[i + 1] + "is not a valid file location.");
                    }
                    String filename = file.getName();
                    String path = file.getParentFile().getPath();

                    String name = "python_" + agentNum;
                    agents.add(ExternalAgent.load(filename + "%" + path + "%" + "2" + "%" + "python~-u~" + path + "/" + filename + "*0>" + name + ">0>0>0>0", 0));
                    agentNum++;
                    break;
                }
                case "-executable":
                case "-e": {
                    File file = new File(args[i + 1]);
                    file = file.getAbsoluteFile();
                    if (!file.isFile()) {
                        System.err.println(args[i + 1] + "is not a valid file location.");
                        return;
                    }
                    String filename = file.getName();
                    String path = file.getParentFile().getPath();
                    String name = "executable_" + agentNum;
                    boolean isWindows = System.getProperty("os.name").toLowerCase().contains("win");
                    String command = isWindows ? "" : "./";
                    agents.add(ExternalAgent.load(filename + "%" + path + "%" + "2" + "%" + command + path + "/" + filename + "*0>" + name + ">0>0>0>0", 0));
                    agentNum++;
                    break;
                }
            }
        }
    }

    private void initComponents(List<Agent> agents) {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pane = new GamePane();
        agents.stream()
                .filter(agent -> agent != null && agent.getClass() == HumanAgent.class)
                .map(agent -> (HumanAgent) agent)
                .forEach(human -> human.setupKeyBindings(pane));

        setContentPane(pane);
        pack();
    }

    public void start() {

        new Thread(game).start();
        this.setSize(SCREEN_WIDTH + 200, SCREEN_HEIGHT);
        this.setVisible(true);
        while (!game.isRunning()) {
            try {
                //game is still initialising
                Thread.sleep(20);
            } catch (InterruptedException ex) {
                Logger.getLogger(SnakeVisualiser.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        Config config = LocalConfig.getInstance();
        parser = new Parser();
        String initString = String.format("%d %d %d %d\n", config.getSnakes(), config.getGameWidth(), config.getGameHeight(), config.getGameMode().ordinal());
        Obstacle[] staticObstacles = game.getObstacles();
        if (staticObstacles.length > 0) {
            StringBuilder sb = new StringBuilder();
            for (Obstacle o : staticObstacles) {
                sb.append(o).append("\n");
            }
            initString += sb.toString();
        }
        parser.setInitString(initString);
        final int refreshRate = config.getGameSpeed();

        String os = System.getProperty("os.name");
        final boolean mustSync = os.contains("nux");
        final Timer gameLoop = new Timer(refreshRate, e -> {
            if (!game.isRunning()) {
                ((Timer) e.getSource()).stop();
                return;
            }
            String state = game.getNextState();
            if (state == null || state.equals("Game Starting")) {
                return;
            }
            parser.setStateString(state);
            String[] appleStrings = parser.getApples();
            Point[] apples = new Point[appleStrings.length];
            for (int i = 0; i < apples.length; i++) {
                apples[i] = readApple(appleStrings[i]);
            }
            String[] snakeStrings = parser.getSnakes();
            SimpleSnake snakes[] = new SimpleSnake[snakeStrings.length];
            for (int i = 0; i < snakes.length; i++) {
                snakes[i] = readSnake(snakeStrings[i]);
            }
            String[] obstacleStrings = parser.getObstacles();
            SimpleSnake obstacles[] = new SimpleSnake[obstacleStrings.length];
            for (int i = 0; i < obstacles.length; i++) {
                obstacles[i] = readObstacle(obstacleStrings[i]);
            }
            String[] zombieStrings = parser.getZombies();
            SimpleSnake zombies[] = new SimpleSnake[zombieStrings.length];
            for (int i = 0; i < zombies.length; i++) {
                zombies[i] = readZombies(zombieStrings[i]);
            }
            pane.setParams(config.getGameWidth(), config.getGameHeight(), apples, snakes, obstacles, zombies);
            repaint();
            if (mustSync) {
                Toolkit.getDefaultToolkit().sync();
            }
        });

        gameLoop.start();
    }

    private SimpleSnake readObstacle(String line) {
        String split[] = line.split(" ");
        ArrayList<Point> obsBody = new ArrayList<Point>();
        for (int i = 0; i < split.length; i++) {
            String sp[] = split[i].split(",");
            obsBody.add(new Point(Integer.parseInt(sp[0]), Integer.parseInt(sp[1])));
        }
        return new SimpleSnake(obsBody, 5, 0, true, false);
    }

    private SimpleSnake readZombies(String line) {
        String split[] = line.split(" ");
        ArrayList<Point> obsBody = new ArrayList<Point>();
        for (int i = 0; i < split.length; i++) {
            String sp[] = split[i].split(",");
            obsBody.add(new Point(Integer.parseInt(sp[0]), Integer.parseInt(sp[1])));
        }
        return new SimpleSnake(obsBody, 5, 0, true, false);
    }

    private Point readApple(String line) {
        String[] temp = line.split(" ");
        int x = Integer.parseInt(temp[0]);
        int y = Integer.parseInt(temp[1]);
        return new Point(x, y);
    }

    private SimpleSnake readSnake(String snakeLine) {
        String split[] = snakeLine.split(" ");
        String snakeStatus = split[3];
        int snakeLength = Integer.parseInt(split[4]);
        int snakeKills = Integer.parseInt(split[5]);
        ArrayList<Point> snakeBody = new ArrayList<Point>();
        int start = 6;
        if (snakeStatus.equals("invisible")) {
            start +=2;
        }
        for (int j = start; j < split.length; j++) {
            String pts[] = split[j].split(",");
            snakeBody.add(new Point(Integer.parseInt(pts[0]), Integer.parseInt(pts[1])));
        }
        boolean alive = false;
        boolean invisible = false;
        if (snakeStatus.equals("alive")) {
            alive = true;
        } else if (snakeStatus.equals("invisible")) {
            alive = true;
            invisible = true;
        }
        return new SimpleSnake(snakeBody, snakeLength, snakeKills, alive, invisible);
    }

    private class GamePane extends JPanel {

        int width;
        int height;
        Point[] apples;
        SimpleSnake[] snakes;
        SimpleSnake[] obstacles;
        SimpleSnake[] zombies;

        @Override
        protected void paintComponent(Graphics buffer) {
            super.paintComponent(buffer);
            setBackground(Color.BLACK);
            if (width <= 0) {
                return;
            }
            int ratioX = 500 / width;
            int ratioY = 500 / height;
            buffer.setColor(Color.GRAY);
            buffer.fillRect(X_OFFSET, Y_OFFSET, 500, 500);

            buffer.setColor(new Color(0, 0, 255));
            if (apples.length > 0) {
                Point superApple = apples[0];
                buffer.fillOval(superApple.x * ratioX + X_OFFSET, superApple.y * ratioY + Y_OFFSET, ratioX, ratioY);
            }
            buffer.setColor(new Color(255, 0, 0));
            for (int i = 1; i < apples.length; i++) {
                Point apple = apples[i];
                buffer.fillOval(apple.x * ratioX + X_OFFSET, apple.y * ratioY + Y_OFFSET, ratioX, ratioY);
            }

            for (int i = 0; i < snakes.length; i++) {
                if (snakes[i].power) {
                    drawSnake(buffer, snakes[i], WHITE);
                } else if (snakes[i].alive) {
                    drawSnake(buffer, snakes[i], COLOURS[i % COLOURS.length]);
                }
            }
            for (int i = 0; i < obstacles.length; i++) {
                drawSnake(buffer, obstacles[i], Color.DARK_GRAY);
            }

            for (int i = 0; i < zombies.length; i++) {
                drawSnake(buffer, zombies[i], Color.BLACK);
            }
            standings = getStandings();
            if (standings != null) {
                int y = Y_OFFSET + 30;
                int i = 0;
                for (String line : standings) {
                    buffer.setColor(COLOURS[i % COLOURS.length]);
                    buffer.drawString(line, SCREEN_WIDTH, y);
                    y += 20;
                    i++;
                }
                buffer.setColor(Color.WHITE);
                buffer.setFont(new Font(Font.MONOSPACED, Font.BOLD, 12));
                buffer.drawString("Standings (longest/kills)", SCREEN_WIDTH, Y_OFFSET + 10);
            }
            //    buffer.dispose();
        }

        void setParams(int width, int height, Point[] apples, SimpleSnake[] snakes, SimpleSnake[] obstacles, SimpleSnake[] zombies) {
            this.width = width;
            this.height = height;
            this.apples = apples;
            this.snakes = snakes;
            this.obstacles = obstacles;
            this.zombies = zombies;
        }

        private String[] getStandings() {

            String str = game.getStandings();
            if (str == null) {
                return null;
            }
            if (str.equals(previousStandings)) {
                return standings;
            }
            previousStandings = str;

            String[] arr = str.split("\n");
            String[] ans = new String[arr.length - 1];
            System.arraycopy(arr, 1, ans, 0, ans.length);

            Arrays.sort(ans, (o1, o2) -> {
                int x1 = Integer.parseInt(o1.split(" ")[0]);
                int y1 = Integer.parseInt(o2.split(" ")[0]);
                return Integer.compare(x1, y1);
            });

            for (int i = 0; i < ans.length; i++) {
                String[] agent = ans[i].split(" ");
                String name = agent[1].isEmpty() ? "BuiltIn" : agent[1];
                ans[i] = name + ": " + agent[3] + "/" + agent[4];
            }
            return standings = ans;
        }

        private void drawHalo(Graphics buffer, SimpleSnake s) {
            int ratioX = 500 / LocalConfig.getInstance().getGameWidth();
            int ratioY = 500 / LocalConfig.getInstance().getGameHeight();
            for (int i = 1; i < s.body.size(); i++) {
                Point prev = s.body.get(i - 1);
                Point curr = s.body.get(i);
                int topLeftX;
                int topLeftY;
                if (prev.x < curr.x) {
                    topLeftX = prev.x;
                    topLeftY = prev.y;
                } else if (prev.x == curr.x) {
                    if (prev.y < curr.y) {
                        topLeftX = prev.x;
                        topLeftY = prev.y;
                    } else {
                        topLeftX = curr.x;
                        topLeftY = curr.y;
                    }
                } else {
                    topLeftX = curr.x;
                    topLeftY = curr.y;
                }
                int lengthX = Math.abs(prev.x - curr.x) + 1;
                int lengthY = Math.abs(prev.y - curr.y) + 1;
                int x1 = topLeftX * ratioX;
                int y1 = topLeftY * ratioY;
                int x2 = lengthX * ratioX;
                int y2 = lengthY * ratioY;
                buffer.setColor(Color.WHITE);
                buffer.fillRect(x1 + X_OFFSET - 2, y1 + Y_OFFSET - 2, x2 + 4, y2 + 4);
                buffer.setColor(Color.CYAN);
                buffer.fillRect(x1 + X_OFFSET - 1, y1 + Y_OFFSET - 1, x2 + 2, y2 + 2);
            }
        }

        public void drawSnake(Graphics buffer, SimpleSnake s, Color colour) {
            int ratioX = 500 / width;
            int ratioY = 500 / height;
            if (s.power) {
                drawHalo(buffer, s);
            }
            for (int i = 1; i < s.body.size(); i++) {
                Point prev = s.body.get(i - 1);
                Point curr = s.body.get(i);
                int topLeftX;
                int topLeftY;
                if (prev.x < curr.x) {
                    topLeftX = prev.x;
                    topLeftY = prev.y;
                } else if (prev.x == curr.x) {
                    if (prev.y < curr.y) {
                        topLeftX = prev.x;
                        topLeftY = prev.y;
                    } else {
                        topLeftX = curr.x;
                        topLeftY = curr.y;
                    }
                } else {
                    topLeftX = curr.x;
                    topLeftY = curr.y;
                }
                int lengthX = Math.abs(prev.x - curr.x) + 1;
                int lengthY = Math.abs(prev.y - curr.y) + 1;
                int x1 = topLeftX * ratioX;
                int y1 = topLeftY * ratioY;
                int x2 = lengthX * ratioX;
                int y2 = lengthY * ratioY;

                buffer.setColor(colour);
                buffer.fillRect(x1 + X_OFFSET, y1 + Y_OFFSET, x2, y2);
            }
        }
    }

}

class SimpleSnake {

    ArrayList<Point> body;
    int length;
    int kills;
    boolean alive;
    boolean power;

    public SimpleSnake(ArrayList<Point> body, int length, int kills, boolean alive, boolean power) {
        this.body = body;
        this.length = length;
        this.kills = kills;
        this.alive = alive;
        this.power = power;
    }
}
