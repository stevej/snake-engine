package za.ac.wits.snake;

import za.ac.wits.snake.agent.Agent;
import za.ac.wits.snake.agent.ExternalAgent;
import za.ac.wits.snake.agent.HumanAgent;
import za.ac.wits.snake.config.LocalConfig;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Steve James on 08-Apr-16.
 */
public class Main {

    public static void main(String[] args) {

        if (args.length % 2 != 0) {
            System.err.println("Invalid number of parameters.");
            return;
        }
        List<Agent> agentList = new ArrayList<>(4);
        int agentNum = 0;
        for (int i = 0; i < args.length - 1; i = i + 2) {
            switch (args[i].toLowerCase()) {
                case "-human":
                case "-h": {
                    String keys = args[i + 1];
                    if (keys.equalsIgnoreCase("normal")) {
                        agentList.add(new HumanAgent(agentNum, "Human " + agentNum, "normal"));
                        agentNum++;
                    } else if (keys.length() == 4) {
                        agentList.add(new HumanAgent(agentNum, "Human " + agentNum, keys.toLowerCase()));
                        agentNum++;
                    } else {
                        System.err.println("Must specify if the human player is using 'normal' arrow keys or specify up/down/left/right keys.");
                        return;
                    }
                    break;
                }
                case "-java":
                case "-j": {
                    File file = new File(args[i + 1]);
                    file = file.getAbsoluteFile();
                    if (!file.isFile()) {
                        System.err.println(args[i + 1] + "is not a valid file location.");
                        return;
                    }
                    String filename = file.getName();
                    String path = file.getParentFile().getPath();
                    String name = "java_" + agentNum;
                    agentList.add(ExternalAgent.load(filename + "%" + path + "%" + "1" + "%" + "java~-jar~" + path + "/" + filename + "*0>" + name + ">0>0>0>0", 0));
                    agentNum++;
                    break;
                }
                case "-python":
                case "-p": {
                    File file = new File(args[i + 1]);
                    file = file.getAbsoluteFile();
                    if (!file.isFile()) {
                        System.err.println(args[i + 1] + "is not a valid file location.");
                        return;
                    }
                    String filename = file.getName();
                    String path = file.getParentFile().getPath();
                    String name = "python_" + agentNum;
                    agentList.add(ExternalAgent.load(filename + "%" + path + "%" + "2" + "%" + "python~-u~" + path + "/" + filename + "*0>" + name + ">0>0>0>0", 0));
                    agentNum++;
                    break;
                }
                case "-executable":
                case "-e": {
                    File file = new File(args[i + 1]);
                    file = file.getAbsoluteFile();
                    if (!file.isFile()) {
                        System.err.println(args[i + 1] + "is not a valid file location.");
                        return;
                    }
                    String filename = file.getName();
                    String path = file.getParentFile().getPath();
                    String name = "executable_" + agentNum;
                    boolean isWindows = System.getProperty("os.name").toLowerCase().contains("win");
                    String command = isWindows ? "" : "./";
                    agentList.add(ExternalAgent.load(filename + "%" + path + "%" + "2" + "%" + command + path + "/" + filename + "*0>" + name + ">0>0>0>0", 0));
                    agentNum++;
                    break;
                }
            }
        }

        SnakeGame game;
        switch (LocalConfig.getInstance().getGameMode()) {
            case GROW:
                game = new SnakeGameImpl(1, agentList);
                break;
            case SURVIVE:
                game = new TronGame(1, agentList);
                break;
            default:
                throw new UnsupportedOperationException("Mode " + LocalConfig.getInstance().getGameMode() + " not yet implemented");
        }

        SnakeVisualiser snakeVisualiser = new SnakeVisualiser(agentList, game);
        snakeVisualiser.start();
    }


}
