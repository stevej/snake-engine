package za.ac.wits.snake;


/**
 * Created by Steve James on 06-Apr-16.
 */
public class Grid {


    public static final int EMPTY = 0;
    public static final int OBSTACLE = -1;
    public static final int APPLE = -2;
    public static final int ZOMBIE = -3;

    private final int[][] grid;
    private final int width;
    private final int height;

    public Grid(int width, int height) {
        this.width = width;
        this.height = height;
        grid = new int[height][width];
    }

    public void clearSnakes() {
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                if (grid[y][x] > EMPTY) {
                    grid[y][x] = EMPTY;
                }
            }
        }
    }

    public void clearZombies() {
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                if (grid[y][x] == ZOMBIE) {
                    grid[y][x] = EMPTY;
                }
            }
        }
    }

    public void reset() {
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                grid[y][x] = EMPTY;
            }
        }
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int at(final Point point) {
        return at(point.x, point.y);
    }

    public int at(int x, int y) {
        return grid[y][x];
    }

    public void set(int x, int y, int value) {
        grid[y][x] = value;
    }

    public void set(final Point point, int value) {
        set(point.x, point.y, value);
    }

    public boolean contains(final Point point) {
        return point.x >= 0 && point.x < width &&
                point.y >= 0 && point.y < height;
    }

    public boolean add(final Apple apple) {
        if (at(apple.getX(), apple.getY()) != EMPTY) {
            return false;
        }
        grid[apple.getY()][apple.getX()] = APPLE;
        return true;
    }

    public boolean add(final Snake snake) {
        for (final Point p : snake) {
            if (at(p) != EMPTY) {
                return false;
            }
        }
        for (final Point p : snake) {
            set(p.x, p.y, snake.getId() + 1);
        }
        return true;
    }

    public boolean add(final Obstacle obstacle) {
        for (final Point p : obstacle) {
            if (at(p) != EMPTY) {
                return false;
            }
        }
        for (final Point p : obstacle) {
            set(p.x, p.y, Grid.OBSTACLE);
        }
        return true;
    }

    public boolean add(final ZombieSnake zombie) {
        for (final Point p : zombie) {
            if (at(p) != EMPTY) {
                return false;
            }
        }
        for (final Point p : zombie) {
            set(p.x, p.y, ZOMBIE);
        }
        return true;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                switch (grid[i][j]) {
                    case EMPTY:
                        sb.append('.');
                        break;
                    case OBSTACLE:
                        sb.append('x');
                        break;
                    case APPLE:
                        sb.append('a');
                        break;
                    case ZOMBIE:
                        sb.append('#');
                        break;
                    default:
                        sb.append(grid[i][j]);
                        break;
                }
                sb.append(' ');
            }
            sb.append('\n');
        }
        return sb.toString();
    }
}