package za.ac.wits.snake;

/**
 * Created by Steve James on 06-May-16.
 */
public abstract class SpecialApple extends Apple {

    private int remainingTime;
    private final int spawnWait;

    public SpecialApple(int timeLimit, int spawnWait) {
        super(-1, -1, timeLimit);
        this.spawnWait = spawnWait;
        remainingTime = spawnWait;
    }

    @Override
    public void reset(int x, int y) {
        super.reset(x, y);
        remainingTime = spawnWait;
    }

    @Override
    public void advanceTimeStep() {
        if (isWaiting()) {
            remainingTime = Math.max(remainingTime - 1, 0);
        }
        super.advanceTimeStep();
    }

    public boolean isReady() {
        return remainingTime == 0 && isWaiting();
    }

    private boolean isWaiting() {
        return getX() == -1 && getY() == -1;
    }


    /**
     * Define how the instance of the special apple works here, if any
     * @param snakeId the ID of the snake that consumed the special apple
     * @param game the game in progress
     */
    protected abstract void affectGame(int snakeId, final Game game);

}
