package za.ac.wits.snake;

import za.ac.wits.snake.agent.Agent;

import java.util.List;

/**
 * Created by Steve James on 13-May-16.
 */
public interface SnakeGame extends Runnable {

    String getNextState();

    boolean isRunning();

    void updateStandings();

    String getStandings();

    List<Agent> getAgents();

    Obstacle[] getObstacles();

}
