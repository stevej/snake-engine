package za.ac.wits.snake;

import za.ac.wits.snake.agent.Agent;
import za.ac.wits.snake.agent.SnakeAgent;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Steve James on 20-Jun-17.
 */
class TronGame extends SnakeGameImpl {

    private int currentPlayer = 0;

    public TronGame(int id, List<Agent> agents) {
        super(id, agents);
    }

    @Override
    protected boolean isPrematureWin() {
        int nAliveSnakes = 0;
        int id = -1;
        for (Snake snake : snakes) {
            if (snake.isAlive()) {
                ++nAliveSnakes;
                id = snake.getId();
            }
        }
        if (nAliveSnakes == 1) {
            updateFinalFrame(id);
            return true;
        }
        return false;
    }

    private void updateFinalFrame(int winnerId) {
        SnakeAgent agent = (SnakeAgent) agents.get(winnerId);
        agent.setLongest(config.getGameWidth() * config.getGameHeight());
        ++timeSteps;
        updateState();
        try {
            Thread.sleep(config.getGameSpeed());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    /*Modify this method if you want to change the starting positions of the agents/obstacles*/
    @Override
    protected Snake generateRandomSnake(int id) {

        switch (id) {
            case Grid.ZOMBIE:
                //generate a random zombie
                return super.generateRandomSnake(id);
            case Grid.OBSTACLE:
                //generate a random obstacle
                return super.generateRandomSnake(id);
            default:
                //generate a random starting position for snake #id:
                //change this if you want specific positions
                Snake snake = super.generateRandomSnake(id);
                //turn the snake into a Tron agent
                return new TronSnake(snake);
        }
    }


        /*The methods below make the game alternate between players. Comment them out if you want simultaneous*/

    protected void provideInput(int numSnakes) {
        if (snakes[currentPlayer].isAlive()) {
            //todo send status non-blocking
            agents.get(currentPlayer).acceptState(buildState(currentPlayer));
        }
    }


    protected Direction[] getActions(int numSnakes) {
        Direction[] actions = new Direction[numSnakes];
        Arrays.fill(actions, Direction.FROZEN);
        actions[currentPlayer] = getAction(currentPlayer);
        return actions;
    }


    @Override
    protected void updateState() {
        super.updateState();
        if (timeSteps == 0) {
            currentPlayer = 0; //new game
        } else {
            currentPlayer = (currentPlayer + 1) % snakes.length;
        }
    }

    @Override
    void checkSnakeCollisions(final boolean[] markDead) {
        super.checkSnakeCollisions(markDead);
        for (int i = 0; i < snakes.length; ++i) {
            if (i != currentPlayer) {
                markDead[i] = false; //snakes whose turn it isn't can't possibly be killed
            }
        }
    }

    /*The methods above make the game alternate between players. Comment them out if you want simultaneous*/

}
