package za.ac.wits.snake;

import java.util.*;

/**
 * Created by Steve James on 06-Apr-16.
 */
public class Snake implements Iterable<Point> {


    private int timeOfGrowthLeft;
    private final int id;
    private boolean alive;
    private int length;
    private int kills;
    private int deaths;
    private int timeToRespawn;
    private boolean invisible;
    private int invisibilityTimeLeft;
    private int timeSinceInvisible;
    private Point invisiblePoint;

    private ArrayDeque<Point> orderedPoints;
    private Set<Point> unorderedPoints;


    public Snake(final Point head, final Point tail, final int id) {
        this.id = id;
        unorderedPoints = new HashSet<>(100);
        orderedPoints = new ArrayDeque<>(100);
        addSegment(head, tail);
        length = unorderedPoints.size();
        invisible = false;
        invisibilityTimeLeft = 0;
    }

    public void addKill() {
        ++kills;
    }

    public int getId() {
        return id;
    }

    public void setPoints(Point... points) {

        unorderedPoints.clear();
        orderedPoints.clear();
        for (int i = 0; i < points.length - 1; ++i) {
            addSegment(points[i], points[i + 1]);
        }

        length = unorderedPoints.size();
        assert (orderedPoints.size() == unorderedPoints.size());


    }

//    @Override
//    public String toString() {
//
//        String status = isInvisible() ? "invisible" : isAlive() ? "alive" : "dead";
//
//        StringBuilder sb = new StringBuilder(String.format("%s %d %d", status, length, kills));
//        if (isAlive()) {
//            Point[] points = new Point[length];
//            Direction[] directions = new Direction[length];
//            boolean[] vertices = new boolean[length];
//            int i = 0;
//            for (Point p : this) {
//                points[i] = p;
//                if (i == 0) {
//                    vertices[i] = true;
//                } else {
//                    directions[i] = Direction.directionTo(p, points[i - 1]);
//                    vertices[i - 1] = (directions[i] != directions[i - 1]);
//                }
//                ++i;
//            }
//            vertices[length - 1] = true;
//            for (int j = 0; j < vertices.length; ++j) {
//                if (vertices[j]) {
//                    sb.append(' ').append(points[j].x).append(',').append(points[j].y);
//                }
//            }
//        }
//        return sb.toString();
//    }

    @Override
    public String toString() {

        String status = isInvisible() ? "invisible" : isAlive() ? "alive" : "dead";

        StringBuilder sb;
        if (isInvisible()) {
            sb = new StringBuilder(String.format("%s %d %d %d %s", status, length, kills, timeSinceInvisible, invisiblePoint.x + "," + invisiblePoint.y));
        }
        else {
            sb = new StringBuilder(String.format("%s %d %d", status, length, kills));
        }
        if (isAlive()) {
            Point[] points = new Point[length];
            Direction[] directions = new Direction[length];
            boolean[] vertices = new boolean[length];
            int i = 0;
            for (Point p : this) {
                points[i] = p;
                if (i == 0) {
                    vertices[i] = true;
                } else {
                    directions[i] = Direction.directionTo(p, points[i - 1]);
                    vertices[i - 1] = (directions[i] != directions[i - 1]);
                }
                ++i;
            }
            vertices[length - 1] = true;
            for (int j = 0; j < vertices.length; ++j) {
                if (vertices[j]) {
                    sb.append(' ').append(points[j].x).append(',').append(points[j].y);
                }
            }
        }
        return sb.toString();
    }

    /**
     * Returns the snake's description with the known section visible.
     *
     * @return
     */
    public String getSemiVisible() {
        if (!isInvisible()) {
            return toString();
        }
        String status = isInvisible() ? "invisible" : isAlive() ? "alive" : "dead";

        StringBuilder sb = new StringBuilder(String.format("%s %d %d %d %s", status, length, kills, timeSinceInvisible, invisiblePoint.x + "," + invisiblePoint.y));
        if (isAlive()) {
            if (this.timeSinceInvisible < this.getLength()) { //part of the snake is still known information, for for example, a snake of length 2 passes through the apple. The head position is set to where the apple was and time is set to 0. Snake is fully visible. Next iteration time is 1 and length is 2, so 1 segment of the snake is visible. Next iteration time is 2, length is 2, so snake is now completely invisible. Should work even if the snake grows.
                Point[] points = new Point[length];
                Direction[] directions = new Direction[length];
                boolean[] vertices = new boolean[length];
                int i = 0;
                boolean foundCut = false;
                //need to account for special case where snake goes over the point where it ate the invisible apple again.
                for (Point p : this) {
                    points[i] = p;
                    if (p.equals(this.invisiblePoint)) {
                        vertices[i] = true; //gets set to true in the next iteration anyway because directions is null up to this point
                        foundCut = true;
                    } else {
                        if (foundCut) {
                            directions[i] = Direction.directionTo(p, points[i - 1]);
                            vertices[i - 1] = (directions[i] != directions[i - 1]);
                        }
                    }
                    ++i;
                }
                if (foundCut) { //if the point where it went invisible is still part of the snake, this should never not happen because of the time/length check
                    vertices[length - 1] = true;
                    for (int j = 0; j < vertices.length; ++j) {
                        if (vertices[j]) {
                            sb.append(' ').append(points[j].x).append(',').append(points[j].y);
                        }
                    }
                    if (points[length-1].equals(this.invisiblePoint)) {
                        //if the only visible part is a 1x1
                        sb.append(' ').append(this.invisiblePoint.x).append(',').append(this.invisiblePoint.y);
                    }
                }
            }
        }
        return sb.toString();
    }

    @Override
    public Iterator<Point> iterator() {
        return orderedPoints.iterator();
    }

    public void clear() {
        alive = false;
        invisible = false;
        invisibilityTimeLeft = 0;
        timeSinceInvisible = 0;
        invisiblePoint = null;
        length = 0;
        kills = 0;
        deaths = 0;
        timeOfGrowthLeft = 0;
        orderedPoints.clear();
        unorderedPoints.clear();
    }

    public void setDead() {
        alive = false;
    }

    public void kill() {
        assert (timeToRespawn == -1);
        assert (alive);
        invisible = false;
        alive = false;
        length = 0;
        timeOfGrowthLeft = 0;
        orderedPoints.clear();
        unorderedPoints.clear();
        ++deaths;
        timeToRespawn = 1;
        invisibilityTimeLeft = 0;
        timeSinceInvisible = 0;
    }

    public void setAlive() {
        alive = true;
        timeToRespawn = -1;
    }

    public Point getHead() {
        if (orderedPoints.isEmpty()) {
            return null;
        }
        return orderedPoints.getFirst();
    }

    public Collection<Point> getFullBody() {
        return orderedPoints;
    }

    public Point getTail() {
        if (orderedPoints.isEmpty()) {
            return null;
        }
        return orderedPoints.getLast();
    }

    public boolean isAlive() {
        return alive;
    }

    public boolean isInvisible() {
        return invisible;
    }

    public int getLength() {
        return length;
    }

    public int getKills() {
        return kills;
    }

    public boolean contains(final Point point) {
        return unorderedPoints.contains(point);
    }

    public boolean isGrowing() {
        return timeOfGrowthLeft > 0;
    }

    public void addAppleBonus(int appleGrowLength) {
        timeOfGrowthLeft += appleGrowLength;
    }

    public void advance() {
        assert !isAlive();
        --timeToRespawn;
    }

    public int getTimeToRespawn() {
        return timeToRespawn;
    }

    public void advance(final Point nextPoint) {
        if (isInvisible()) {
            if (invisibilityTimeLeft == 0) {
                invisible = false;
                timeSinceInvisible = 0;
            } else {
                invisibilityTimeLeft--;
                timeSinceInvisible++;
            }
        }

        if (isGrowing()) {
            --timeOfGrowthLeft;
        } else {
            Point tail = orderedPoints.pollLast();
            unorderedPoints.remove(tail);
        }
        orderedPoints.addFirst(nextPoint);
        unorderedPoints.add(nextPoint);
        length = unorderedPoints.size();
    }

    public Direction getDirection() {
        if (!isAlive() || getLength() == 1) {
            return Direction.NONE;
        }

        Iterator<Point> it = orderedPoints.iterator();
        return Direction.directionTo(it.next(), it.next());
    }

    protected boolean isConsistent() {
        if (length == unorderedPoints.size() && length == orderedPoints.size()) {
            Point prev = null;
            for (final Point point : orderedPoints) {
                if (!contains(point)) {
                    return false;
                }
                if (prev != null && prev.distSq(point) != 1) {
                    return false;
                }
                prev = point;
            }
            return true;
        }
        return false;
    }

    private List<Point> getPointsBetween(Point A, Point B) {
        assert (A.x == B.x) ^ (A.y == B.y);
        List<Point> points = new ArrayList<>();
        if (A.x == B.x) {
            for (int y = A.y; y <= B.y; ++y) {
                points.add(new Point(A.x, y));
            }
            for (int y = A.y; y >= B.y; --y) {
                points.add(new Point(A.x, y));
            }
        } else {
            for (int x = A.x; x <= B.x; ++x) {
                points.add(new Point(x, A.y));
            }
            for (int x = A.x; x >= B.x; --x) {
                points.add(new Point(x, A.y));
            }
        }
        return points;
    }

    private void addSegment(final Point A, final Point B) {
        List<Point> points = getPointsBetween(A, B);
        for (Point C : points) {
            if (!unorderedPoints.contains(C)) {
                orderedPoints.addLast(C);
                unorderedPoints.add(C);
            }
        }
        assert orderedPoints.size() == unorderedPoints.size();
    }

    /**
     * Cuts a snake at the given point. The snake dies if its subsequent length is less that two
     *
     * @param cutPoint the point at which to cut
     * @return whether the snake was killed by the cut
     */
    public boolean cut(Point cutPoint) {
        assert isAlive();
        List<Point> newPoints = new ArrayList<>(getLength());
        for (Point point : this) {
            if (point.equals(cutPoint)) {
                break;
            }
            newPoints.add(point);
        }
        if (newPoints.size() < 2) {
            kill();
            return true;
        } else {
            setPoints(newPoints.toArray(new Point[0]));
            assert isConsistent();
        }
        return false;
    }

    public void setInvisible(int period) {
        this.invisible = true;
        this.invisiblePoint = getHead();
        invisibilityTimeLeft = period;
        timeSinceInvisible = 0;
    }


}
