package za.ac.wits.snake;


import za.ac.wits.snake.agent.Agent;
import za.ac.wits.snake.config.Config;
import za.ac.wits.snake.config.LocalConfig;

import java.util.*;

/**
 * Created by Steve James on 06-Apr-16.
 */
abstract class Game {

    private static final Random random = LocalConfig.getInstance().createRNG();

    protected int timeSteps;
    protected GameStatus status;

    protected int gameId;
    protected Grid grid;

    protected final List<Agent> agents;
    protected Snake[] snakes;
    protected Apple[] apples;
    protected Obstacle[] obstacles;
    protected ZombieSnake[] zombies;
    protected final Config config;

    public Game(int id, final List<Agent> agents) {
        gameId = id;
        config = LocalConfig.getInstance();
        grid = new Grid(config.getGameWidth(), config.getGameHeight());
        this.agents = agents;
        timeSteps = -1;
    }

    protected void addAgent(final Agent agent) {
        if (status == GameStatus.RUNNING) {
            throw new IllegalStateException("Cannot add agent when game is already running");
        }
        agents.add(agent);
    }

    Obstacle[] getObstacles() {
        return obstacles;
    }

    public boolean isRunning() {
        return status == GameStatus.RUNNING;
    }

    private boolean isTimeElapsed() {
        return config.getGameSpeed() * timeSteps >= config.getGameDuration() * 1000;
    }

    protected void updateState() {
        //no-op
    }

    public void updateStatus(GameStatus status) {
        this.status = status;
    }

    public final void start() {

        updateStatus(GameStatus.STARTING);
        int numSnakes = config.getSnakes();

        initObstacles(config.getObstacles());
        initZombies(config.getZombies());
        initSnakes(numSnakes);
        initApples(config.getApples());
        initAgents();

        timeSteps = 0;

        updateState();
        updateStatus(GameStatus.RUNNING);

        int actionTime = config.getGameSpeed();

        //gameLoop
        while (isRunning() && !isTimeElapsed()) {
            respawnSnakes();
            respawnApples();
            assert isConsistent();

            provideInput(numSnakes);
            //wait for appropriate length of time
            sleep(actionTime);
            Direction[] actions = getActions(numSnakes);
            execute(actions);
            //zombies move last
            executeZombieActions();
            assert isConsistent();
            if (isPrematureWin()) {
                break;
            }
            ++timeSteps;
            updateState();

            for (Apple apple : apples) {

                if (apple.isEaten()) {
                    //apple has just been eaten. No need to advance time step
                    continue;
                }

                //always count down the special apple
//                if (apple instanceof InvisibilityApple && isSnakeInvisible()) {
//                    //count should start for special only when there's no invisible currently
//                    continue;
//                }
                apple.advanceTimeStep();

            }
        }
        ++timeSteps;
        updateStatus(GameStatus.FINISHING);
        killAgents();
        updateEndState();
        updateStatus(GameStatus.FINISHED);
        onCompleted();
    }

    protected void onCompleted() {
    }

    protected void updateEndState() {

    }

    protected void killAgents() {
        for (Agent agent : agents) {
            agent.endEpisode();
        }
    }

    protected boolean isPrematureWin() {
        return false;
    }

    protected void provideInput(int numSnakes) {
        for (int i = 0; i < numSnakes; ++i) {
            if (snakes[i].isAlive()) {
                //todo send status non-blocking
                agents.get(i).acceptState(buildState(i));
            }
        }
    }

    private void sleep(int actionTime) {
        try {
            Thread.sleep(actionTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected final Direction getAction(int snakeId) {
        if (snakes[snakeId].isAlive()) {
            Direction move = agents.get(snakeId).getMove();
            if (move == Direction.NONE) {
                //agent has crashed
                snakes[snakeId].setDead();
            }
            Direction current = snakes[snakeId].getDirection();
            move = Direction.toFixedDirection(move, snakes[snakeId]);
            if (Direction.isOpposite(move, current)) {
                move = current; //keep heading in same direction
            }
            return move;
        }
        return Direction.NONE;
    }

    protected Direction[] getActions(int numSnakes) {
        Direction[] actions = new Direction[numSnakes];
        Arrays.fill(actions, Direction.STRAIGHT);
        for (int i = 0; i < numSnakes; ++i) {
            actions[i] = getAction(i);
        }
        return actions;
    }


    private Point calculateNextPoint(final Point current, Direction dir) {
        switch (dir) {
            case SOUTH:
                return new Point(current.x, current.y + 1);
            case WEST:
                return new Point(current.x - 1, current.y);
            case EAST:
                return new Point(current.x + 1, current.y);
            case NORTH:
                return new Point(current.x, current.y - 1);
            case STRAIGHT:
            default:
                assert false;
                return new Point();
        }
    }

    protected Apple generateApple() {
        Apple apple = new Apple(random(0, grid.getWidth()), random(0, grid.getHeight()), config.getAppleTimeLimit());
        return apple;
    }

    protected Snake generateRandomSnake(int id) {
        final int length = config.getStartingLength();

        Point[] endpoints = new Point[2];

        if (random()) {
            //horizontal
            int x = random(0, grid.getWidth() - length);
            int y = random(0, grid.getHeight());
            endpoints[0] = new Point(x, y);
            endpoints[1] = new Point(x + length - 1, y);
        } else {
            //vertical
            int x = random(0, grid.getWidth());
            int y = random(0, grid.getHeight() - length);
            endpoints[0] = new Point(x, y);
            endpoints[1] = new Point(x, y + length - 1);
        }
        if (random()) {
            //swap direction
            return new Snake(endpoints[1], endpoints[0], id);
        }
        return new Snake(endpoints[0], endpoints[1], id);

    }

    private boolean isConsistent() {

        //check that apples are consistent
        Set<Point> expected = new HashSet<>();
        for (Apple apple : apples) {
            if (!apple.isEaten() && apple.getX() >= 0 && apple.getY() >= 0) {
                expected.add(new Point(apple.getX(), apple.getY()));
            }
        }
        for (int x = 0; x < grid.getWidth(); ++x) {
            for (int y = 0; y < grid.getHeight(); ++y) {
                Point z = new Point(x, y);
                int val = grid.at(z);
                if (val == Grid.APPLE) {
                    if (!expected.remove(z)) {
                        System.err.println("A");
                        return false;
                    }
                }
            }
        }
        if (!expected.isEmpty()) {
            System.err.println("B");
            return false;
        }


        //check that board is consistent
        for (int x = 0; x < grid.getWidth(); ++x) {
            for (int y = 0; y < grid.getHeight(); ++y) {
                int z = grid.at(x, y);
                if (z > Grid.EMPTY) {
                    if (!(snakes[z - 1].isAlive() && snakes[z - 1].contains(new Point(x, y)))) {
                        System.err.println("C");
                        return false;
                    }
                }
            }
        }
        for (Snake snake : snakes) {
            if (snake.isAlive()) {
                for (Point point : snake) {
                    if (grid.at(point) != snake.getId() + 1) {
                        System.err.println("D");
                        return false;
                    }
                }
            }
        }

        //check that snakes are consistent
        for (final Snake snake : snakes) {
            if (snake.isAlive() && !snake.isConsistent()) {
                //System.err.println("snake " + snake + " is not consistent");
                System.err.println("E");
                return false;
            }
        }
        //check that zombies are consistent
        for (final ZombieSnake zombie : zombies) {
            if (!zombie.isAlive() || !zombie.isConsistent()) {
                //    System.err.println("zombie " + zombie + " is not consistent");
                System.err.println("F");
                return false;
            }
        }
        //check that snakes don't intersect one another
        for (int i = 0; i < snakes.length - 1; i++) {
            for (int j = i + 1; j < snakes.length; j++) {
                if (snakes[i].isAlive() && snakes[j].isAlive() && snakes[i].contains(snakes[j].getHead())) {
                    //      System.err.println("snakes " + snakes[i] + snakes[j] + " intersect");
                    System.err.println("G");
                    return false;
                }
            }
        }

        //check that zombies don't intersect one another
        for (int i = 0; i < zombies.length - 1; i++) {
            for (int j = i + 1; j < zombies.length; j++) {
                if (zombies[i].contains(zombies[j].getHead())) {
                    //  System.err.println("zombies " + zombies[i] + " - " + zombies[j] + " intersect");
                    // System.err.println(grid);
                    System.err.println("H");
                    return false;
                }
            }
        }

        //check that zombies don't intersect snakes
        for (int i = 0; i < zombies.length; i++) {
            for (int j = 0; j < snakes.length; j++) {
                if (snakes[j].isAlive() && snakes[j].contains(zombies[i].getHead())) {
                    // System.err.println("SZ " + snakes[j] + zombies[i] + " intersect");
                    System.err.println("I");
                    return false;
                }
            }
        }

        return true;
    }


    private void redrawSnakes() {
        grid.clearSnakes();
        for (Snake snake : snakes) {
            if (snake.isAlive()) {
                grid.add(snake);
            }
        }
    }

    private void redrawZombies() {
        grid.clearZombies();
        for (ZombieSnake zombie : zombies) {
            assert zombie.isAlive();
            grid.add(zombie);
        }
    }

    private boolean isInconsistent(Iterable<Point> points) {

        for (Point point : points) {

            if (grid.at(point) != Grid.EMPTY) {
                continue;
            }
            for (Snake snake : snakes) {
                if (snake.isAlive() && snake.contains(point)) {
                    return true;
                }
            }
        }
        return false;
    }

    private void respawnSnakes() {
        for (Snake snake : snakes) {
            while (!snake.isAlive() && snake.getTimeToRespawn() == 0) {
                Snake candidate = generateRandomSnake(snake.getId());
                assert !isInconsistent(candidate.getFullBody());
                if (grid.add(candidate)) {
                    snake.setPoints(candidate.getHead(), candidate.getTail());
                    snake.setAlive();
                }
            }
        }
    }

    private boolean isSnakeInvisible() {
        for (Snake snake : snakes) {
            if (snake.isInvisible()) {
                return true;
            }
        }
        return false;
    }

    private void respawnApple(SpecialApple apple) {
        if (!apple.isReady() || isSnakeInvisible()) {
            return;
        }
        Apple candidate;
        do {
            candidate = generateApple();
        } while (!grid.add(candidate));
        apple.reset(candidate.getX(), candidate.getY());
    }

    private void respawnApples() {
        for (Apple apple : apples) {
            if (apple instanceof SpecialApple) {
                SpecialApple special = (SpecialApple) apple;
                respawnApple(special);
            } else {
                while (apple.isEaten()) {
                    //if the apple's spawn limit has been reached, clear  it off the board
                    if (grid.at(apple.getX(), apple.getY()) == Grid.APPLE) {
                        grid.set(apple.getX(), apple.getY(), Grid.EMPTY);
                    }
                    Apple candidate = generateApple();
                    if (grid.add(candidate)) {
                        apple.reset(candidate.getX(), candidate.getY());
                    }
                }
            }
        }
    }

    private void initSnakes(int numSnakes) {
        snakes = new Snake[numSnakes];
        for (int i = 0; i < numSnakes; ) {
            Snake snake = generateRandomSnake(i);
            snake.setAlive();
            if (grid.add(snake)) {
                snakes[i] = snake;
                ++i;
            }
        }
    }

    private void initObstacles(int numObstacles) {
        obstacles = new Obstacle[numObstacles];
        for (int i = 0; i < numObstacles; ) {
            Snake temp = generateRandomSnake(Grid.OBSTACLE);
            Obstacle obstacle = new Obstacle(temp.getHead(), temp.getTail());
            if (grid.add(obstacle)) {
                obstacles[i] = obstacle;
                ++i;
            }
        }
    }

    private void initZombies(int numZombies) {
        zombies = new ZombieSnake[numZombies];
        for (int i = 0; i < numZombies; ) {
            ZombieSnake zombie = new ZombieSnake(generateRandomSnake(Grid.ZOMBIE));
            if (grid.add(zombie)) {
                zombies[i] = zombie;
                ++i;
            }
        }
    }


    private void initApples(int numApples) {
        apples = new Apple[numApples];
        for (int i = 0; i < numApples; ) {
            Apple apple = generateApple();
            if (i == 0 && config.isSpecialApple()) {
                apples[i] = new InvisibilityApple(config.getAppleTimeLimit(), config.getSpecialAppleAppearance(), config.getInvisibilityPeriod());
                ++i;
            } else if (grid.add(apple)) {
                apples[i] = apple;
                ++i;
            }
        }
    }

    private void initAgents() {

        StringBuilder sb = new StringBuilder();
        for (Obstacle obstacle : obstacles) {
            sb.append(obstacle).append('\n');
        }
        String initString = String.format("%d %d %d %d\n%s", agents.size(), grid.getWidth(), grid.getHeight(),
                config.getGameMode().ordinal(), sb.toString());
        for (Agent agent : agents) {
            agent.startEpisode(gameId, initString);
        }
    }

    private void execute(final Direction[] actions) {

        boolean[] markDead = new boolean[actions.length];
        for (int i = 0; i < actions.length; ++i) {
            if (actions[i] == Direction.FROZEN) {
                continue;
            }
            if (actions[i] != Direction.NONE) {
                Point next = calculateNextPoint(snakes[i].getHead(), actions[i]);
                if (!grid.contains(next) || grid.at(next) == Grid.OBSTACLE || grid.at(next) == Grid.ZOMBIE) {
                    markDead[i] = true;
                } else {
                    snakes[i].advance(next);
                }
            } else {
                snakes[i].advance();
            }
        }
        checkSnakeCollisions(markDead);
        for (int i = 0; i < snakes.length; i++) {
            if (snakes[i].isAlive()) {
                Point head = snakes[i].getHead();
                switch (grid.at(head)) {
                    case Grid.OBSTACLE:
                        markDead[i] = true;
                        break;
                    case Grid.APPLE:
                        if (!markDead[i]) {
                            snakes[i].addAppleBonus(config.getAppleGrow());
                        }
                        //if the snake simultaneuously eats the apple and dies, he gets no bonus, but the apple is removed
                        //from the grid
                        for (Apple apple : apples) {
                            if (apple.equals(head)) {
                                apple.markEaten();
                                grid.set(apple.getX(), apple.getY(), Grid.EMPTY);
                                if (apple instanceof SpecialApple) {
                                    SpecialApple sa = (SpecialApple) apple;
                                    sa.affectGame(i, this);
                                    apple.reset(-1, -1); //move apple off board
                                }
                                break;
                            }
                        }

                        break;
                }
            }
        }
        for (int i = 0; i < markDead.length; ++i) {
            if (markDead[i]) {
                snakes[i].kill();
            }
        }
        redrawSnakes();
    }

    /*check only for snake-on-snake action*/
    void checkSnakeCollisions(final boolean[] markDead) {

        for (int i = 0; i < snakes.length; ++i) {
            if (markDead[i] || !snakes[i].isAlive()) {
                continue;
            }

            final Point head = snakes[i].getHead();
            int square = grid.at(head);
            if (square == Grid.EMPTY || square == Grid.APPLE) {
                //check head-on
                for (int j = 0; j < snakes.length; j++) {
                    if (i == j) continue;
                    if (snakes[j].isAlive() && snakes[j].getHead().equals(head)) {
                        markDead[i] = markDead[j] = true;
                    }
                }
            } else if (square != Grid.OBSTACLE && square != Grid.ZOMBIE) {
                Snake other = snakes[square - 1];
                if (other.contains(head)) {
                    //the above can be false, since grid is inconsistent with snakes at this point, in which
                    //case the snake has just moved to the other's tail, and so is still alive
                    markDead[i] = true;
                    if (other.getId() != snakes[i].getId()) {
                        other.addKill();
                    }
                }
            }
        }
    }

    /**
     * Zombies DO NOT move simultaneously. They move one at a time, in order, and pick their most preferred choice.
     * If they cannot move, then they don't
     */
    private void executeZombieActions() {
        Point[][] preferences = calculateZombiePreferences();

        //prevent head ons and allow tail following
        Set<Point> accessible = new HashSet<>();
        Set<Point> inaccessible = new HashSet<>();

        boolean[] snakesKilled = new boolean[snakes.length];
        for (int i = 0; i < zombies.length; i++) {
            for (int j = 0; j < preferences[i].length; ++j) {
                Point point = preferences[i][j];
                if (point == null) {
                    //has no move
                    zombies[i].advance(null);
                    break;
                }

                boolean valid = !inaccessible.contains(point) && (grid.at(point) != Grid.ZOMBIE ||
                        accessible.contains(point));
                if (valid && grid.at(point) > Grid.EMPTY) {
                    //new policy: zombies cannot cut a snake's body; only it's head
                    valid = false;
                    for (Snake snake : snakes) {
                        if (snake.isAlive() && snake.getHead().equals(point)) {
                            valid = true;
                            break;
                        }
                    }
                }

                if (valid) {
                    final Point previousTail = zombies[i].getTail();
                    zombies[i].advance(point);
                    if (grid.at(point) > Grid.EMPTY) {
                        //we've hit a snake: cut or kill it
                        int snakeId = grid.at(point) - 1;
                        Snake snake = snakes[snakeId];
                        //either the snake is alive, or it's been killed in this phase. If killed in another phase, we
                        //have issues
                        assert snakesKilled[snakeId] || snake.isAlive();
                        if (snake.isAlive()) {
                            boolean isKilled = snake.cut(point);
                            snakesKilled[snakeId] = isKilled;
                        }
                    } else {
                        for (Apple apple : apples) {
                            if (apple.equals(point)) {
                                apple.markEaten();
                                grid.set(apple.getX(), apple.getY(), Grid.EMPTY);
                                break;
                            }
                        }
                    }
                    accessible.add(previousTail);
                    inaccessible.add(zombies[i].getHead());
                    break;
                }
            }
        }
        redrawSnakes();
        redrawZombies();
    }


    protected final String buildState(int snakeId) {
        StringBuilder sb = new StringBuilder();
        for (final Apple apple : apples) {
            sb.append(apple).append('\n');
        }
        for (final ZombieSnake zombie : zombies) {
            sb.append(zombie).append('\n');
        }
        sb.append(snakeId).append("\n");
        for (final Snake snake : snakes) {

            //no other players should be able to see the invisible snake's body
            if (snake.isInvisible() && snake.getId() != snakeId) {
                //sb.append("invisible ").append(snake.getLength()).append(' ').append(snake.getKills()).append('\n');
                sb.append(snake.getSemiVisible()).append('\n');
            } else {
                sb.append(snake).append('\n');
            }
        }
        return sb.toString();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (final Apple apple : apples) {
            sb.append(apple).append('\n');
        }
        for (final ZombieSnake zombie : zombies) {
            sb.append(zombie).append('\n');
        }
        sb.append("%d\n");
        for (final Snake snake : snakes) {
            sb.append(snake).append('\n');
        }
        return sb.toString();
    }


    //random in range [low, high)
    private static int random(int low, int high) {
        return random.nextInt(high - low) + low;
    }

    private static boolean random() {
        return random.nextBoolean();
    }

    private Point[][] calculateZombiePreferences() {

        Point[][] preferences = new Point[zombies.length][4];

        List<Point> targets = new ArrayList<>(snakes.length);
        for (Snake snake : snakes) {
            //zombies can't see invisible snakes (but can still accidentally collide with them)
            if (snake.isAlive() && !snake.isInvisible()) {
                //only attack heads/brains
                targets.add(snake.getHead());
                //attack any part of the body
                //targets.addAll(snake.getFullBody());
            }
        }

        int i = 0;
        for (ZombieSnake zombie : zombies) {
            assert zombie.isAlive();
            if (zombie.canMove()) {
                Point head = zombie.getHead();
                List<Point> safePoints = new ArrayList<>(4);
                Point[] neighbours = {
                        new Point(head.x - 1, head.y),
                        new Point(head.x + 1, head.y),
                        new Point(head.x, head.y - 1),
                        new Point(head.x, head.y + 1),
                };
                for (Point candidate : neighbours) {
                    if (grid.contains(candidate)) {
                        safePoints.add(candidate);
                    }
                }
                preferences[i] = zombie.getPreference(safePoints, targets);
            }
            i++;
        }

        return preferences;
    }
}


