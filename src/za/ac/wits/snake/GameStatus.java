package za.ac.wits.snake;

/**
 * Created by Steve James on 06-Apr-16.
 */
public enum GameStatus {

    WAITING, STARTING, RUNNING, FINISHING, FINISHED,
}
