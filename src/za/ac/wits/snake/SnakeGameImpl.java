package za.ac.wits.snake;

import za.ac.wits.snake.agent.Agent;
import za.ac.wits.snake.agent.BuiltInAgent;
import za.ac.wits.snake.agent.ExternalAgent;
import za.ac.wits.snake.agent.SnakeAgent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Steve James on 07-Apr-16.
 */
class SnakeGameImpl extends Game implements SnakeGame {

    private String lastState;
    private String standings;

    public SnakeGameImpl(int id, List<Agent> agents) {
        super(id, agents);
        int nAgents = config.getSnakes();
        for (int i = 0; i < agents.size(); ++i) {
            if (agents.get(i) == null) {
                agents.set(i, createStandardAgent());
            }
        }
        for (int i = agents.size(); i < nAgents; i++) {
            addAgent(createStandardAgent());
        }
    }

    private Agent createStandardAgent() {
        return new BuiltInAgent(0, "");
    }

    @Override
    public String getNextState() {
        return lastState;
    }

    protected void updateState(String state) {
        lastState = state;
    }


    @Override
    public boolean isRunning() {
        return status == GameStatus.RUNNING;
    }

    @Override
    protected void updateState() {
        String appleString = String.join("\n",
                Arrays.stream(apples)
                        .map(apple -> String.format("%d %d", apple.getX(), apple.getY()))
                        .collect(Collectors.toList()));

        String zombieString = String.join("\n",
                Arrays.stream(zombies)
                        .map(ZombieSnake::toString)
                        .collect(Collectors.toList()));

        StringBuilder snakeString = new StringBuilder();
        for (int i = 0; i < snakes.length; ++i) {
            SnakeAgent agent = (SnakeAgent) agents.get(i);
            Snake snake = snakes[i];
            snakeString.append(String.format("%d %d %s %s\n", agent.getLongest(), agent.getId(), agent.getName(), snake.toString()));
        }


        List<String> list = new ArrayList<>(Arrays.asList(appleString, zombieString, snakeString.toString()));
        list.removeAll(Collections.singleton(""));
        updateState(String.join("\n", list));
        updateStandings();
    }

    @Override
    public void updateStandings() {
        standings = getRankings("Standings", false);
    }

    protected String getRankings(String heading) {
        return getRankings(heading, true);
    }

    protected String getRankings(String heading, boolean sort) {
        List<SnakeAgent> copy = agents.stream()
                .map(agent -> (SnakeAgent) agent)
                .collect(Collectors.toList());
        if (sort) {
            Collections.sort(copy);
        }
        int rank = 1;
        for (SnakeAgent agent : copy) {
            agent.setRank(rank);
            ++rank;
        }
        StringBuilder sb = new StringBuilder(heading).append('\n');
        for (SnakeAgent agent : copy) {
            sb.append(agent.getId()).append(' ').append(agent.getName()).append(' ').append(agent.getRank()).append(' ')
                    .append(agent.getLongest()).append(' ').append(agent.getKills()).append('\n');
        }
        return sb.toString();
    }

    @Override
    protected void updateEndState() {
        super.updateEndState();
        updateState(getRankings("Game Over"));

    }

    @Override
    public String getStandings() {
        return standings;
    }


    @Override
    protected boolean isPrematureWin() {
        int nAliveSnakes = 0;
        SnakeAgent aliveSnake = null;

        for (Agent a : agents) {
            boolean alive = true;
            if (a instanceof ExternalAgent) {
                ExternalAgent ea = (ExternalAgent) a;
                alive = ea.isRunning();
            }
            if (alive) {
                ++nAliveSnakes;
                SnakeAgent agent = (SnakeAgent) a;
                aliveSnake = agent;
            }
        }

        if (nAliveSnakes < 2) {
            for (Agent agent : agents) {
                if (agent != aliveSnake) {
                    ((SnakeAgent) agent).setLongest(-1);
                }
            }
            if (aliveSnake != null) {
                System.out.println("(" + aliveSnake.getId() + ") wins by forfeit");
            }
            return true;
        }
        return false;
    }


    @Override
    public final void run() {
        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
        updateState("Game Starting");
        start();
    }

    @Override
    public List<Agent> getAgents() {
        return agents;
    }

    @Override
    public Obstacle[] getObstacles() {
        return super.getObstacles();
    }
}

