package za.ac.wits.snake;

/**
 * Created by Steve James on 28-Apr-17.
 */
public class InvisibilityApple extends SpecialApple {

    private final int invisibilityPeriod;

    public InvisibilityApple(int timeLimit, int spawnWait, int invisibilityPeriod) {
        super(timeLimit, spawnWait);
        this.invisibilityPeriod = invisibilityPeriod;
    }

    @Override
    protected void affectGame(int snakeId, Game game) {
        game.snakes[snakeId].setInvisible(invisibilityPeriod);
    }
}
