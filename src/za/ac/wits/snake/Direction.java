package za.ac.wits.snake;

/**
 * Created by Steve James on 06-Apr-16.
 */
public enum Direction {
    FROZEN(-2), //the snake does nothing at all (not even continues)
    NONE(-1),
    //fixed directions
    NORTH(0), SOUTH(1), WEST(2), EAST(3),
    //relative directions
    LEFT(4), STRAIGHT(5), RIGHT(6);

    private final int value;

    Direction(int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }

    public static Direction fromValue(int value) {
        for (Direction direction : values()) {
            if (direction.value == value) {
                return direction;
            }
        }
        return NONE;
    }

    public static Direction toFixedDirection(Direction direction, Snake snake) {
        Direction currentDirection = snake.getDirection();
        if (currentDirection == Direction.EAST || currentDirection == Direction.WEST) {
            switch (direction) {
                case LEFT:
                    return currentDirection == Direction.EAST ? Direction.NORTH : Direction.SOUTH;
                case STRAIGHT:
                    return currentDirection;
                case RIGHT:
                    return currentDirection == Direction.EAST ? Direction.SOUTH : Direction.NORTH;
            }
        } else {
            switch (direction) {
                case LEFT:
                    return currentDirection == Direction.NORTH ? Direction.WEST : Direction.EAST;
                case STRAIGHT:
                    return currentDirection;
                case RIGHT:
                    return currentDirection == Direction.NORTH ? Direction.EAST : Direction.WEST;
            }
        }
        return direction;

    }

    public static Direction fromString(String value) {
        try {
            int val = Integer.parseInt(value);
            return fromValue(val);
        } catch (NumberFormatException e) {
        }
        value = value.toUpperCase();
        for (Direction direction : Direction.values()) {
            if (value.equals(direction.name())) {
                return direction;
            }
        }
        return NONE;
    }

    public static boolean isOpposite(Direction A, Direction B) {
        switch (A) {
            case NORTH:
                return B == SOUTH;
            case SOUTH:
                return B == NORTH;
            case WEST:
                return B == EAST;
            case EAST:
                return B == WEST;
            case NONE:
                return false;
            default:
                assert (false);
                return false;
        }
    }

    public static Direction directionTo(final Point to, final Point from) {
        assert to.distSq(from) == 1;
        //if statement may be quicker
        if (to.x == from.x) {
            return (to.y < from.y) ? Direction.NORTH : Direction.SOUTH;
        }
        return (to.x < from.x) ? Direction.WEST : Direction.EAST;
        /*
         int x = to.x - from.x + 2;
         int y = to.y - from.y + 2;
         switch ((x - y) * (x * y)) {
             case -2:
                 return Direction.WEST;
             case 2:
                 return Direction.EAST;
             case -6:
                 return Direction.NORTH;
             case 6:
                 return Direction.SOUTH;
             builtin:
                 assert(false);
                 return Direction.NONE;
         }
         */
    }


}
