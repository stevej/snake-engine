package za.ac.wits.snake;

import za.ac.wits.snake.config.Config;
import za.ac.wits.snake.config.LocalConfig;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/**
 * Created by Steve on 06-May-16.
 */
public class ZombieSnake extends Snake {

    private static final int SLOWNESS = LocalConfig.getInstance().getZombieSpeed() - 1; //zombies will move every x time steps

    private int timeTillNextAction;


    public ZombieSnake(final Snake snake) {
        super(snake.getHead(), snake.getTail(), Grid.ZOMBIE);
        timeTillNextAction = SLOWNESS;
        setAlive();
    }

    @Override
    public void clear() {
        super.clear();
        timeTillNextAction = SLOWNESS;
    }

    @Override
    public void kill() {
        throw new IllegalStateException("Zombies cannot be killed!");
    }

    @Override
    public void advance() {
        throw new IllegalStateException("Zombies cannot be killed!");
    }

    @Override
    public void advance(Point nextPoint) {
        //nextPoint = null means that the snake had no where to go, so shouldn't move
        timeTillNextAction = Math.max(timeTillNextAction - 1, 0);
        if (nextPoint != null) {
            super.advance(nextPoint);
            timeTillNextAction = SLOWNESS;
            assert isConsistent();
        }
    }

    @Override
    protected boolean isConsistent() {
        return getLength() == 5 && super.isConsistent();
    }

    public boolean canMove() {
        return timeTillNextAction == 0;
    }

    @Override
    public boolean cut(Point cutPoint) {
        throw new IllegalStateException("Zombies cannot be cut!");
    }

    public boolean isGrowing() {
        return false;
    }

    public void addAppleBonus(int appleGrowLength) {
        //no-op
    }

    /**
     * Zombies pick the action that gets them closest to a snake's head, without killing itself
     *
     * @param candidates the possible squares to move to
     * @param targets    the list of snake heads
     * @return the direction to move, or the current direction if no move exists
     */
    public Point[] getPreference(final List<Point> candidates, final List<Point> targets) {
        assert canMove();
        Point[] points = new Point[4];
        //if we want stochastic, do Collections.shuffle(candidates);
        Collections.sort(candidates, new CandidateComparator(targets));
        for (int i = 0; i < candidates.size(); ++i) {
            points[i] = candidates.get(i);
        }
        return points;
    }

    private static class CandidateComparator implements Comparator<Point> {

        private final List<Point> targets;

        public CandidateComparator(final List<Point> targets) {
            this.targets = targets;
        }

        @Override
        public int compare(Point A, Point B) {
            return Integer.compare(getMin(A), getMin(B));
        }

        private int getMin(Point candidate) {
            int min = Integer.MAX_VALUE;
            for (Point target : targets) {
                int dist = Math.abs(candidate.x - target.x) + Math.abs(candidate.y - target.y);
                min = Math.min(min, dist);
            }
            return min;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj.getClass() == CandidateComparator.class) {
                CandidateComparator c = (CandidateComparator) obj;
                return c.targets.equals(targets) && Objects.equals(obj, this);
            }
            return false;
        }
    }

    @Override
    public String toString() {
        assert isAlive();
        StringBuilder sb = new StringBuilder();
        if (isAlive()) {
            Point[] points = new Point[getLength()];
            Direction[] directions = new Direction[getLength()];
            boolean[] vertices = new boolean[getLength()];
            int i = 0;
            for (Point p : this) {
                points[i] = p;
                if (i == 0) {
                    vertices[i] = true;
                } else {
                    directions[i] = Direction.directionTo(p, points[i - 1]);
                    vertices[i - 1] = (directions[i] != directions[i - 1]);
                }
                ++i;
            }
            vertices[getLength() - 1] = true;
            for (int j = 0; j < vertices.length; ++j) {
                if (vertices[j]) {
                    sb.append(' ').append(points[j].x).append(',').append(points[j].y);
                }
            }
        }
        return sb.toString().trim();
    }


}
