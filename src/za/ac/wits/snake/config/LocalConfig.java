package za.ac.wits.snake.config;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Steve James on 07-May-16.
 */
public final class LocalConfig extends Config {

    private File file;

    @Override
    public void load() {
        //try find config file
        File current = new File(".");
        while (!current.isDirectory()) {
            current = current.getParentFile();
        }
        for (File file : current.listFiles()) {
            if (!file.isDirectory() && file.getName().toLowerCase().contains("snake_config")) {
                try {
                    load(file);
                    break;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public void load(File file) throws IOException {
        try (Stream<String> stream = Files.lines(Paths.get(file.getAbsolutePath()))) {
            load(stream);
        }
        this.file = file;
    }

    public File getFile() {
        return file;
    }


    public static Config getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new LocalConfig();
        }
        return INSTANCE;
    }


    @Override
    public String toString() {
        return settings.stream().map(Setting::toString).collect(Collectors.joining("\n"));
    }
}
