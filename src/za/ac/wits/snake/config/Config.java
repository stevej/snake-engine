package za.ac.wits.snake.config;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.*;
import java.util.stream.Stream;

/**
 * Created by Steve James on 07-May-16.
 */
public abstract class Config {

    protected static final Map<String, Setting<?>> fromAlias = new HashMap<>();
    protected static final List<Setting<?>> settings = new ArrayList<>();

    private static final int DEFAULT_GAME_TIME = 300; //seconds
    private final Setting<Integer> gameTime = new Setting<>(DEFAULT_GAME_TIME, "game_time", "gt", "d", "duration");

    private static final Mode DEFAULT_GAME_MODE = Mode.GROW;
    private final Setting<Mode> gameMode = new Setting<>(DEFAULT_GAME_MODE, "game_mode", "gm");

    private static final int DEFAULT_GAME_SPEED = 50;
    private final Setting<Integer> gameSpeed = new Setting<>(DEFAULT_GAME_SPEED, "game_speed", "gs", "s", "speed");

    private static final int DEFAULT_APPLE_GROWTH_AMOUNT = 3;
    private final Setting<Integer> appleGrow = new Setting<>(DEFAULT_APPLE_GROWTH_AMOUNT, "apple_growth", "ag");

    private static final int DEFAULT_STARTING_LENGTH = 5;
    private final Setting<Integer> startingLength = new Setting<>(DEFAULT_STARTING_LENGTH, "starting_length", "sl");

    private static final int DEFAULT_NUMBER_SNAKES = 4;
    private final Setting<Integer> numSnakes = new Setting<>(DEFAULT_NUMBER_SNAKES, "num_snakes", "ns");

    private static final int DEFAULT_NUMBER_OBSTACLES = 0;
    private final Setting<Integer> numObstacles = new Setting<>(DEFAULT_NUMBER_OBSTACLES, "num_obstacles", "no");

    private static final int DEFAULT_NUMBER_ZOMBIES = 0;
    private final Setting<Integer> numZombies = new Setting<>(DEFAULT_NUMBER_ZOMBIES, "num_zombies", "nz");

    private static final int DEFAULT_ZOMBIE_SPEED = 1;
    private final Setting<Integer> zombieSpeed = new Setting<>(DEFAULT_ZOMBIE_SPEED, "zombie_speed", "zs");

    private static final int DEFAULT_NUMBER_APPLES = 2;
    private final Setting<Integer> numApples = new Setting<>(DEFAULT_NUMBER_APPLES, "num_apples", "na");

    private static final boolean DEFAULT_SPECIAL_APPLE = true;
    private final Setting<Boolean> specialApple = new Setting<>(DEFAULT_SPECIAL_APPLE, "special_apple", "sa");

    private static final int DEFAULT_APPLE_TIME_LIMIT = 600; //timesteps
    private final Setting<Integer> appleTimeLimit = new Setting<>(DEFAULT_APPLE_TIME_LIMIT, "apple_limit", "al");

    private static final int DEFAULT_SPECIAL_APPLE_APPEARANCE = 600; //timesteps
    private final Setting<Integer> specialAppleAppearance = new Setting<>(DEFAULT_SPECIAL_APPLE_APPEARANCE, "special_apple_appearance", "saa");

    private static final int DEFAULT_INVISIBILITY_PERIOD = 75; //timesteps
    private final Setting<Integer> invisibilityPeriod = new Setting<>(DEFAULT_INVISIBILITY_PERIOD, "invisibility_period", "ip");

    private static final int DEFAULT_GAME_WIDTH = 50;
    private final Setting<Integer> gameWidth = new Setting<>(DEFAULT_GAME_WIDTH, "game_width", "gw");

    private static final int DEFAULT_GAME_HEIGHT = 50;
    private final Setting<Integer> gameHeight = new Setting<>(DEFAULT_GAME_HEIGHT, "game_height", "gh");

    private static final String DEFAULT_SEED = null;
    private final Setting<String> randomSeed = new Setting<>(DEFAULT_SEED, "random_seed", "rs");

    private static final int DEFAULT_TIME_BETWEEN_ROUNDS = 10000;
    private final Setting<Integer> betweenRounds = new Setting<>(DEFAULT_TIME_BETWEEN_ROUNDS, "between_rounds", "br");


    public int getSpecialAppleAppearance() {
        return specialAppleAppearance.value;
    }

    public int getGameDuration() {
        return gameTime.value;
    }

    public Mode getGameMode() {
        return gameMode.value;
    }

    public int getGameSpeed() {
        return gameSpeed.value;
    }

    public int getAppleGrow() {
        return appleGrow.value;
    }

    public int getStartingLength() {
        return startingLength.value;
    }

    public int getSnakes() {
        return numSnakes.value;
    }

    public int getObstacles() {
        return numObstacles.value;
    }

    public int getZombies() {
        return numZombies.value;
    }

    public int getApples() {
        return numApples.value;
    }

    public boolean isSpecialApple() {
        return specialApple.value;
    }

    public int getAppleTimeLimit() {
        return appleTimeLimit.value;
    }

    public int getGameWidth() {
        return gameWidth.value;
    }

    public int getGameHeight() {
        return gameHeight.value;
    }

    public int getZombieSpeed() {
        return zombieSpeed.value;
    }

    public int getTimeBetweenRounds() {
        return betweenRounds.value;
    }

    public int getInvisibilityPeriod() {
        return invisibilityPeriod.value;
    }


    public byte[] getSeed() {
        return randomSeed.value == null ? null : randomSeed.value.getBytes();
    }

    public SecureRandom createRNG() {
        byte[] seed = getSeed();
        return seed == null ? new SecureRandom() : new SecureRandom(seed);
    }

    public <T> void set(String key, final T value) {
        Setting<?> setting = fromAlias.get(key);
        if (setting != null) {
            Setting<T> s = (Setting<T>) setting;
            s.value = value;
        }
    }

    public void write(File file) throws IOException {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            for (Setting<?> setting : settings) {
                bw.write(setting + System.lineSeparator());
            }
        }
    }

    public abstract void load();

    protected final void load(Stream<String> stream) {
        stream.map(String::trim)
                .map(line -> line.contains("#") ? line.substring(0, line.indexOf('#')) : line)
                .filter(line -> !line.isEmpty())
                .map(line -> line.split("\\s+"))
                .filter(array -> fromAlias.containsKey(array[0]))
                .forEach(array -> fromAlias.get(array[0]).set(array[1]));
    }


    protected static Config INSTANCE;

    protected Config() {
        load();
    }


    public enum Mode {
        NONE,
        GROW,
        SURVIVE,
        KILL,
        DEATHMATCH;

        public static Mode parseMode(String string) {
            for (Mode m : values()) {
                if (m.toString().equalsIgnoreCase(string)) {
                    return m;
                }
            }
            throw new RuntimeException("Mode " + string + " not found");
        }
    }

    protected class Setting<T> {

        private T value;
        private final String alias;

        public Setting(T value, String... alias) {
            this.value = value;
            this.alias = alias[0];
            for (String a : alias) {
                assert !fromAlias.containsKey(a);
                fromAlias.put(a, this);
            }
            settings.add(this);
        }

        @Override
        public String toString() {
            return alias + "\t" + String.valueOf(value);
        }

        public boolean set(String string) {
            try {
                if (value == null) {
                    Config.this.set(alias, string);
                    return true;
                } else if (value.getClass() == Boolean.class) {
                    Config.this.set(alias, Boolean.parseBoolean(string));
                    return true;
                } else if (value.getClass() == Integer.class) {
                    Config.this.set(alias, Integer.parseInt(string));
                    return true;
                } else if (value.getClass() == Long.class) {
                    Config.this.set(alias, Long.parseLong(string));
                    return true;
                } else if (value.getClass() == Float.class) {
                    Config.this.set(alias, Float.parseFloat(string));
                    return true;
                } else if (value.getClass() == Double.class) {
                    Config.this.set(alias, Double.parseDouble(string));
                    return true;
                } else if (value.getClass() == String.class) {
                    Config.this.set(alias, string);
                    return true;
                } else if (value.getClass() == Mode.class) {
                    Config.this.set(alias, Mode.parseMode(string));
                    return true;
                }
            } catch (Exception e) {
            }
            return false;
        }
    }


}
