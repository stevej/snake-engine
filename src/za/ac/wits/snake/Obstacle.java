package za.ac.wits.snake;

import java.util.Iterator;
import java.util.StringJoiner;

/**
 * Created by Steve James on 06-Apr-16.
 */
public class Obstacle implements Iterable<Point> {

    private final Snake snake;

    public Obstacle(final Point start, final Point end) {
        snake = new Snake(start, end, Grid.OBSTACLE);
    }

    @Override
    public Iterator<Point> iterator() {
        return snake.iterator();
    }

    public boolean intersects(Point point) {
        return snake.contains(point);
    }

    @Override
    public String toString() {
        StringJoiner sj = new StringJoiner(" ");
        for (Point point : snake) {
            sj.add(point.toString());
        }
        return sj.toString();
    }
}
