package za.ac.wits.snake.agent;

import za.ac.wits.snake.Direction;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.ArrayDeque;
import java.util.Queue;

/**
 * Created by  Dean Wookey on 07-Apr-12.
 */
public class HumanAgent extends SnakeAgent {

    private final String[] keys;
    private final String[] actionsKey;
    private final Direction[] actions = {Direction.NORTH, Direction.SOUTH, Direction.WEST, Direction.EAST};


    private Queue<Direction> moves = new ArrayDeque<>();

    public HumanAgent(int agentId, String name, String keytype) {
        super(agentId, name);
        keys = new String[4];
        keytype = keytype.toUpperCase();
        if (keytype.equals("NORMAL")) {
            keys[0] = "UP";
            keys[1] = "DOWN";
            keys[2] = "LEFT";
            keys[3] = "RIGHT";
        } else {
            assert keytype.length() == 4;
            keys[0] = Character.toString(keytype.charAt(0));
            keys[1] = Character.toString(keytype.charAt(1));
            keys[2] = Character.toString(keytype.charAt(2));
            keys[3] = Character.toString(keytype.charAt(3));
        }
        actionsKey = new String[]{getId() + "UP", getId() + "DOWN", getId() + "LEFT", getId() + "RIGHT"};
    }

    @Override
    public Direction getMove() {
        return moves.isEmpty() ? Direction.STRAIGHT : moves.poll();
    }

    @Override
    public void endEpisode() {
    }

    public void setupKeyBindings(JComponent component) {
        for (int i = 0; i < keys.length; i++) {
            final int j = i;
            component.getInputMap().put(KeyStroke.getKeyStroke(keys[i]), actionsKey[i]);
            component.getActionMap().put(actionsKey[i], new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    moves.add(actions[j]);
                }
            });
        }

    }
}