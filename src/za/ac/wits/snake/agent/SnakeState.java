package za.ac.wits.snake.agent;

import za.ac.wits.snake.Parser;
import za.ac.wits.snake.Point;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Bare-bones state-representation
 * <p>
 * Created by Dean Wookey on 07-Apr-12.
 */

class SnakeState {

    public int[][] playArea;
    Point[] apples;
    int width;
    int height;
    int numSnakes;
    Point myHead, myTail;

    private Parser parser = new Parser();

    public SnakeState(String state, int numSnakes, int width, int height) {
        playArea = new int[width][height];
        this.width = width;
        this.height = height;
        this.numSnakes = numSnakes;
        update(state);
    }

    public void addSnake(List<Point> snakeBody) {
        for (int i = 0; i < snakeBody.size() - 1; i++) {
            Point sectStart = snakeBody.get(i);
            Point sectEnd = snakeBody.get(i + 1);
            int xlower = Math.min(sectStart.x, sectEnd.x);
            int xupper = Math.max(sectStart.x, sectEnd.x);
            int ylower = Math.min(sectStart.y, sectEnd.y);
            int yupper = Math.max(sectStart.y, sectEnd.y);
            for (int x = xlower; x <= xupper; x++) {
                for (int y = ylower; y <= yupper; y++) {
                    //ignore the very last one
                    if (x == sectEnd.x && y == sectEnd.y) {
                        //break;
                    }
                    playArea[x][y] = 1;
                }
            }
        }
    }

    private Point toPoint(String str) {
        String[] t = str.split(",");
        return new Point(Integer.parseInt(t[0]), Integer.parseInt(t[1]));
    }

    public void update(String state) {
        playArea = new int[width][height];

        parser.setStateString(state);

        //apples
        String[] appleStrings = parser.getApples();
        apples = new Point[appleStrings.length];
        for (int i = 0; i < apples.length; i++) {
            String split[] = appleStrings[i].split(" ");
            apples[i] = new Point(Integer.parseInt(split[0]), Integer.parseInt(split[1]));
        }

        //zombies
        String[] zombies = parser.getZombies();
        for (String zombie : zombies) {
            String[] temp = zombie.split(" ");
            List<Point> points = new ArrayList<>(temp.length);
            for (String strPoint : temp) {
                points.add(toPoint(strPoint));
            }
            addSnake(points);
        }

        //snakes
        int mySnakeNum = parser.getSnakeId();
        String[] snakeStrings = parser.getSnakes();

        for (int i = 0; i < numSnakes; i++) {
            String line = snakeStrings[i].trim();
            String[] split = line.split(" ");
            String snakeStatus = split[0];
            ArrayList<Point> snakeBody = new ArrayList<>();
            int start = 3;
            if (snakeStatus.equals("invisible")) { //if its invisible, we expect "invisible length kills timeSinceInvisible invisiblePointX,invisiblePointY headX,headY, ..., tailX,tailY" where head to tail is partial information
                start = 5;
            }
            for (int j = start; j < split.length; j++) {
                String pts[] = split[j].split(",");
                snakeBody.add(new Point(Integer.parseInt(pts[0]), Integer.parseInt(pts[1])));
            }

            if (snakeStatus.equals("alive") || snakeStatus.equals("invisible")) {
                addSnake(snakeBody);
            }
            //if this is my snake
            if (i == mySnakeNum) {
                myHead = snakeBody.get(0);
                myTail = snakeBody.get(snakeBody.size() - 1);
            }
        }
    }

}