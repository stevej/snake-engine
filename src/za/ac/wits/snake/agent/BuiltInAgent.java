package za.ac.wits.snake.agent;


import za.ac.wits.snake.Direction;
import za.ac.wits.snake.Parser;
import za.ac.wits.snake.Point;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Dean Wookey on 07-Apr-12.
 */
public class BuiltInAgent extends SnakeAgent {

    private int numSnakes;
    protected int width;
    protected int height;
    private ArrayList<Point> obstacles[];
    private SnakeState currentState;

    private final Parser parser = new Parser();
    private static final Random random = new Random();
    public BuiltInAgent(int agentId, String name) {
        super(agentId, name);
    }

    public BuiltInAgent(String loadString) {
        super(loadString);
    }

    @Override
    public void startEpisode(int gameIdentifier, String input) {
        super.startEpisode(gameIdentifier, input);

        parser.setInitString(input);

        numSnakes = parser.getNumSnakes();
        width = parser.getWidth();
        height = parser.getHeight();
        String[] obstacleStrings = parser.getObstacles();
        int obs = obstacleStrings.length;
        obstacles = new ArrayList[obs];
        for (int i = 0; i < obs; i++) {
            obstacles[i] = new ArrayList<>();
            String line = obstacleStrings[i];
            String pts[] = line.split(" ");
            for (int j = 0; j < pts.length; j++) {
                String pt[] = pts[j].split(",");
                int x = Integer.parseInt(pt[0]);
                int y = Integer.parseInt(pt[1]);
                obstacles[i].add(new Point(x, y));
            }
        }
    }

    @Override
    public void acceptState(String state) {
        super.acceptState(state);
        currentState = new SnakeState(state, numSnakes, width, height);
        for (ArrayList<Point> p : obstacles) {
            currentState.addSnake(p);
        }
    }

    @Override
    public Direction getMove() {

        Point apple = getClosestApple(currentState.myHead, currentState.apples);
        if (apple == null) {
            apple = currentState.myTail;
        }
        Point p = getClosest(currentState, currentState.myHead, apple);
        if (p == null) {
            return Direction.STRAIGHT;
        }
        Direction dir = dirFrom(currentState.myHead, p);
        return dir;
    }


    public Point getClosestApple(Point snake, Point[] apples) {
        int min = Integer.MAX_VALUE;
        Point best = null;
        for (Point apple : apples) {
            if (apple.x >= 0 && apple.y >= 0) {
                int dist = manhattanDistance(apple, snake);
                if (dist < min) {
                    min = dist;
                    best = apple;
                }
            }
        }
        if (best == null) return null;
        return new Point(best.x, best.y);
    }

    protected final int manhattanDistance(Point a, Point b) {
        return Math.abs(a.x - b.x) + Math.abs(a.y - b.y);
    }

    protected final Point[] getNeighbours(Point p) {
        return new Point[]{
                new Point(p.x - 1, p.y),
                new Point(p.x + 1, p.y),
                new Point(p.x, p.y - 1),
                new Point(p.x, p.y + 1),

        };
    }


    protected Point getClosest(SnakeState state, Point head, Point target) {
        double min = Double.POSITIVE_INFINITY;
        Point best = null;
        Point[] points = getNeighbours(head);
        for (Point p : points) {
            if (p.x < 0 || p.x >= width || p.y < 0 || p.y >= height) {
                continue;
            }
            if (state.playArea[p.x][p.y] == 1) {
                continue;
            }
            double d = manhattanDistance(p, target);
            if (d < min) {
                min = d;
                best = p;
            }
        }
        return best;
    }

    private Direction dirFrom(Point from, Point to) {
        if (from.x == to.x) {
            if (from.y > to.y) {
                return Direction.NORTH;
            } else {
                return Direction.SOUTH;
            }
        } else {
            if (from.x > to.x) {
                return Direction.WEST;
            } else {
                return Direction.EAST;
            }
        }

    }
}

