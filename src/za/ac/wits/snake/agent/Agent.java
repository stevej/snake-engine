package za.ac.wits.snake.agent;

import za.ac.wits.snake.Direction;

/**
 * Created by Dean Wookey on 06-Apr-12.
 */
public interface Agent {

    String getName();

    void startEpisode(int gameId, String input);

    void acceptState(String input);

    Direction getMove();

    void endEpisode();

}
