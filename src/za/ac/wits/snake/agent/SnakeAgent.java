package za.ac.wits.snake.agent;

import za.ac.wits.snake.config.Config;
import za.ac.wits.snake.Parser;


/**
 * Created by Steve on 07-Apr-16.
 */
public abstract class SnakeAgent implements Agent, Comparable<SnakeAgent> {

    private int id = -1;
    private int gameId = -1;
    private int gameMode = -1;
    private boolean alive = false;
    private boolean invisible = false;
    private int currentLength = 0;
    private int longestLength = 0;
    private int totalKills = 0;
    private String name;
    private int rank;
    private double weightedScore = 0;
    private double eloRating = 1600;

    private final Parser parser = new Parser();

    public SnakeAgent(int agentID, String name) {
        this.id = agentID;
        this.name = name.replace(" ", "_");
    }

    public SnakeAgent(String loadString) {
        String split[] = loadString.split(">");
        this.id = Integer.parseInt(split[0]);
        this.name = split[1];
        this.totalKills = Integer.parseInt(split[2]);
        if (split.length > 3) {
            this.weightedScore = Double.parseDouble(split[3]);
        }
        if (split.length > 4) {
            this.longestLength = Integer.parseInt(split[4]);
        }
        if (split.length > 5) {
            this.eloRating = Double.parseDouble(split[5]);
        }

    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void startEpisode(int gameId, String input) {
        totalKills = 0;
        longestLength = 0;
        currentLength = 0;
        this.gameId = gameId;
        parser.setInitString(input);
        gameMode = parser.getMode();
    }

    @Override
    public void acceptState(String input) {
        parser.setStateString(input);
        int agentNum = parser.getSnakeId();
        String snakeLine = parser.getSnakes()[agentNum];
        String[] split = snakeLine.split(" ");
        alive = "alive".equals(split[0]);
        invisible = "invisible".equals(split[0]);
        currentLength = Integer.parseInt(split[1]);
        totalKills = Integer.parseInt(split[2]);
        longestLength = Math.max(longestLength, currentLength);
    }

    @Override
    public void endEpisode() {
        //no-op
    }

    @Override
    public String toString() {
        return String.format("%d>%s>%d>%f>%d>%f",
                id, name, totalKills, weightedScore, longestLength, eloRating);
    }

    public int getGameId() {
        return this.gameId;
    }

    public boolean isAlive() {
        return alive;
    }

    public boolean isInvisible() {
        return invisible;
    }

    public int getKills() {
        return totalKills;
    }

    public int getId() {
        return this.id;
    }

    public double getWeightedScore() {
        return weightedScore;
    }

    public double getEloRating() {
        return eloRating;
    }

    public void incrementScore(double score) {
        weightedScore = weightedScore + (1.0 / 80) * (score - weightedScore);
    }

    public void setGameId(int id) {
        this.gameId = id;
    }


    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getRank() {
        return rank;
    }

    public int getLongest() {
        return longestLength;
    }

    public void setLongest(int longest) {
        this.longestLength = longest;
    }

    public String getLogs() {
        return "";
    }

    public String getErrorLogs() {
        return "";
    }

    @Override
    public int compareTo(SnakeAgent other) {
        //first compare based on division
        if (other.getGameId() > getGameId()) {
            return -1;
        } else if (other.getGameId() < getGameId()) {
            return 1;
        }

        switch (Config.Mode.values()[gameMode]) {
            case GROW:
                return compareLength(other);
            case DEATHMATCH:
                return compareDeathMatch(other);
            case KILL:
                return compareKillCount(other);
            case SURVIVE:
                return compareSurvival(other);
            default:
                assert false;
        }
        return 0;
    }

    private int compareSurvival(SnakeAgent other) {
        if (isAlive() && !other.isAlive()) {
            return -1;
        }
        if (!isAlive() && other.isAlive()) {
            return 1;
        }
        return 0;
    }

    private int compareLength(SnakeAgent other) {
        if (longestLength == other.longestLength) {
            return compareKillCount(other);
        }
        return Integer.compare(other.longestLength, longestLength);
    }

    private int compareKillCount(SnakeAgent other) {
        return Integer.compare(other.getKills(), getKills());
    }

    private int compareDeathMatch(SnakeAgent other) {
        if (isAlive() && !other.isAlive()) {
            return -1;
        }
        if (!isAlive() && other.isAlive()) {
            return 1;
        }
        return compareKillCount(other);
    }


    public void updateElo(double result, SnakeAgent opponent) {
        final int K = 5;
        double qA = Math.exp(eloRating / 400);
        double qB = Math.exp(opponent.eloRating / 400);
        double eA = qA / (qA + qB);
        double delta = K * (result - eA);
        eloRating += delta;
        eloRating = Math.min(Math.max(400, eloRating), 4500);
    }

    public final String getType(){
        return getClass().getSimpleName();
    }

}
