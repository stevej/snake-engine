package za.ac.wits.snake.agent;

import za.ac.wits.snake.Direction;
import za.ac.wits.snake.process.DaemonThreadFactory;
import za.ac.wits.snake.process.Program;
import za.ac.wits.snake.process.langauge.Language;
import za.ac.wits.snake.process.langauge.ProgramFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by  Dean Wookey on 07-Apr-12.
 */
public class ExternalAgent extends SnakeAgent {

    //todo maybe we shouldn't use this at all
    private static final ExecutorService THREAD_POOL = Executors.newCachedThreadPool(new DaemonThreadFactory());

    private static final String ERROR_FILE = "err.txt";
    private static final String OUTPUT_FILE = "out.txt";


    private String filename = "";
    private String directory = "";
    private String execString[];

    private File errorFile;
    private File outputFile;

    private String language = "";
    private boolean invalid = false;

    private long timestamp;
    private Program process;

    private ExternalAgent(int id, String username) {
        super(id, username);
    }

    private ExternalAgent(String loadString) {
        super(loadString.split("\\*")[1]);
    }


    public static ExternalAgent build(String userName, String userId, String folderPath, String language, String source) {

        ExternalAgent agent = new ExternalAgent(Integer.parseInt(userId), userName);

        agent.directory = folderPath;
        File path = new File(folderPath);
        if (!path.exists()) {
            path.mkdirs();
        }
        if (!path.exists()) {
            agent.invalid = true;
        }
        try {
            /* 1 => 'Jar (snake)',
               2 => 'Python 2.6 (snake)',
               3 => 'Jython (snake)',
               4 => 'C++ (snake)',
               5 => 'C (snake)'
             */
            switch (language) {
                case "1":
                case "7":
                    agent.language = "jar";
                    break;
                case "2":
                    agent.language = "py";
                    break;
                case "3":
                    //not doing this
                    throw new RuntimeException("Not allowing Jython");
                case "4":
                case "5":
                    agent.language = "cpp";
                    break;
                default:
                    agent.language = language;
                    break;
            }
            ProgramFile program = ProgramFile.create(userId, agent.directory, source.getBytes(), Language.get(agent.language));
            agent.execString = program.getExecCommand();
            agent.filename = program.getPath();
            File file = new File(agent.filename);
            agent.directory = file.getParent();
            agent.timestamp = System.currentTimeMillis();

            agent.errorFile = new File(agent.directory, ERROR_FILE);
            agent.outputFile = new File(agent.directory, OUTPUT_FILE);

        } catch (IOException ex) {
            throw new RuntimeException("Unable to create program. Permissions issue?", ex);
        }
        return agent;

    }

    public static ExternalAgent load(String loadString, long timestamp) {
        ExternalAgent agent = new ExternalAgent(loadString);
        String split[] = loadString.split("\\*");
        split = split[0].split("\\%");
        agent.filename = split[0];
        agent.directory = split[1];
        agent.errorFile = new File(agent.directory, ERROR_FILE);
        agent.outputFile = new File(agent.directory, OUTPUT_FILE);
        agent.language = split[2];
        String exec = split[3];
        agent.execString = exec.split("~");
        agent.timestamp = timestamp;
        return agent;

    }

    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        String str = execString == null ? "" : String.join("~", execString);
        return "External#" + filename + "%" + directory + "%" + language + "%" + str + "*" + super.toString();
    }

    public boolean removeLogs() {
        //no short circuit!
        return errorFile.delete() & outputFile.delete();
    }

    @Override
    public void startEpisode(int gameIdentifier, String initString) {
        System.out.println("Episode Started");
        super.startEpisode(gameIdentifier, initString);
        process = null;
        //killAgent();
        if (invalid) {
            return;
        }
        process = new Program(execString, outputFile.getAbsolutePath(), errorFile.getAbsolutePath());
        THREAD_POOL.execute(process);

        //give one second to start up before killing
        long time = System.currentTimeMillis();
        while (!isRunning()) {

            if (System.currentTimeMillis() - time > 1000) {
                System.out.println("Not running");
                return;
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        process.provideInput(initString);

    }

    @Override
    public void acceptState(String state) {
        super.acceptState(state);
        process.provideInput(state);
    }

    @Override
    public Direction getMove() {
        if (!isRunning()) {
            //agent died.
            return Direction.NONE;
        }
        String line = process.getOutput().trim();
        if (line.isEmpty()) {
            return Direction.STRAIGHT;
        }
        String split[] = line.split("\n");
        line = split[split.length - 1].trim();
        Direction dir = Direction.fromString(line);
        return dir == Direction.NONE ? Direction.STRAIGHT : dir;
    }

    @Override
    public void endEpisode() {
        super.endEpisode();
        process.provideInput("Game Over\n");
        try {
            Thread.sleep(500);
        } catch (InterruptedException ex) {
            Logger.getLogger(ExternalAgent.class.getName()).log(Level.SEVERE, null, ex);
        }
        killAgent();
    }

    public void killAgent() {
        if (process != null) {
            process.close();
        }
    }

    public boolean isRunning() {
        if (process == null) {
            return false;
        }
        return process.isRunning();
    }


    @Override
    public String getLogs() {
        if (!outputFile.exists()) {
            return "No log found";
        }
        try {
            return String.join(System.lineSeparator(), Files.readAllLines(outputFile.toPath()));
        } catch (IOException e) {
            e.printStackTrace();
            return "No log found";
        }
    }


    @Override
    public String getErrorLogs() {
        if (!errorFile.exists()) {
            return "No log found";
        }
        try {
            return String.join(System.lineSeparator(), Files.readAllLines(errorFile.toPath()));
        } catch (IOException e) {
            e.printStackTrace();
            return "No log found";
        }
    }

    public String getFilename() {
        return filename;
    }

    public String getDirectory() {
        return directory;
    }

    public String getLanguage() {
        return language;
    }

    public String getExecString() {
        return String.join("~", execString);
    }
}
