package za.ac.wits.snake;

/**
 * Created by Steve James on 06-Apr-16.
 */
public class Apple {

    private Point point;
    private final int timeLimit;
    private boolean eaten;
    private int currentTime;

    public Apple(int x, int y, int timeLimit) {
        point = new Point(x, y);
        eaten = false;
        this.timeLimit = timeLimit;
        currentTime = 0;
    }

    public void reset(int x, int y) {
        point = new Point(x, y);
        eaten = false;
        currentTime = 0;
    }

    public final int getX() {
        return point.x;
    }

    public final int getY() {
        return point.y;
    }

    public void advanceTimeStep() {
        ++currentTime;
        if (currentTime == timeLimit) {
            markEaten();
        }
    }

    public final int markEaten() {
        eaten = true;
        return currentTime;
    }

    public final boolean isEaten() {
        return eaten;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Apple) {
            return point.equals(((Apple) o).point);
        }
        return point.equals(o);
    }

    @Override
    public int hashCode() {
        return point.hashCode();
    }

    @Override
    public String toString() {
        return getX() + " " + getY();
    }
}
