package za.ac.wits.snake;

/**
 * Created by Steve James on 06-Apr-16.
 */
public class Point {

    public final int x;
    public final int y;

    public Point() {
        x = -1;
        y = -1;
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }


    public final double distSq(final Point b) {
        return (x - b.x) * (x - b.x) + (y - b.y) * (y - b.y);
    }

    public final double dist(final Point b) {
        return Math.sqrt(distSq(b));
    }


    @Override
    public String toString() {
        return String.format("%d,%d", x, y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        if (x != point.x) return false;
        return y == point.y;

    }

    @Override
    public int hashCode() {
        return (int) (y + 0.5 * (x + y) * (x + y + 1));
    }
}