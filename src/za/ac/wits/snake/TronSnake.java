package za.ac.wits.snake;

/**
 * Created by Steve James on 20-Jun-17.
 */
public class TronSnake extends Snake {

    public TronSnake(final Snake other){
        super(other.getHead(), other.getTail(), other.getId());
    }

    @Override
    public int getTimeToRespawn() {
        //ensures we never respawn
        return 1;
    }

    @Override
    public void advance() {
        //do nothing, since we don't respawn
    }

    @Override
    public boolean isGrowing() {
        //always growing
        return true;
    }
}
