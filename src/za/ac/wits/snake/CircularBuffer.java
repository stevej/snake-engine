package za.ac.wits.snake;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Deque;

/**
 * Created by Steve James on 07-Apr-16.
 */
public class CircularBuffer<T> {

    private final Deque<T> buffer;
    private final int capacity;

    public CircularBuffer(int capacity) {
        this.capacity = capacity;
        buffer = new ArrayDeque<>(capacity);
    }

    public boolean isFilled() {
        return size() == capacity();
    }

    public int size() {
        return buffer.size();
    }

    public int capacity() {
        return capacity;
    }

    public void push(final T element) {
        if (size() == capacity) {
            pop();
        }
        buffer.addLast(element);
    }

    public T pop() {
        return buffer.pollFirst();
    }

    public T peek() {
        return buffer.peekFirst();
    }

    public void clear() {
        buffer.clear();
    }

    public Collection<T> toCollection() {
        return buffer;
    }
}
