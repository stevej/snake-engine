package za.ac.wits.snake.client.client;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class SnakeGameImpl implements SnakeGame {

    public Queue<StateHolder> queue;
    public static int MIN_STATES = 500;
    StateGetter sg;
    int gameNumber = 0;
    int currentStateIndex = 0;

    public SnakeGameImpl(int id, List<Agent> agents) {
        queue = new LinkedList<>();
        sg = new StateGetter();
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
    }

    @Override
    public String getNextState() {

        sg.getNewStates(queue, gameNumber);
        if (queue.size() > 0) {
            StateHolder sh = queue.remove();
//			GWT.log("Queue size : " + queue.size() + "\t Item : "
            //				+ sh.getIndex());
            String result = sh.getState().replace("\"", "")
                    .replace("\\n", "\n");
            // GWT.log(result);
            currentStateIndex = sh.getIndex();
            return result;
        } else {
            return null;
        }
    }

    public void setGame(int number) {
        if (number != gameNumber) {
            gameNumber = number;
            queue.clear();
            sg.refresh();
        }
    }

    @Override
    public long getTimeElapsed() {
        // TODO Auto-generated method stub
        return currentStateIndex;
    }

    @Override
    public long getGameDuration() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public boolean isRunning() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public int getWidth() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int getHeight() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void updateStandings() {
        // TODO Auto-generated method stub

    }

    @Override
    public String getStandings() {
        // TODO Auto-generated method stub
        return "Something\nSomeotherThing";
    }

    @Override
    public String getAgentNames() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setOnCompleted(Runnable onCompleted) {
        // TODO Auto-generated method stub

    }

    @Override
    public void attachServer(ExternalServer recorder) {
        // TODO Auto-generated method stub

    }

    @Override
    public int getMode() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int getActionTime() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public List<Agent> getAgents() {
        // TODO Auto-generated method stub
        return null;
    }

}
