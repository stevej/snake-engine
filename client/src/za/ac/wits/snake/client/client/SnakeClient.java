package za.ac.wits.snake.client.client;

import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.http.client.Response;
import com.google.gwt.json.client.*;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class SnakeClient implements EntryPoint {

    private static final String API_ROOT = "http://marker.ms.wits.ac.za:8080/SnakeService";

    private static int leagueId = 0;

    private static boolean stopRequests = false;

    public static void setLeagueId(int leagueId) {
        if (SnakeClient.leagueId == leagueId) {
            return;
        }
        SnakeClient.leagueId = leagueId;
        if (stopRequests) {
            UI.getInstance().hideError();
            stopRequests = false;
        }
        //this is a massive hack, but don't really care now
        RemoteConfig.initOnLoad(null);
        RemoteConfig.getInstance().load();
        UI.getInstance().updateTable();
    }

    public static int getLeagueId() {
        return leagueId;
    }

    public void onModuleLoad() {
        RemoteConfig.initOnLoad(new Callback<Void, Throwable>() {
            @Override
            public void onFailure(Throwable reason) {
                stopRequests = true;
                UI.getInstance().loadUI();
                UI.getInstance().showError(reason.getMessage());

            }

            @Override
            public void onSuccess(Void result) {
                UI.getInstance().loadUI();
                initLeagueCount();
            }
        });

        RemoteConfig.getInstance();
    }


    public static String buildUrl(Object... paths) {
        StringBuilder sb = new StringBuilder(API_ROOT);
        for (Object path : paths) {
            sb.append('/').append(path);
        }
        return sb.toString();
    }


    public static boolean hasAborted() {
        return stopRequests;
    }

    public static void setStopRequests(boolean stopRequests) {
        SnakeClient.stopRequests = stopRequests;
    }


    private void initLeagueCount() {

        new GetRequest().makeRequest(buildUrl("leagues"), new Callback<Response, Throwable>() {
            @Override
            public void onFailure(Throwable reason) {
                UI.getInstance().showError("Unable to get number of leagues: <br/>" + reason.getMessage());
                stopRequests = true;
            }

            @Override
            public void onSuccess(Response response) {
                if (response.getStatusCode() == 200) {
                    try {

                        JSONObject json = (JSONObject) JSONParser.parseStrict(response.getText());
                        int N = (int) ((JSONNumber) json.get("count")).doubleValue();
                        UI.getInstance().setNumberOfLeagues(N);
                        if (N == 0) {
                            UI.getInstance().showError("No leagues in progress");
                        } else {
                            String value = com.google.gwt.user.client.Window.Location.getParameter("league");
                            try {
                                int n = Integer.parseInt(value);
                                UI.getInstance().setLeague(n);
                                setLeagueId(n);
                            } catch (NumberFormatException e) {
                            }

                            value = com.google.gwt.user.client.Window.Location.getParameter("division");
                            try {
                                int n = Integer.parseInt(value);
                                UI.getInstance().setDivision(n);
                            } catch (NumberFormatException e) {
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    UI.getInstance().showError("Unable to get number of leagues: <br/>" + response.getStatusCode());
                    stopRequests = true;
                }
            }
        });


    }


}

