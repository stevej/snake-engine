package za.ac.wits.snake.client.client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.*;

public class PlayerTable extends CellTable<Player> {


    private TextColumn<Player> nameColumn;
    private TextColumn<Player> scoreColumn;
    private TextColumn<Player> eloColumn;
    private TextColumn<Player> divisionColumn;
    private GameStateProcessor gsp;
    private PlayerGetter getter = new PlayerGetter();

    private final List<Player> data = new ArrayList<>();
    private final ListDataProvider<Player> dataProvider = new ListDataProvider<>();

    public void refresh(List<Player> players) {

        int nDivisions = players.size() / RemoteConfig.getInstance().getSnakes();
        if (players.size() % RemoteConfig.getInstance().getSnakes() > 0) {
            nDivisions++;
        }
        UI.getInstance().setNumberOfDivisions(nDivisions);
        GWT.log("Num players: " + players.size());
        GWT.log("Num divs: " + nDivisions);
        data.clear();
        data.addAll(players);
        dataProvider.refresh();
        setRowCount(players.size(), true);
        GWT.log("Rows: " + getRowCount());
        ColumnSortEvent.fire(this, this.getColumnSortList());

    }

    public void update() {
        getter.getPlayers(this);
    }

    public PlayerTable(GameStateProcessor link) {
        gsp = link;
        // Create a CellTable.

        // Create name column.
        nameColumn = new TextColumn<Player>() {
            @Override
            public String getValue(Player contact) {
                return contact.name;
            }
        };
        scoreColumn = new TextColumn<Player>() {
            @Override
            public String getValue(Player contact) {
                return contact.score;
            }
        };
        eloColumn = new TextColumn<Player>() {
            @Override
            public String getValue(Player contact) {
                return contact.elo;
            }
        };
        divisionColumn = new TextColumn<Player>() {
            @Override
            public String getValue(Player contact) {
                return contact.division;
            }
        };
        nameColumn.setSortable(true);
        scoreColumn.setSortable(true);
        eloColumn.setSortable(true);
        divisionColumn.setSortable(true);

        // Add the columns.
        addColumn(nameColumn, "Name");
        addColumn(scoreColumn, "Score");
        addColumn(eloColumn, "ELO");
        addColumn(divisionColumn, "Division");


        addRowCountChangeHandler(new RowCountChangeEvent.Handler() {
            @Override
            public void onRowCountChange(RowCountChangeEvent event) {
                setVisibleRange(new Range(0, event.getNewRowCount()));
            }
        });

        dataProvider.setList(data);
        ListHandler<Player> sortHandler = getSorter(dataProvider);
        addColumnSortHandler(sortHandler);
        dataProvider.addDataDisplay(this);
        // Set the total row count. This isn't strictly necessary, but it
        // affects
        // paging calculations, so its good habit to keep the row count up to
        // date.
        dataProvider.refresh();
        update();
        getColumnSortList().push(divisionColumn);

    }

    public ListHandler<Player> getSorter(ListDataProvider<Player> dataProvider) {
        ListHandler<Player> sortHandler = new ListHandler<Player>(
                dataProvider.getList());
        sortHandler.setComparator(nameColumn, new Comparator<Player>() {
            @Override
            public int compare(Player o1, Player o2) {
                return o1.name.compareTo(o2.name);
            }
        });

        sortHandler.setComparator(scoreColumn, new Comparator<Player>() {
            @Override
            public int compare(Player o1, Player o2) {
                return (int) ((Double.parseDouble(o1.score) - Double
                        .parseDouble(o2.score)) * 1000);
            }
        });

        sortHandler.setComparator(eloColumn, new Comparator<Player>() {
            @Override
            public int compare(Player o1, Player o2) {
                return (int) ((Double.parseDouble(o1.elo) - Double
                        .parseDouble(o2.elo)) * 1000);
            }
        });

        sortHandler.setComparator(divisionColumn, new Comparator<Player>() {
            @Override
            public int compare(Player o1, Player o2) {
                return (Integer.parseInt(o1.division) - Integer
                        .parseInt(o2.division));
            }
        });

        return sortHandler;
    }

}