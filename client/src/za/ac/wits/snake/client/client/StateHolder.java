package za.ac.wits.snake.client.client;

import com.google.gwt.json.client.JSONException;
import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;


public class StateHolder {

    private final int index; //the state index within the current round
    private final long globalIndex; //the state index in the context of the entire competition lifecycle
    private final String state;

    public StateHolder(int index, long globalIndex, String state) {
        this.index = index;
        this.globalIndex = globalIndex;
        this.state = state;
    }


    public static StateHolder get(JSONObject jo) throws JSONException {
        JSONNumber index = (JSONNumber) jo.get("index");
        JSONNumber globalIndex = (JSONNumber) jo.get("globalIndex");
        JSONString state = (JSONString) jo.get("state");

        return new StateHolder((int) index.doubleValue(), (long) globalIndex.doubleValue(), state.stringValue());
    }

    public String getState() {
        return state;
    }

    public int getIndex() {
        return index;
    }

    public long getGlobalIndex() {
        return globalIndex;
    }
}
