package za.ac.wits.snake.client.client;

import java.util.List;

/**
 * Created by Steve James on 13-May-16.
 */
public interface SnakeGame extends Runnable {

    String getNextState();

    long getTimeElapsed();

    long getGameDuration();

    boolean isRunning();

    int getWidth();

    int getHeight();

    void updateStandings();

    String getStandings();

    String getAgentNames();

    void setOnCompleted(Runnable onCompleted);

    void attachServer(final ExternalServer recorder);

    int getMode();
    
    void setGame(int game);

    int getActionTime();

    List<Agent> getAgents();
}
