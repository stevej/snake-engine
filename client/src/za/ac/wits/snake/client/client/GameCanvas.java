package za.ac.wits.snake.client.client;

import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;

public abstract class GameCanvas {

    protected Canvas canvas;
    protected Context2d context;
    protected int width;
    protected int height;

    public GameCanvas(int width, int height, int redrawInterval) {
        this.width = width;
        this.height = height;
        canvas = Canvas.createIfSupported();
        if (canvas == null) {
            RootPanel.get().add(new Label("Sorry, your browser doesn't support the HTML5 Canvas element"));
            return;
        }

        canvas.setWidth(width + "px");
        canvas.setCoordinateSpaceWidth(width);

        canvas.setHeight(height + "px");
        canvas.setCoordinateSpaceHeight(height);

        context = canvas.getContext2d();

        final Timer timer = new Timer() {
            @Override
            public void run() {
                drawNextFrame();
            }
        };
        timer.scheduleRepeating(redrawInterval);

    }

    public abstract void drawNextFrame();

    public Canvas getCanvas() {
        return canvas;
    }

}
