package za.ac.wits.snake.client.client;

import com.google.gwt.core.client.Callback;
import com.google.gwt.http.client.*;

/**
 * Created by Steve James on 29-Apr-17.
 */
public class GetRequest {


    protected final void makeRequest(String url, final Callback<Response, Throwable> callback) {
        RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, URL.encode(url));
        try {
            builder.sendRequest(null,
                    new RequestCallback() {
                        public void onError(Request request, Throwable exception) {
                            // Couldn't connect to server (could be timeout,
                            // SOP
                            // violation, etc.)
                            callback.onFailure(exception);
                        }

                        public void onResponseReceived(Request request, Response response) {
                            callback.onSuccess(response);
                        }
                    });
        } catch (RequestException e) {
            // Couldn't connect to server
            callback.onFailure(e);
        }
    }
}
