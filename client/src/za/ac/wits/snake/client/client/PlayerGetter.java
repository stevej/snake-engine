package za.ac.wits.snake.client.client;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.Callback;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONException;
import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;

public class PlayerGetter extends GetRequest {


    public void getPlayers(final PlayerTable display) {
        final ArrayList<Player> ap = new ArrayList<>();

        String url = SnakeClient.buildUrl("agents", SnakeClient.getLeagueId());
        makeRequest(url, new Callback<Response, Throwable>() {
            @Override
            public void onFailure(Throwable reason) {

            }

            @Override
            public void onSuccess(Response response) {
                if (response.getStatusCode() == 200) {
                    try {
                        JSONArray array = (JSONArray) JSONParser
                                .parseStrict(response.getText());
                        for (int i = 0; i < array.size(); i++) {
                            JSONObject player = (JSONObject) array.get(i);
                            ap.add(Player.get(player));
                        }
                        display.refresh(ap);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    // Handle the error. Can get the status text
                    // from
                    // response.getStatusText()
                }
            }
        });

    }

}
