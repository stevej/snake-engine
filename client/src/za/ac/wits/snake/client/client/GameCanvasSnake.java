package za.ac.wits.snake.client.client;


public class GameCanvasSnake extends GameCanvas {

    private final GameStateProcessor gameStateProcessor;
    private final Graphics graphics;

    public GameCanvasSnake(int width, int height, int redrawInterval, GameStateProcessor gameStateProcessor) {
        super(width, height, redrawInterval);
        this.gameStateProcessor = gameStateProcessor;
        this.graphics = new Graphics(context);
    }

    public void drawNextFrame() {

        if (SnakeClient.hasAborted()) {
            return;
        }
        gameStateProcessor.processNextState(graphics);
        graphics.fill();
    }
}
