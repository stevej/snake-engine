package za.ac.wits.snake.client.client;

import com.google.gwt.canvas.dom.client.CssColor;

public class Color{
	CssColor current;

	public static final Color GRAY = new Color(120, 120, 120);

	public static final Color DARK_GRAY =  new Color(50, 50, 50);

	public static final Color BLACK =  new Color(0, 0, 0);

	public static final Color WHITE = new Color(255, 255, 255);

	public static final Color CYAN = new Color(0, 255, 255);

	public static final Color RED =  new Color(255, 0, 0);

	public static final Color GREEN = new Color(0, 255, 0);

	public static final Color BLUE = new Color(0, 0, 255);

	public static final Color ORANGE = new Color(255, 128, 0);

	public static final Color MAGENTA = new Color(255, 0, 255);

	public static final Color PINK = new Color(255, 120, 120);

	public static final Color YELLOW = new Color(255, 255, 0);


	public Color(int r, int g, int b){
		current =  CssColor.make(r,g,b);
	}

	public CssColor getColor(){
		return current;
	}

}