package za.ac.wits.snake.client.client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import com.google.gwt.user.client.ui.Image;


public class GameStateProcessor {
    private static final int SCREEN_WIDTH = 540;
    private static final int SCREEN_HEIGHT = 520;
    private static final int X_OFFSET = 10;
    private static final int Y_OFFSET = 10;
    private SnakeGame game;

    private GamePainter pane;

    private Parser parser;

    private String[] standings;
    private String previousStandings;


    public GameStateProcessor(SnakeGame sg) {
        game = sg;
        parser = new Parser();
        pane = new GamePainter(sg);
    }

    public void setGame(int gnum) {
        game.setGame(gnum);
    }

    public SnakeGame getGame() {
        return game;
    }

    public void processNextState(Graphics output) {

        String state = game.getNextState();
        if (state == null) {
            return;
        }
        if (state.equals("Game Starting")) {
            UI.getInstance().updateTable();
            return;
        } else if (state.startsWith("Game Over")) {
            //display results!
            return;
        }
        parser.setStateString(state);
        String[] appleStrings = parser.getApples();
        Point[] apples = new Point[appleStrings.length];
        for (int i = 0; i < apples.length; i++) {
            apples[i] = readApple(appleStrings[i]);
        }
        String[] snakeStrings = parser.getSnakes();
        SimpleSnake snakes[] = new SimpleSnake[snakeStrings.length];
        for (int i = 0; i < snakes.length; i++) {
            snakes[i] = readSnake(snakeStrings[i], i);
        }
        String[] obstacleStrings = parser.getObstacles();
        SimpleSnake obstacles[] = new SimpleSnake[obstacleStrings.length];
        for (int i = 0; i < obstacles.length; i++) {
            obstacles[i] = readObstacle(obstacleStrings[i]);
        }
        String[] zombieStrings = parser.getZombies();
        SimpleSnake zombies[] = new SimpleSnake[zombieStrings.length];
        for (int i = 0; i < zombies.length; i++) {
            zombies[i] = readZombies(zombieStrings[i]);
        }
        pane.setParams(RemoteConfig.getInstance().getGameWidth(), RemoteConfig.getInstance().getGameHeight(), apples, snakes, obstacles, zombies);
        pane.paintGame(output);
    }


    private static final Color[] COLOURS = {Color.RED, Color.GREEN,
            Color.BLUE, Color.ORANGE, Color.PINK, Color.WHITE, Color.MAGENTA,
            Color.CYAN, Color.YELLOW};
    private static final Color[] STANDINGS_COLOURS = {Color.PINK, Color.GREEN,
            Color.CYAN, Color.ORANGE, Color.PINK, Color.WHITE, Color.MAGENTA,
            Color.CYAN, Color.YELLOW};

    private SimpleSnake readObstacle(String line) {
        String split[] = line.split(" ");
        ArrayList<Point> obsBody = new ArrayList<Point>();
        for (int i = 0; i < split.length; i++) {
            String sp[] = split[i].split(",");
            obsBody.add(new Point(Integer.parseInt(sp[0]), Integer.parseInt(sp[1])));
        }
        return new SimpleSnake(obsBody, "obstacle", -1, 5, 5, 0, true, false);
    }

    private SimpleSnake readZombies(String line) {
        String split[] = line.split(" ");
        ArrayList<Point> obsBody = new ArrayList<Point>();
        for (int i = 0; i < split.length; i++) {
            String sp[] = split[i].split(",");
            obsBody.add(new Point(Integer.parseInt(sp[0]), Integer.parseInt(sp[1])));
        }
        return new SimpleSnake(obsBody, "Zombie", -1, 5, 5, 0, true, false);
    }

    private Point readApple(String line) {
        String[] temp = line.split(" ");
        int x = Integer.parseInt(temp[0]);
        int y = Integer.parseInt(temp[1]);
        return new Point(x, y);
    }

    private SimpleSnake readSnake(String snakeLine, int number) {
        String split[] = snakeLine.split(" ");
        String snakeStatus = split[3];
        String name = split[2];
        int snakeLength = Integer.parseInt(split[4]);
        int snakeLongest = Integer.parseInt(split[0]);
        int snakeKills = Integer.parseInt(split[5]);

        ArrayList<Point> snakeBody = new ArrayList<Point>();

        int start = 6;
        if (snakeStatus.equals("invisible")) {
            start += 2;
        }
        for (int j = start; j < split.length; j++) {
            String pts[] = split[j].split(",");
            snakeBody.add(new Point(Integer.parseInt(pts[0]), Integer.parseInt(pts[1])));
        }
        boolean alive = false;
        boolean invisible = false;
        if (snakeStatus.equals("alive")) {
            alive = true;
        } else if (snakeStatus.equals("invisible")) {
            alive = true;
            invisible = true;
        }
        return new SimpleSnake(snakeBody, name, number, snakeLength, snakeLongest, snakeKills, alive, invisible);
    }


    private class GamePainter {

        int width;
        int height;
        Point[] apples;
        SimpleSnake[] snakes;
        SimpleSnake[] obstacles;
        SimpleSnake[] zombies;
        SnakeGame sg;
        //	Image myImage = new Image("grasstexture.png");
        Image appleRed = new Image("redapple.png");
        Image appleBlue = new Image("blueapple.png");

        public GamePainter(SnakeGame sg) {
            this.sg = sg;
        }

        public void paintGame(Graphics buffer) {
            //		super.paintComponent(buffer);
            setBackground(buffer, Color.DARK_GRAY);
            if (width <= 0) {
                return;
            }
            int ratioX = 500 / width;
            int ratioY = 500 / height;
            buffer.setColor(Color.BLACK);
            buffer.fillRect(X_OFFSET - 1, Y_OFFSET - 1, 507, 507);

            buffer.setColor(Color.GRAY);
            buffer.fillRect(X_OFFSET, Y_OFFSET, 505, 505);

            buffer.setBGPen();
            buffer.setAlpha(0.3);
            buffer.fillRect(X_OFFSET, Y_OFFSET, 505, 505);
            buffer.setAlpha(1);


            for (int i = 0; i < snakes.length; i++) {
                if (snakes[i].alive) {
                    buffer.setAlpha(0.4);
                    drawDistantShadow(buffer, snakes[i], Color.DARK_GRAY, 5);
                    buffer.setAlpha(1);
                }
            }

            buffer.setColor(Color.BLUE);
            if (apples.length > 0) {
                Point superApple = apples[0];
                buffer.drawImage(appleBlue, superApple.x * ratioX + X_OFFSET, superApple.y * ratioY + Y_OFFSET);
            }
            buffer.setColor(Color.RED);
            for (int i = 1; i < apples.length; i++) {
                Point apple = apples[i];
                drawApple(buffer, apple.x, apple.y, Color.RED);
                buffer.drawImage(appleRed, apple.x * ratioX + X_OFFSET, apple.y * ratioY + Y_OFFSET);
                /*buffer.fillOval(apple.x * ratioX + X_OFFSET, apple.y * ratioY
                        + Y_OFFSET, ratioX, ratioY);*/
            }


            for (int i = 0; i < snakes.length; i++) {
                if (snakes[i].alive) {
                    drawShadow(buffer, snakes[i], Color.DARK_GRAY);
                    drawHighlight(buffer, snakes[i], Color.WHITE);
                    drawSnake(buffer, snakes[i], COLOURS[i % COLOURS.length], Color.WHITE, Color.BLACK);
                    buffer.setAlpha(0.2);
                    drawMetal(buffer, snakes[i], COLOURS[i % COLOURS.length]);
                    buffer.setAlpha(1);
                }
            }
            for (int i = 0; i < obstacles.length; i++) {
                drawSnake(buffer, obstacles[i], Color.DARK_GRAY, Color.BLACK, Color.BLACK);
            }

            for (int i = 0; i < zombies.length; i++) {
                drawSnake(buffer, zombies[i], Color.BLACK, Color.BLACK, Color.GREEN);
            }
            //	buffer.drawString(""+System.currentTimeMillis(), 20, 20);

            drawStandings(buffer);

            // buffer.dispose();
        }

        private void setBackground(Graphics buffer, Color black) {
            buffer.setColor(black);
            buffer.fillRect(0, 0, 800, 800);
        }

        void setParams(int width, int height, Point[] apples,
                       SimpleSnake[] snakes, SimpleSnake[] obstacles,
                       SimpleSnake[] zombies) {
            this.width = width;
            this.height = height;
            this.apples = apples;
            this.snakes = snakes;
            this.obstacles = obstacles;
            this.zombies = zombies;
        }

        private void drawStandings(Graphics buffer) {
            ArrayList<SimpleSnake> al = new ArrayList<SimpleSnake>();
            for (SimpleSnake s : snakes) {
                al.add(s);
            }

            SimpleSnake[] ss = Arrays.copyOf(snakes, snakes.length);
            Arrays.sort(ss, new Comparator<SimpleSnake>() {
                @Override
                public int compare(SimpleSnake o1, SimpleSnake o2) {
                    return o2.longest - o1.longest;
                }
            });

            int y = 530;
            String[] score = new String[4];
            score[0] = "Max";
            score[1] = "Now";
            score[2] = "Kills";
            score[3] = "Name";
            buffer.setColor(Color.WHITE);
            for (int j = 0; j < score.length; j++) {
                String s = score[j];
                buffer.drawString(s, Y_OFFSET + (j * 40), y);
            }


            long totalStates = RemoteConfig.getInstance().getGameDuration() * 1000 / RemoteConfig.getInstance().getGameSpeed();

            buffer.drawString("State : " + sg.getTimeElapsed() + " / " + totalStates, 400, 530);
            y += 15;

            for (int i = 0; i < snakes.length; i++) {
                score[0] = "" + ss[i].longest;
                score[1] = "" + ss[i].length;
                score[2] = "" + ss[i].kills;
                score[3] = "" + ss[i].name;
                buffer.setColor(STANDINGS_COLOURS[ss[i].number % STANDINGS_COLOURS.length]);
                for (int j = 0; j < score.length; j++) {
                    String s = score[j];
                    buffer.drawString(s, Y_OFFSET + (j * 40), y);
                }
                y += 15;
            }
        }

        private void drawApple(Graphics buffer, int x, int y, Color c) {
            buffer.setColor(c);

        }

        private void drawHalo(Graphics buffer, SimpleSnake s) {
            int ratioX = 500 / RemoteConfig.getInstance().getGameWidth();
            int ratioY = 500 / RemoteConfig.getInstance().getGameHeight();
            for (int i = 1; i < s.body.size(); i++) {
                Point prev = s.body.get(i - 1);
                Point curr = s.body.get(i);
                int topLeftX;
                int topLeftY;
                if (prev.x < curr.x) {
                    topLeftX = prev.x;
                    topLeftY = prev.y;
                } else if (prev.x == curr.x) {
                    if (prev.y < curr.y) {
                        topLeftX = prev.x;
                        topLeftY = prev.y;
                    } else {
                        topLeftX = curr.x;
                        topLeftY = curr.y;
                    }
                } else {
                    topLeftX = curr.x;
                    topLeftY = curr.y;
                }
                int lengthX = Math.abs(prev.x - curr.x) + 1;
                int lengthY = Math.abs(prev.y - curr.y) + 1;
                int x1 = topLeftX * ratioX;
                int y1 = topLeftY * ratioY;
                int x2 = lengthX * ratioX;
                int y2 = lengthY * ratioY;
                buffer.setColor(Color.WHITE);
                buffer.fillRect(x1 + X_OFFSET - 2, y1 + Y_OFFSET - 2, x2 + 4,
                        y2 + 4);
                buffer.setColor(Color.CYAN);
                buffer.fillRect(x1 + X_OFFSET - 1, y1 + Y_OFFSET - 1, x2 + 2,
                        y2 + 2);
            }
        }

        public void drawMetal(Graphics buffer, SimpleSnake s, Color colour) {
            int ratioX = 500 / width;
            int ratioY = 500 / height;
            if (s.power) {
                drawHalo(buffer, s);
            }
            for (int i = 1; i < s.body.size(); i++) {
                Point prev = s.body.get(i - 1);
                Point curr = s.body.get(i);
                int topLeftX;
                int topLeftY;
                if (prev.x < curr.x) {
                    topLeftX = prev.x;
                    topLeftY = prev.y;
                } else if (prev.x == curr.x) {
                    if (prev.y < curr.y) {
                        topLeftX = prev.x;
                        topLeftY = prev.y;
                    } else {
                        topLeftX = curr.x;
                        topLeftY = curr.y;
                    }
                } else {
                    topLeftX = curr.x;
                    topLeftY = curr.y;
                }
                int lengthX = Math.abs(prev.x - curr.x) + 1;
                int lengthY = Math.abs(prev.y - curr.y) + 1;
                int x1 = topLeftX * ratioX + 1;
                int y1 = topLeftY * ratioY + 1;
                int x2 = lengthX * ratioX - 2;
                int y2 = lengthY * ratioY - 2;

                buffer.setSnakeColor(colour);
                buffer.fillRect(x1 + X_OFFSET, y1 + Y_OFFSET, x2, y2);
            }
        }

        public void drawSnake(Graphics buffer, SimpleSnake s, Color colour, Color eyeOutside, Color eyeInside) {
            int ratioX = 500 / width;
            int ratioY = 500 / height;
            if (s.power) {
                drawHalo(buffer, s);
            }
            for (int i = 1; i < s.body.size(); i++) {
                Point prev = s.body.get(i - 1);
                Point curr = s.body.get(i);
                int topLeftX;
                int topLeftY;
                if (prev.x < curr.x) {
                    topLeftX = prev.x;
                    topLeftY = prev.y;
                } else if (prev.x == curr.x) {
                    if (prev.y < curr.y) {
                        topLeftX = prev.x;
                        topLeftY = prev.y;
                    } else {
                        topLeftX = curr.x;
                        topLeftY = curr.y;
                    }
                } else {
                    topLeftX = curr.x;
                    topLeftY = curr.y;
                }
                int lengthX = Math.abs(prev.x - curr.x) + 1;
                int lengthY = Math.abs(prev.y - curr.y) + 1;
                int x1 = topLeftX * ratioX + 1;
                int y1 = topLeftY * ratioY + 1;
                int x2 = lengthX * ratioX - 2;
                int y2 = lengthY * ratioY - 2;

                buffer.setColor(colour);
                buffer.fillRect(x1 + X_OFFSET, y1 + Y_OFFSET, x2, y2);

                buffer.setColor(Color.WHITE);
                if (i == 1) {
                    buffer.setColor(eyeOutside);
                    buffer.fillOval(prev.x * ratioX + X_OFFSET, prev.y * ratioY + Y_OFFSET, 6, 6);
                    buffer.setColor(eyeInside);
                    buffer.fillOval(prev.x * ratioX + X_OFFSET + 1, prev.y * ratioY + Y_OFFSET + 1, 5, 5);
                }
            }
        }

        public void drawShadow(Graphics buffer, SimpleSnake s, Color colour) {
            int ratioX = 500 / width;
            int ratioY = 500 / height;
            if (s.power) {
                drawHalo(buffer, s);
            }
            for (int i = 1; i < s.body.size(); i++) {
                Point prev = s.body.get(i - 1);
                Point curr = s.body.get(i);
                int topLeftX;
                int topLeftY;
                if (prev.x < curr.x) {
                    topLeftX = prev.x;
                    topLeftY = prev.y;
                } else if (prev.x == curr.x) {
                    if (prev.y < curr.y) {
                        topLeftX = prev.x;
                        topLeftY = prev.y;
                    } else {
                        topLeftX = curr.x;
                        topLeftY = curr.y;
                    }
                } else {
                    topLeftX = curr.x;
                    topLeftY = curr.y;
                }
                int lengthX = Math.abs(prev.x - curr.x) + 1;
                int lengthY = Math.abs(prev.y - curr.y) + 1;
                int x1 = topLeftX * ratioX;
                int y1 = topLeftY * ratioY;
                int x2 = lengthX * ratioX;
                int y2 = lengthY * ratioY;

                buffer.setColor(colour);
                buffer.fillRect(x1 + X_OFFSET + 1, y1 + Y_OFFSET + 1, x2 - 1, y2 - 1);
            }
        }

        public void drawDistantShadow(Graphics buffer, SimpleSnake s, Color colour, int offset) {
            int ratioX = 500 / width;
            int ratioY = 500 / height;
            if (s.power) {
                drawHalo(buffer, s);
            }
            for (int i = 1; i < s.body.size(); i++) {
                Point prev = s.body.get(i - 1);
                Point curr = s.body.get(i);
                int topLeftX;
                int topLeftY;
                if (prev.x < curr.x) {
                    topLeftX = prev.x;
                    topLeftY = prev.y;
                } else if (prev.x == curr.x) {
                    if (prev.y < curr.y) {
                        topLeftX = prev.x;
                        topLeftY = prev.y;
                    } else {
                        topLeftX = curr.x;
                        topLeftY = curr.y;
                    }
                } else {
                    topLeftX = curr.x;
                    topLeftY = curr.y;
                }
                int lengthX = Math.abs(prev.x - curr.x) + 1;
                int lengthY = Math.abs(prev.y - curr.y) + 1;
                int x1 = topLeftX * ratioX;
                int y1 = topLeftY * ratioY;
                int x2 = lengthX * ratioX;
                int y2 = lengthY * ratioY;

                buffer.setColor(colour);
                buffer.fillRect(x1 + X_OFFSET + 1 + offset, y1 + Y_OFFSET + 1 + offset, x2 - 1, y2 - 1);
            }
        }

        public void drawHighlight(Graphics buffer, SimpleSnake s, Color colour) {
            int ratioX = 500 / width;
            int ratioY = 500 / height;
            if (s.power) {
                drawHalo(buffer, s);
            }
            for (int i = 1; i < s.body.size(); i++) {
                Point prev = s.body.get(i - 1);
                Point curr = s.body.get(i);
                int topLeftX;
                int topLeftY;
                if (prev.x < curr.x) {
                    topLeftX = prev.x;
                    topLeftY = prev.y;
                } else if (prev.x == curr.x) {
                    if (prev.y < curr.y) {
                        topLeftX = prev.x;
                        topLeftY = prev.y;
                    } else {
                        topLeftX = curr.x;
                        topLeftY = curr.y;
                    }
                } else {
                    topLeftX = curr.x;
                    topLeftY = curr.y;
                }
                int lengthX = Math.abs(prev.x - curr.x) + 1;
                int lengthY = Math.abs(prev.y - curr.y) + 1;
                int x1 = topLeftX * ratioX;
                int y1 = topLeftY * ratioY;
                int x2 = lengthX * ratioX;
                int y2 = lengthY * ratioY;

                buffer.setColor(colour);
                buffer.fillRect(x1 + X_OFFSET, y1 + Y_OFFSET, x2 - 1, y2 - 1);
            }
        }
    }


}

class SimpleSnake implements Comparable<SimpleSnake> {

    ArrayList<Point> body;
    int length;
    int kills;
    int longest;
    boolean alive;
    boolean power;
    String name;
    int number;

    public SimpleSnake(ArrayList<Point> body, String name, int number, int length, int longest, int kills,
                       boolean alive, boolean power) {
        this.body = body;
        if (name.isEmpty()) {
            this.name = "Built-in Agent";
        } else {
            this.name = name;
        }
        this.number = number;
        this.longest = longest;
        this.length = length;
        this.kills = kills;
        this.alive = alive;
        this.power = power;
    }

    @Override
    public int compareTo(SimpleSnake o) {
        // TODO Auto-generated method stub
        return length - o.length;
    }


}

