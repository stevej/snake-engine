package za.ac.wits.snake.client.client;
/**
 * Created by Steve James on 08-Apr-16.
 */
public final class Parser {

    private String initString;
    private String stateString;

    private int mode;
    private int width;
    private int height;
    private int numSnakes;
    private String[] obstacles = new String[0];
    private String[] zombies;
    private String[] apples;
    private int snakeId;
    private String[] snakes;
    private boolean gameOver;
    private boolean gameStarting;


    public Parser() {
        initString = null;
        stateString = null;

        mode = -1;
        width = -1;
        height = -1;
        numSnakes = -1;
        snakeId = -1;
    }


    /*the following method depend on the format of  the strings for the given competition*/
    public void setInitString(String initString) {

        this.initString = null;
        mode = -1;
        width = -1;
        height = -1;
        numSnakes = -1;
        obstacles = null;

        this.initString = initString;
        String[] lines = initString.split("\n");
        String[] gameData = lines[0].split(" ");

        numSnakes = Integer.parseInt(gameData[0]);
        width = Integer.parseInt(gameData[1]);
        height = Integer.parseInt(gameData[2]);
        mode = Integer.parseInt(gameData[3]);

        obstacles = new String[RemoteConfig.getInstance().getObstacles()];
        for (int i = 0; i < obstacles.length; i++) {
            obstacles[i] = lines[i + 1];
        }
    }

    /*the following method depend on the format of  the strings for the given competition*/
    public void setStateString(String stateString) {

        this.stateString = null;
        apples = null;
        snakeId = -1;
        snakes = null;
        gameOver = false;
        gameStarting = false;

        this.stateString = stateString;
        String[] lines = stateString.split("\n");
        if (lines[0].contains("Game Over")) {
            gameOver = true;
        } else if (lines[0].contains("Game Starting")) {
            gameStarting = true;
        } else {
            Config config = RemoteConfig.getInstance();
            int idx = 0;
            apples = new String[config.getApples()];
            for (int i = 0; i < apples.length; i++, idx++) {
                apples[i] = lines[idx];
            }
            zombies = new String[config.getZombies()];
            for (int i = 0; i < zombies.length; i++, idx++) {
                zombies[i] = lines[idx];
            }
            int nSnakes = numSnakes == -1 ? config.getSnakes() : numSnakes;
            if (lines.length - idx == nSnakes + 1) {
                //then snakeId provided
                snakeId = Integer.parseInt(lines[idx++]);
            } else {
                snakeId = -1;
            }
            snakes = new String[nSnakes];
            for (int i = 0; i < snakes.length; i++, idx++) {
                snakes[i] = lines[idx];
            }
        }
    }

    public int getMode() {
        return mode;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getNumSnakes() {
        return numSnakes;
    }

    public String[] getObstacles() {
        return obstacles;
    }

    public String[] getApples() {
        return apples;
    }

    public String[] getZombies() {
        return zombies;
    }

    public int getSnakeId() {
        return snakeId;
    }

    public String[] getSnakes() {
        return snakes;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public boolean isGameStarting() {
        return gameStarting;
    }
}
