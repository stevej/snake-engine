package za.ac.wits.snake.client.client;

import com.google.gwt.core.client.Callback;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.http.client.Response;
import com.google.gwt.json.client.*;
import com.google.gwt.user.client.Timer;

import java.util.Queue;


public class StateGetter extends GetRequest {


    private static final int MIN_BUFFER = 150;


    private static final int[] TIMEOUTS = {0, 200, 300, 400, 750, 1000, 1500, 2000, 5000, 10000};

    private long startState = -1;
    private boolean inProgress = false;
    private int failureCount = 0;
    private long lastRequestTime = 0;
    private int leagueId = 0;
    private int gameId = 0;
    private boolean reset = false;

    public void getNewStates(final Queue<StateHolder> states, int gameNumber) {

        if (SnakeClient.getLeagueId() != leagueId) {
            GWT.log("League changed. Clearing queue");
            states.clear();
            leagueId = SnakeClient.getLeagueId();
            startState = -1;
            failureCount = 0;
        }

        if (gameId != gameNumber) {
            GWT.log("Game changed. Clearing queue");
            states.clear();
            startState = -1;
            gameId = gameNumber;
        }


        if (!inProgress && (states.size() < MIN_BUFFER)) {
            lastRequestTime = System.currentTimeMillis();
            inProgress = true;
            GWT.log("Requesting states from state number " + startState);


            final String url = SnakeClient.buildUrl("games", leagueId, gameNumber, startState);


            Timer t = new Timer() {
                @Override
                public void run() {


                    makeRequest(url, new Callback<Response, Throwable>() {
                        @Override
                        public void onFailure(Throwable reason) {
                            inProgress = false;
                            // Couldn't connect to server (could be timeout,
                            // SOP
                            // violation, etc.)
                            incrementFailure();
                        }

                        @Override
                        public void onSuccess(Response response) {
                            inProgress = false;
                            if (response.getStatusCode() == 200) {
                                try {
                                    JSONObject json = (JSONObject) JSONParser.parseStrict(response.getText());
                                    JSONArray stateArray = (JSONArray) json.get("states");
                                    if (stateArray.size() < MIN_BUFFER) {
                                        incrementFailure();
                                    }
                                    for (int i = 0; i < stateArray.size(); i++) {
                                        JSONObject jo = (JSONObject) stateArray.get(i);
                                        JSONNumber last = (JSONNumber) jo.get("globalIndex");
                                        startState = (long) last.doubleValue();
                                        states.add(StateHolder.get(jo));
                                        failureCount = 0;
                                        reset = false;
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            } else {
                                // Handle the error. Can get the status text
                                // from
                                // response.getStatusText()
                                incrementFailure();
                                return;
                            }
                        }
                    });

                }
            };

            if (failureCount >= TIMEOUTS.length) {
                reset();
            } else {
                if (failureCount > 0) {
                    GWT.log("WAITING " + TIMEOUTS[failureCount]);
                }
                t.schedule(TIMEOUTS[failureCount]);
            }
        }
    }

    private void incrementFailure() {
        failureCount++;
        if (failureCount >= TIMEOUTS.length && reset) {
            startState = -1;
            failureCount = 0;
            UI.getInstance().showError("Nothing to get!<br/>Try refreshing.<br/>If the problem persists, the server may be down :(");
            SnakeClient.setStopRequests(true);
        }
    }

    private void reset() {
        Timer t = new Timer() {
            @Override
            public void run() {
                reset = true;
                startState = -1;
                inProgress = false;
                GWT.log("Problems. Try resetting");
                failureCount = TIMEOUTS.length - 1;
            }
        };
        t.schedule(TIMEOUTS[TIMEOUTS.length - 1]);
    }

    public void refresh() {
        startState = -1;
        failureCount = 0;
        inProgress = false;
        SnakeClient.setStopRequests(false);
    }
}
