package za.ac.wits.snake.client.client;

import com.google.gwt.core.client.Callback;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.http.client.*;
import com.google.gwt.json.client.JSONException;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONString;


/**
 * Created by Steve James on 07-May-16.
 */
public final class RemoteConfig extends Config {


    private static Callback<Void, Throwable> onLoad;


    public static void initOnLoad(Callback<Void, Throwable> onLoad) {
        RemoteConfig.onLoad = onLoad;
    }

    @Override
    public void load() {

        GWT.log("Requesting config");
        String url = SnakeClient.buildUrl("games", SnakeClient.getLeagueId(), "config");
        RequestBuilder builder = new RequestBuilder(RequestBuilder.GET,
                URL.encode(url));

        try {
            builder.sendRequest(null,
                    new RequestCallback() {
                        public void onError(Request request,
                                            Throwable exception) {
                        }

                        public void onResponseReceived(Request request,
                                                       Response response) {
                            if (response.getStatusCode() == 200) {
                                try {
                                    JSONObject body = (JSONObject) JSONParser.parseStrict(response.getText());
                                    String[] config = ((JSONString) body.get("config")).stringValue().split("\n");
                                    for (String line : config) {
                                        line = line.trim();
                                        int idx = line.indexOf('#');
                                        if (idx > -1) {
                                            line = line.substring(0, idx);
                                        }
                                        if (!line.isEmpty()) {
                                            String[] tokens = line.split("\\s+");
                                            if (fromAlias.containsKey(tokens[0])) {
                                                fromAlias.get(tokens[0]).set(tokens[1]);
                                            }

                                        }
                                    }
                                    if (onLoad != null) {
                                        onLoad.onSuccess(null);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    if (onLoad != null) {
                                        onLoad.onFailure(e);
                                    }
                                }

                            } else {
                                // Handle the error. Can get the status text
                                onLoad.onFailure(new Exception("Received error code " + response.getStatusCode()));
                            }
                        }
                    });
        } catch (RequestException e) {
            // Couldn't connect to server
            onLoad.onFailure(e);
        }


    }


    public static Config getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RemoteConfig();
        }
        return INSTANCE;
    }


}
