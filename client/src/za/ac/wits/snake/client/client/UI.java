package za.ac.wits.snake.client.client;

import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Steve James on 29-Apr-17.
 */
public class UI {

    private final String[] LEAGUE_NAMES = {"Open League", "Closed League", "DSA League"};

    private static final UI ui = new UI();

    private Map<String, Widget> widgets = new HashMap<>();


    private static final int CANVAS_HEIGHT = 600;
    private static final int CANVAS_WIDTH = 525;

    private static final String DIV_TAG_ID = "gameCanvas"; // must match div tag in html file


    private UI() {

    }

    public static UI getInstance() {
        return ui;
    }

    private void addWidget(String name, Widget widget) {
        widgets.put(name, widget);
    }

    private <T extends Widget> T getWidget(String name) {
        return (T) widgets.get(name);
    }


    public void loadUI() {
        HorizontalPanel root = new HorizontalPanel();
        addWidget("root", root);

        VerticalPanel left = new VerticalPanel();
        addWidget("left", left);
        VerticalPanel right = new VerticalPanel();
        addWidget("right", right);


        int redrawInterval = RemoteConfig.getInstance().getGameSpeed();
        final GameStateProcessor gsp = new GameStateProcessor(new SnakeGameImpl(0, null));
        GameCanvasSnake board = new GameCanvasSnake(CANVAS_WIDTH, CANVAS_HEIGHT, redrawInterval, gsp);


        Canvas canvas = board.getCanvas();
        addWidget("canvas", canvas);
        left.add(canvas);


        // Add the widgets to the root panel.
        PlayerTable playerTable = new PlayerTable(gsp);
        addWidget("table", playerTable);

        final ListBox leagues = new ListBox();
        widgets.put("leagues", leagues);

        final ListBox divisions = new ListBox();
        divisions.addItem("Division 1");
        divisions.setSelectedIndex(0);
        widgets.put("divisions", divisions);

        leagues.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                SnakeClient.setLeagueId(leagues.getSelectedIndex());
            }
        });

        divisions.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                gsp.setGame(divisions.getSelectedIndex());
            }
        });


        right.add(leagues);
        right.add(divisions);
        right.add(playerTable);

        root.add(left);
        root.add(right);

        RootPanel.get(DIV_TAG_ID).add(root);
    }

    public void setLeague(int league) {
        ListBox leagues = getWidget("leagues");
        leagues.setItemSelected(league, true);
    }

    public void setNumberOfLeagues(int numLeagues) {
        ListBox leagues = getWidget("leagues");
        leagues.clear();
        for (int i = 0; i < numLeagues; i++) {
            if (i >= LEAGUE_NAMES.length) {
                leagues.addItem("League " + (i + 1));
            } else {
                leagues.addItem(LEAGUE_NAMES[i]);
            }
        }
        leagues.setSelectedIndex(0);
        leagues.setVisible(numLeagues > 0);
    }

    public void setDivision(int division) {
        ListBox leagues = getWidget("division");
        leagues.setItemSelected(division, true);
    }

    public void setNumberOfDivisions(int numDivisions) {
        ListBox divisions = getWidget("divisions");
        if (divisions.getItemCount() == numDivisions) {
            return;
        }
        divisions.setVisible(numDivisions > 0);
        int idx = divisions.getSelectedIndex();
        idx = Math.max(0, Math.min(numDivisions - 1, idx));
        divisions.clear();
        for (int i = 0; i < numDivisions; i++) {
            divisions.addItem("Division " + (i));
        }
        divisions.setSelectedIndex(idx);
    }


    public void showError(String html) {
        VerticalPanel left = getWidget("left");

        HTMLPanel error = getWidget("error");
        if (error != null) {
            left.remove(error);
        }

        left.remove(getWidget("canvas"));
        error = new HTMLPanel("h1", html);
        addWidget("error", error);
        left.add(error);
    }

    public void hideError() {
        VerticalPanel left = getWidget("left");
        HTMLPanel error = getWidget("error");
        if (error != null) {
            left.remove(error);
        }
        left.add(getWidget("canvas"));
    }

    public void updateTable() {
        PlayerTable table = getWidget("table");
        table.update();
    }


}
