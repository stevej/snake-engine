package za.ac.wits.snake.client.client;


import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.json.client.JSONException;
import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;

public class Player implements Comparable<Player>{
	static NumberFormat nf =NumberFormat.getFormat("####0.0000");
	public Player(String name, String score, String elo, String division) {
		super();
		this.name = name;
		this.score = score;
		this.elo = elo;
		this.division = division;
	}
	public String name;
	public String score;
	public String elo;
	public String division;
	@Override
	public int compareTo(Player o) {
		// TODO Auto-generated method stub
		return name.compareTo(o.name);
	}
	
	public static Player get(JSONObject jo) throws JSONException {
		JSONString jn = (JSONString)jo.get("name");
		JSONNumber js = (JSONNumber)jo.get("score");
		JSONNumber js2 = (JSONNumber)jo.get("elo");
		JSONNumber js3 = (JSONNumber)jo.get("currentGame");
		return new Player(jn.toString().replaceAll("\"",""),nf.format(Double.parseDouble(js.toString())),
				nf.format(Double.parseDouble(js2.toString())),js3.toString());
		//	return null;
	}
	
	
}
