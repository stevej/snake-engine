package za.ac.wits.snake.client.client;

import com.google.gwt.canvas.dom.client.CanvasGradient;
import com.google.gwt.canvas.dom.client.CanvasPattern;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.canvas.dom.client.Context2d.Repetition;
import com.google.gwt.dom.client.ImageElement;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.RootPanel;

public class Graphics{
	Context2d pen;
	final Image image;
	
	protected Graphics(Context2d pen){
		super();
		this.pen=pen;
		image = new Image("grasstexture.jpg");
		
		//RootPanel.get().add(image);
	}

	public final void fillOval(int i, int j, int ratioX, int ratioY) {
		pen.arc(i+ratioX/2, j+ratioX/2, ratioX/2, 0, Math.PI*2,true);
		pen.closePath();
		pen.fill();
		pen.beginPath();
	}
	
	public final void setAlpha(double alpha){
		pen.setGlobalAlpha(alpha);
	}
	
	public final void setBGPen(){
		CanvasPattern cp = pen.createPattern(ImageElement.as(image.getElement()), Repetition.REPEAT);
		
		pen.setFillStyle(cp);
	}
	
	public final void setSnakeColor(Color c){
	//	pen.setStrokeStyle(c.getColor());
		CanvasGradient cg = pen.createLinearGradient(40, 40, 700, 700);
		cg.addColorStop(0, "black");
		cg.addColorStop(0.1,"white");
		cg.addColorStop(0.15, "black");
		cg.addColorStop(0.2,"white");
		cg.addColorStop(0.25, "black");
		cg.addColorStop(0.3,"white");
		cg.addColorStop(0.35, "black");
		cg.addColorStop(0.4,"white");
		cg.addColorStop(0.45, "black");
		cg.addColorStop(0.5,"white");
		cg.addColorStop(0.55,"black");
		cg.addColorStop(0.6,"white");
		cg.addColorStop(0.65, "black");
		cg.addColorStop(0.7,"white");
		cg.addColorStop(0.75,"black");
		cg.addColorStop(0.8,"white");
		cg.addColorStop(0.85,"black");
		cg.addColorStop(0.9, "white");
		cg.addColorStop(0.95,"black");
		cg.addColorStop(1,"white");
		//pen.setFillStyle(c.getColor());
		pen.setFillStyle(cg);
	}
	
	public final void setColor(Color c){
	//	pen.setStrokeStyle(c.getColor());
		pen.setFillStyle(c.getColor());
	}

	public void fillRect(int x, int y, int w, int h) {
		pen.fillRect(x,y,w,h);
	}
	
	public void drawImage(Image image, int x, int y){
		pen.drawImage(ImageElement.as(image.getElement()), x, y);
	}
	
	public void fill() {
		// TODO Auto-generated method stub
		pen.fill();
	}
	
	public void drawString(String string, int x, int y){
		pen.fillText(string, x, y);
	}
}
