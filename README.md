# README #

This project contains all the code for actually running the games. It makes calls to a webservice running on localhost to retrieve the agents.

The Snake2016-v*.jar files are the standalone apps.

The game-server-master-v* is the master server that runs all the games. It requires Java's JSON library to run, so eecute it from the command line like:

java -cp game-server.jar;javax.json.jar za.ac.wits.snake.GameCoordinator

The game-server-slave-v* is the slave server that the master can schedule games on. It has no yet been tested with everything so rather don't use it unless we really have to.