package za.ac.wits.snake.agent;


import za.ac.wits.snake.Point;

import java.security.SecureRandom;
import java.util.Random;


/**
 * Created by Dean Wookey on 07-Apr-12.
 */
public class EasyAgent extends BuiltInAgent {

    private static final Random random = new SecureRandom();

    public static final int ID = -2;


    public EasyAgent(int agentId, String name) {
        super(agentId, name);
    }

    public EasyAgent(String loadString) {
        super(loadString);
    }


    @Override
    public String toString() {
        return getType() + "#" + super.toString();
    }

    @Override
    protected Point getClosest(SnakeState state, Point head, Point target) {

        //Move randomly 1% of the time
        if (random.nextDouble() >= 0.99) {
            Point[] points = getNeighbours(head);
            return points[random.nextInt(points.length)];
        }
        return super.getClosest(state, head, target);
    }
}
