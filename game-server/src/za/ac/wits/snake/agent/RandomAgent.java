package za.ac.wits.snake.agent;

import za.ac.wits.snake.Direction;

import java.security.SecureRandom;
import java.util.Random;

/**
 * Created by Steve James on 06-Apr-16.
 */
public class RandomAgent extends SnakeAgent {

    public static final int ID = -4;


    private static final Random random = new SecureRandom();


    public RandomAgent(int agentId, String name) {
        super(agentId, name);
    }

    public RandomAgent(String loadString) {
        super(loadString);
    }

    @Override
    public String toString() {
        return getType() + "#" + super.toString();
    }


    @Override
    public Direction getMove() {
        return Direction.fromValue(random.nextInt(4));
    }


}
