package za.ac.wits.snake.agent;


import za.ac.wits.snake.Point;


/**
 * Created by Dean Wookey on 07-Apr-12.
 */
public class VeryEasyAgent extends BuiltInAgent {

    public static final int ID = -3;

    public VeryEasyAgent(int agentId, String name) {
        super(agentId, name);
    }

    public VeryEasyAgent(String loadString) {
        super(loadString);
    }

    @Override
    public String toString() {
        return getType() + "#" + super.toString();
    }

    @Override
    protected Point getClosest(SnakeState state, Point head, Point target) {
        //same as parent, but ignore obstacles
        double min = Double.POSITIVE_INFINITY;
        Point best = null;
        Point[] points = getNeighbours(head);
        for (Point p : points) {
            double d = manhattanDistance(p, target);
            if (d < min) {
                min = d;
                best = p;
            }
        }
        return best;
    }
}
