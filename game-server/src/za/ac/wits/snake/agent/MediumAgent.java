package za.ac.wits.snake.agent;


/**
 * Created by Dean Wookey on 07-Apr-12.
 */
public class MediumAgent extends BuiltInAgent {

    public static final int ID = -1;


    public MediumAgent(int agentId, String name) {
        super(agentId, name);
    }

    public MediumAgent(String loadString) {
        super(loadString);
    }

    @Override
    public String toString() {
        return getType() + "#" + super.toString();
    }

}
