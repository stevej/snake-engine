package za.ac.wits.snake;

import za.ac.wits.snake.agent.Agent;
import za.ac.wits.snake.comms.ExternalServer;
import za.ac.wits.snake.comms.GameState;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.List;

/**
 * Created by Steve James on 07-Apr-16.
 */
class NotifyingSnakeGame extends SnakeGameImpl implements Notifier {

    private Runnable onCompleted;

    private boolean serverAttached;
    private ExternalServer externalServerStub;

    private transient CircularBuffer<GameState> lastStates;

    private final int leagueId;

    public NotifyingSnakeGame(int leagueId, int id, List<Agent> agents) {
        super(id, agents);
        this.leagueId = leagueId;
        lastStates = new CircularBuffer<>(100);
        serverAttached = false;
    }

    @Override
    public String getNextState() {
        GameState state = lastStates.pop();
        return state == null ? null : state.state;
    }

    @Override
    protected void updateState(String state) {
        lastStates.push(new GameState(timeSteps, state));
        if (serverAttached && (status == GameStatus.FINISHING || lastStates.isFilled())) {
            //send to server
            try {
                externalServerStub.addStates(leagueId, gameId, lastStates.toCollection());
            } catch (RemoteException e) {
                //  e.printStackTrace();
            }
            lastStates.clear();
        }
    }


    @Override
    protected void updateEndState() {
        super.updateEndState();
        String endResult = getRankings("Game Over");
        GameState state = new GameState(timeSteps, endResult);
        lastStates.push(state);
        if (serverAttached && (status == GameStatus.FINISHING || lastStates.isFilled())) {
            //send to server
            try {
                //fill queue with repeated Game Over states
                Collection<GameState> collection = lastStates.toCollection();
                for (int i = 0; i < config.getTimeBetweenRounds() / config.getGameSpeed() + 1; i++) {
                    collection.add(new GameState(timeSteps + i + 1, endResult));
                }
                externalServerStub.addStates(leagueId, gameId, collection);
            } catch (RemoteException e) {
                //  e.printStackTrace();
            }
            lastStates.clear();
        }
    }


    @Override
    public final void onCompleted() {
        if (onCompleted != null) {
            onCompleted.run();
        }
    }

    @Override
    public void setOnCompleted(final Runnable onCompleted) {
        this.onCompleted = onCompleted;
    }

    @Override
    public void attachServer(final ExternalServer recorder) {
        externalServerStub = recorder;
        serverAttached = recorder != null;
    }

}

