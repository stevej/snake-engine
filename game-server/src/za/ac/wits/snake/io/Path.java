package za.ac.wits.snake.io;

import java.io.Serializable;

/**
 * Created by Steve James on 15-May-16.
 */
public class Path implements Serializable {

    private static final long serialVersionUID = 42l;

    private final String host;
    private final int port;


    public Path(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }
}
