package za.ac.wits.snake.io;

import za.ac.wits.snake.GameCoordinator;
import za.ac.wits.snake.agent.*;

import javax.json.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;


/**
 * Created by Steve James on 04-Jun-16.
 */
public class AgentDB {

    private static final String USERNAME = "admin";
    private static final String PASSWORD = "weshouldchangethis";

    private final String baseUrl;

    public AgentDB(String baseUrl) {
        if (!baseUrl.endsWith("/")) {
            baseUrl += "/";
        }
        this.baseUrl = baseUrl;
    }


    public boolean update(final List<SnakeAgent> agents) {
        try {
            for (SnakeAgent agent : agents) {

                URL url = new URL(baseUrl + String.format("agents/%d/%d", GameCoordinator.ID, agent.getId()));

                StringJoiner query = new StringJoiner("&");
                query.add("totalKills=" + agent.getKills());
                query.add("weightedScore=" + agent.getWeightedScore());
                query.add("longestLength=" + agent.getLongest());
                query.add("eloRating=" + agent.getEloRating());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();

                String encoded = Base64.getEncoder().encodeToString((USERNAME + ":" + PASSWORD).getBytes());
                conn.setRequestProperty("Authorization", "Basic " + encoded);

                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
                OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
                writer.write(query.toString());
                writer.flush();
                writer.close();

                int responseCode = conn.getResponseCode();
                conn.disconnect();
                if (responseCode != 200 && responseCode != 204) {
                    return false;
                }

            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

    }

    public List<SnakeAgent> getAll() throws IOException {

        List<SnakeAgent> agents = new ArrayList<>(10);
        URL url = new URL(baseUrl + "agents/" + GameCoordinator.ID);

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();


        byte[] auth = (USERNAME + ":" + PASSWORD).getBytes();
        String encoded = Base64.getEncoder().encodeToString(auth);
        conn.setRequestProperty("Authorization", "Basic " + encoded);

        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "application/json");

        if (conn.getResponseCode() != 200) {
            System.err.println(conn.getResponseCode());
            System.err.println(conn.getResponseMessage());
            try (BufferedReader buffer = new BufferedReader(new InputStreamReader(conn.getErrorStream()))) {
                buffer.lines().forEach(System.err::println);
            }
            return Collections.emptyList();
        }
        JsonArray array = Json.createReader(new BufferedReader(new InputStreamReader(conn.getInputStream()))).readArray();
        conn.disconnect();

        for (int i = 0; i < array.size(); i++) {
            JsonObject object = array.getJsonObject(i);
            SnakeAgent agent = createFromJson(object);
            agents.add(agent);
        }
        return agents;
    }


    private double getDouble(JsonObject object, String key) {
        return object.getJsonNumber(key).doubleValue();

    }

    private SnakeAgent createFromJson(JsonObject object) {

        String type = object.getString("agentType");

        if (type.equals("ExternalAgent")) {
            return createExternalFromJson(object);
        }


        StringJoiner sj = new StringJoiner(">");
        String command = sj.add("" + object.getInt("id")).add(object.getString("username")).add("" + object.getInt("totalKills"))
                .add("" + getDouble(object, "weightedScore")).add(object.getInt("longestLength") + "")
                .add("" + getDouble(object, "eloRating")).toString();

        switch (type) {
            case "VeryEasyAgent":
                return new VeryEasyAgent(command);
            case "EasyAgent":
                return new EasyAgent(command);
            case "MediumAgent":
                return new MediumAgent(command);
            case "RandomAgent":
                return new RandomAgent(command);
            default:
                throw new RuntimeException("Corrupted database file. No such agent " + type);
        }
    }

    private ExternalAgent createExternalFromJson(JsonObject object) {

        StringJoiner sj = new StringJoiner(">");
        String details = sj.add("" + object.getInt("id")).add(object.getString("username")).add("" + object.getInt("totalKills"))
                .add("" + getDouble(object, "weightedScore")).add(object.getInt("longestLength") + "")
                .add("" + getDouble(object, "eloRating")).toString();
        StringJoiner s = new StringJoiner("%");
        s.add(object.getString("fileName")).add(object.getString("directory")).add(object.getString("lang")).add(object.getString("command"));
        return ExternalAgent.load(s.toString() + "*" + details, object.getJsonNumber("timestamp").longValue());
    }
}
