package za.ac.wits.snake;

import za.ac.wits.snake.agent.Agent;
import za.ac.wits.snake.agent.SnakeAgent;

import java.util.List;

/**
 * Created by Steve James on 20-Jun-17.
 */

//todo remove code duplication
class NotifyingTronGame extends NotifyingSnakeGame {

    public NotifyingTronGame(int leagueId, int id, List<Agent> agents) {
        super(leagueId, id, agents);
    }

    @Override
    protected boolean isPrematureWin() {
        int nAliveSnakes = 0;
        int id = -1;
        for (Snake snake : snakes) {
            if (snake.isAlive()) {
                ++nAliveSnakes;
                id = snake.getId();
            }
        }
        if (nAliveSnakes == 1) {
            updateFinalFrame(id);
            return true;
        }
        return false;
    }

    protected TronSnake generateRandomSnake(int id) {
        Snake snake = super.generateRandomSnake(id);
        return new TronSnake(snake);
    }

    private void updateFinalFrame(int winnerId) {
        SnakeAgent agent = (SnakeAgent) agents.get(winnerId);
        agent.setLongest(config.getGameWidth() * config.getGameHeight());
        ++timeSteps;
        updateState();
        try {
            Thread.sleep(config.getGameSpeed());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
