package za.ac.wits.snake;

import za.ac.wits.snake.agent.Agent;
import za.ac.wits.snake.agent.SnakeAgent;
import za.ac.wits.snake.config.LocalConfig;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Steve James on 14-May-16.
 * <p>
 * A class that collects and collates the results of the different games. The agents can then be sorted at the end of
 * all the games based on what's been received
 */
public class ResultsManager {

    private static class Position {

        final int division;
        final int position;


        private Position(int division, int position) {
            this.division = division;
            this.position = position;
        }

        public int compareTo(Position other) {
            if (division == other.division) {
                return Integer.compare(position, other.position);
            }
            return Integer.compare(division, other.division);
        }
    }

    private final Map<Integer, Position> map = new HashMap<>();

    /**
     * Clear all the results
     */
    public void clear() {
        map.clear();
    }

    /**
     * Adds a new result to the collection
     *
     * @param agentId  the ID of the agent
     * @param division the division the agent was competing in
     * @param position the position in the division the agent finished in
     */
    public void add(int agentId, int division, int position) {
        synchronized (this) {
            //An agent ID of zero is a BuiltIn agent
            if (agentId != 0) {
                assert !map.containsKey(agentId);
                map.put(agentId, new Position(division, position));
            }
        }
    }

    /**
     * Checks whether there are any results
     *
     * @return whether there is at least one result recorded
     */
    public boolean isEmpty() {
        return map.isEmpty();
    }

    /**
     * Sorts the agents, according to their finishing positions
     *
     * @param agents the agents to sort
     */
    public void sort(final List<SnakeAgent> agents) {

        assert !isEmpty();
        assert map.size() == agents.size();
        Collections.sort(agents, (A, B) -> {
            Position posA = map.get(A.getId());
            Position posB = map.get(B.getId());
            return posA.compareTo(posB);
        });

    }

    /**
     * Adds a set of agents, who have recorded their own scores, to the collection
     *
     * @param agents the agents after the game has completed
     */
    public void add(final List<Agent> agents) {
        List<SnakeAgent> temp = agents.stream().map(agent -> (SnakeAgent) agent).collect(Collectors.toList());
        Collections.sort(temp);
        int i = 0;
        for (SnakeAgent agent : temp) {
            add(agent.getId(), agent.getGameId(), i);
            ++i;
        }
    }

    public void updateEloRatings(List<SnakeAgent> agents) {

        int N = agents.size();
        int agentsPerGame = LocalConfig.getInstance().getSnakes();
        for (int i = 0; i < agents.size() - 1; i++) {
            SnakeAgent A = agents.get(i);
            int league = i / agentsPerGame;
            //  int end = (1 + league) * agentsPerGame;  //within same league and next one
            int end = (0 + league) * agentsPerGame; //within the same league
            for (int j = i + 1; j < N && j < end + agentsPerGame; j++) {
                SnakeAgent B = agents.get(j);
                if (getPosition(A).compareTo(getPosition(B)) == 0) {
                    A.updateElo(0.5, B);
                    B.updateElo(0.5, A);
                } else {
                    A.updateElo(1, B);
                    B.updateElo(0, A);
                }
            }
        }
    }

    private Position getPosition(final SnakeAgent agent) {
        return map.get(agent.getId());
    }


}
