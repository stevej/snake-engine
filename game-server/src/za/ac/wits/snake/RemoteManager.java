package za.ac.wits.snake;

import za.ac.wits.snake.agent.Agent;
import za.ac.wits.snake.agent.SnakeAgent;
import za.ac.wits.snake.comms.Master;
import za.ac.wits.snake.comms.Slave;
import za.ac.wits.snake.config.Config;
import za.ac.wits.snake.config.LocalConfig;
import za.ac.wits.snake.io.Path;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.stream.Collectors;

import static za.ac.wits.snake.GameCoordinator.log;

/**
 * Created by Steve James on 14-May-16.
 * <p>
 * A class that manages the delegation of games to other servers. It has built-in error handling and failover procedures
 * to ensure that this process isn't blocked if any of the other slave servers go down.
 */
public class RemoteManager {

    private CountDownLatch countDownLatch;
    private final Config config;

    private ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(50);
    private final ScheduledFuture<?> slaveScanHandle;

    private Map<Integer, Slave> remainingSlaves;
    private Map<Integer, List<Integer>> agentsByGame;

    private final Master masterServer;
    private final Path externalPath;

    private final int leagueId;

    public RemoteManager(final int leagueId, final Master callback, final Path externalPath) {
        this.leagueId = leagueId;
        masterServer = callback;
        this.externalPath = externalPath;
        config = LocalConfig.getInstance();
        agentsByGame = new HashMap<>();
        remainingSlaves = new HashMap<>();
        slaveScanHandle = Executors.newScheduledThreadPool(1).scheduleAtFixedRate(this::scanSlaves,
                config.getGameDuration(), 10, TimeUnit.SECONDS);
    }

    public void add(final Slave slave, final int gameId, final List<Agent> agents) {
        remainingSlaves.put(gameId, slave);
        List<Integer> agentIds = agents.stream().map(agent -> ((SnakeAgent) agent).getId()).collect(Collectors.toList());
        agentsByGame.put(gameId, agentIds);
    }

    public void start() {
        assert remainingSlaves.size() == agentsByGame.size();
        countDownLatch = new CountDownLatch(remainingSlaves.size());
        for (final int gameId : remainingSlaves.keySet()) {
            executor.execute(() -> {
                Slave slave = remainingSlaves.get(gameId);
                try {
                    log("Executing " + gameId + " remotely");
                    slave.execute(leagueId, gameId, GameCoordinator.DB_PATH, ((LocalConfig) config).getFile(), agentsByGame.get(gameId), externalPath);
                } catch (RemoteException e) {
                    remove(gameId);
                }
            });
        }
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        slaveScanHandle.cancel(true);

    }

    public synchronized void notify(int gameId) {
        assert remainingSlaves.containsKey(gameId);
        remainingSlaves.remove(gameId);
        countDownLatch.countDown();
    }

    private synchronized void scanSlaves() {
        List<Integer> gamesToRemove = new ArrayList<>(remainingSlaves.size());
        for (int gameId : remainingSlaves.keySet()) {
            try {
                remainingSlaves.get(gameId).isAlive();
            } catch (RemoteException e) {
                gamesToRemove.add(gameId);
            }
        }
        gamesToRemove.forEach(this::remove);
    }

    private void remove(int gameId) {
        synchronized (remainingSlaves) {
            assert remainingSlaves.containsKey(gameId);
            try {
                masterServer.putResult(gameId, agentsByGame.get(gameId));
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            remainingSlaves.remove(gameId);
            countDownLatch.countDown();
        }
    }


}
