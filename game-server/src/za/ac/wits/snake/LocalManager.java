package za.ac.wits.snake;

import za.ac.wits.snake.agent.Agent;
import za.ac.wits.snake.comms.ExternalServer;
import za.ac.wits.snake.config.Config;
import za.ac.wits.snake.config.LocalConfig;
import za.ac.wits.snake.io.Path;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;

import static za.ac.wits.snake.GameCoordinator.log;

/**
 * Created by Steve James on 14-May-16.
 * <p>
 * A class that manages the delegation of games to this process
 */
public class LocalManager {

    private CountDownLatch countDownLatch;

    private ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(50);

    private Map<Integer, List<Agent>> agentsByGame;

    private final ResultsManager results;

    private final Path externalPath;

    private final int leagueId;


    public LocalManager(final int leagueId, final ResultsManager results, final Path externalPath) {
        this.leagueId = leagueId;
        this.externalPath = externalPath;
        this.results = results;
        agentsByGame = new HashMap<>();
        //setDaemon(true?)
        executor.setThreadFactory(r -> {
            Thread t = new Thread(r);
            t.setDaemon(true);
            return t;
        });
    }

    public void add(final int gameId, final List<Agent> agents) {
        agentsByGame.put(gameId, agents);
    }

    public void start() {
        countDownLatch = new CountDownLatch(agentsByGame.size());
        for (final int gameId : agentsByGame.keySet()) {


            NotifyingSnakeGame g;
            switch (LocalConfig.getInstance().getGameMode()) {
                case GROW:
                    g =  new NotifyingSnakeGame(leagueId, gameId, agentsByGame.get(gameId));
                    break;
                case SURVIVE:
                    g = new NotifyingTronGame(leagueId, gameId, agentsByGame.get(gameId));
                    break;
                default:
                    throw new UnsupportedOperationException("Mode " + LocalConfig.getInstance().getGameMode() + " not yet implemented");
            }
            final NotifyingSnakeGame game = g;
            game.attachServer(getExternalServer());
            game.setOnCompleted(() -> {
                results.add(game.getAgents());
                log(gameId + ": count down latch decremented");
                countDownLatch.countDown();
            });
            log("Executing game " + gameId);
            log(game.getAgents());
            executor.execute(game);
        }

        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private ExternalServer getExternalServer() {
        if (externalPath != null) {
            try {
                Registry registry = LocateRegistry.getRegistry(externalPath.getHost(), externalPath.getPort());
                return (ExternalServer) registry.lookup(ExternalServer.class.getSimpleName());
            } catch (RemoteException e) {
            } catch (NotBoundException e) {
            }
        }
        return null;
    }

}
