package za.ac.wits.snake;


import za.ac.wits.snake.agent.Agent;
import za.ac.wits.snake.agent.ExternalAgent;
import za.ac.wits.snake.agent.SnakeAgent;
import za.ac.wits.snake.comms.ExternalServer;
import za.ac.wits.snake.comms.Master;
import za.ac.wits.snake.comms.Slave;
import za.ac.wits.snake.config.Config;
import za.ac.wits.snake.config.LocalConfig;
import za.ac.wits.snake.io.AgentDB;
import za.ac.wits.snake.io.Path;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.ExportException;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;


/**
 * This class is responsible for managing all the games/leagues. This includes not only scheduling the games, but
 * also saving state, and updating the agents' scores.
 * <p>
 * Created by Dean Wookey on 07-Apr-12.
 */
public class GameCoordinator extends Thread implements Master {

    //Damn GC tryin' to collect my server!!
    public static Master masterReference;

    public static String DEFAULT_ADDRESS;
    public static int DEFAULT_PORT;


    private RemoteManager remoteManager;
    private LocalManager localManager;

    private List<SnakeAgent> agents = new ArrayList<>();

    private Map<Integer, SnakeAgent> agentIdMap = new HashMap<>();
    private SnakeGame[] games;


    public static final String BASE_PATH = "/snakenfs/";
    public static String DB_PATH;
    public static int ID;

    private boolean stop = false;
    private long startTime = 0;


    private List<Slave> slaves = new ArrayList<>();


    private Config config;

    private Path externalServerPath;
    private ExternalServer externalServer;

    private final ResultsManager results = new ResultsManager();

    private static final boolean LOG = GameCoordinator.class.desiredAssertionStatus();

    protected static void log(String string) {
        if (LOG) {
            System.out.println(string);
        }
    }

    protected static void log(List<Agent> snakes) {
        if (LOG) {
            for (Agent a : snakes) {
                SnakeAgent agent = (SnakeAgent) a;
                System.out.println("\t (" + agent.getId() + "): " + agent.getName());
            }
        }
    }


    protected static void log(Throwable ex) {
        System.out.println(ex);
        if (LOG) {
            ex.printStackTrace(System.out);
        }
    }

    private static void parse(String[] args) {
        String dbHost = "http://lamp.ms.wits.ac.za";
        int dbPort = 8080;
        String extHost = "localhost";
        int extPort = 1099;
        int id = 0;
        int i = 0;
        while (i < args.length) {

            switch (args[i]) {
                case "-db_host":
                    dbHost = args[++i];
                    break;
                case "-db_port":
                    dbPort = Integer.parseInt(args[++i]);
                    break;
                case "-ext_host":
                    extHost = args[++i];
                    break;
                case "-ext_port":
                    extPort = Integer.parseInt(args[++i]);
                    break;
                case "-ID":
                    id = Integer.parseInt(args[++i]);
                    break;
                default:
                    throw new RuntimeException("Invalid parameter " + args[i]);
            }
            i++;
        }
        DEFAULT_ADDRESS = extHost;
        DEFAULT_PORT = extPort;
        DB_PATH = dbHost + ":" + dbPort + "/SnakeDBService/";
        ID = id;
    }

    public static void main(String[] args) {

        parse(args);


        GameCoordinator coordinator = GameCoordinator.getInstance();
        try {

            try {
                LocateRegistry.createRegistry(DEFAULT_PORT);
            } catch (ExportException e) {
                //registry already exists. Oh well.
            }
            masterReference = (Master) UnicastRemoteObject.exportObject(coordinator, 0);
            // Bind the remote object's stub in the registry
            Registry registry = LocateRegistry.getRegistry();
            registry.rebind(GameCoordinator.class.getSimpleName(), masterReference);
            log("Server ready");

            coordinator.setExternalServer(new Path(DEFAULT_ADDRESS, DEFAULT_PORT));

            coordinator.start();
        } catch (Exception e) {
            log("Server exception: " + e.toString());
            log(e);
        }

    }

    @Override
    public void register(Slave slave) {
        synchronized (slaves) {
            slaves.add(slave);
        }
        log("Registered slave " + (slaves.size() - 1));
    }


    private static class LazyHolder {
        private static final GameCoordinator INSTANCE = new GameCoordinator();
    }

    private GameCoordinator() {
        config = LocalConfig.getInstance();
    }

    public static GameCoordinator getInstance() {
        return LazyHolder.INSTANCE;
    }

    private void addAll(final List<SnakeAgent> dbAgents) {

        //DB is empty -> populate
        if (agents.isEmpty()) {
            dbAgents.forEach(agent -> {
                agentIdMap.put(agent.getId(), agent);
                agents.add(agent);
            });
            return;
        }


        List<ExternalAgent> newAgents = new ArrayList<>(dbAgents.size());

        Set<Integer> ids = dbAgents.stream()
                .map(SnakeAgent::getId)
                .collect(Collectors.toSet());
        final Iterator<SnakeAgent> it = agents.iterator();
        while (it.hasNext()) {
            SnakeAgent agent = it.next();
            if (!ids.contains(agent.getId())) {
                //agent was deleted
                it.remove();
                agentIdMap.remove(agent.getId());
            }
        }

        for (SnakeAgent agent : dbAgents) {
            if (!agentIdMap.containsKey(agent.getId())) {
                //new agent
                newAgents.add((ExternalAgent) agent);
                agentIdMap.put(agent.getId(), agent);
            } else if (agent instanceof ExternalAgent) {
                ExternalAgent newAgent = (ExternalAgent) agent;
                ExternalAgent current = (ExternalAgent) agentIdMap.get(agent.getId());
                if (current.getTimestamp() < newAgent.getTimestamp()) {
                    //agent was updated
                    newAgents.add(newAgent);
                    agentIdMap.put(newAgent.getId(), newAgent);
                    agents.remove(current);
                }
            }
        }

        //sort new agents by when they were added
        Collections.sort(newAgents, (o1, o2) -> Long.compare(o1.getTimestamp(), o2.getTimestamp()));
        agents.addAll(newAgents);

    }


    private void cleanSlaves() {
        //close all game servers that have died
        log("Cleaning slaves");
        synchronized (slaves) {
            slaves = slaves.stream().filter(i -> {
                try {
                    return i.isAlive();
                } catch (RemoteException e) {
                    return false;
                }
            }).collect(Collectors.toList());
        }
    }

    private void loadAgents() {
        log("Loading game data");
        AgentDB db = new AgentDB(DB_PATH);
        try {
            List<SnakeAgent> snakes = db.getAll();
            addAll(snakes);
        } catch (Exception e) {
            log(e);
        }

        List<String> strings = agents.stream()
                .collect(Collectors.toList())
                .stream()
                .sorted((a, b) -> Double.compare(b.getWeightedScore(), a.getWeightedScore()))
                .map(agent -> agent.getId() + " " + agent.getName() + " " + agent.getWeightedScore() + " " + agent.getEloRating())
                .collect(Collectors.toList());
        send(() -> externalServer.setAgents(ID, strings));
    }

    @Override
    public void run() {
        try {
            do {
                cleanSlaves();

                loadAgents();

                startTime = System.currentTimeMillis();
                executeRound();
                //sort the into their final positions within their leagues
                results.sort(agents);
                //update the elo based on these positions
                results.updateEloRatings(agents);
                //do promotion/relegation
                reorderAgents();
                //update the weighted score
                scoreAgents();
                //save the data

                AgentDB db = new AgentDB(DB_PATH);
                if (!db.update(agents)) {
                    log("UNABLE TO SAVE AGENTS!!");
                }

                log("Sleeping between rounds");
                try {
                    Thread.sleep(config.getTimeBetweenRounds());
                } catch (InterruptedException ex) {
                    Logger.getLogger(GameCoordinator.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            while (!stop);
        } catch (Exception e) {
            log(e);
        } finally {
            shutdown();
        }
    }


    private void shutdown() {
        for (Slave slave : slaves) {
            try {
                slave.shutdown();
            } catch (RemoteException e) {
            }
        }
        try {
            Registry registry = LocateRegistry.getRegistry();
            registry.unbind(GameCoordinator.class.getSimpleName());
            UnicastRemoteObject.unexportObject(GameCoordinator.this, true);
        } catch (RemoteException | NotBoundException e) {
        }
        System.exit(1);
    }

    public int getNumGames(int numAgents) {
        int agentsPerGame = config.getSnakes();
        int numGames = numAgents / agentsPerGame;
        if (numAgents % agentsPerGame > 0) {
            numGames++;
        }
        return numGames;
    }

    public int getNumAgents() {
        return agents == null ? 0 : agents.size();
    }

    public void executeRound() {
        log("Starting round");
        synchronized (slaves) {
            log("There are " + slaves.size() + " slaves");
            if (slaves.isEmpty()) {
                executeLocally();
            } else {
                executeRemotely();
            }
        }
    }

    private void executeLocally() {
        results.clear();
        localManager = new LocalManager(ID, results, externalServerPath);
        int agentsPerGame = config.getSnakes();
        int numAgents = getNumAgents();
        int numGames = getNumGames(numAgents);
        send(() -> externalServer.setNumGames(ID, numGames));
        log(numGames + " to run");
        games = new SnakeGame[numGames];
        int agentPos = 0;
        for (int gameId = 0; gameId < numGames; gameId++) {
            List<Agent> players = new ArrayList<>(agentsPerGame);
            //add 4 players to a game. If there aren't 4 left, then add as many as possible.
            for (int j = 0; j < agentsPerGame && (agentPos < numAgents); j++) {
                players.add(agents.get(agentPos));
                agentPos++;
            }
            final int j = gameId;
            send(() -> externalServer.setGameDetails(ID, j, config.getGameDuration(), config.getGameWidth(), config.getGameHeight(),
                    players.stream().map(agent -> ((SnakeAgent) agent).getId()).collect(Collectors.toList())));
            localManager.add(gameId, players);
        }
        localManager.start();
    }

    private void executeRemotely() {
        results.clear();
        remoteManager = new RemoteManager(ID, this, externalServerPath);
        int agentsPerGame = config.getSnakes();
        int numAgents = getNumAgents();
        int numGames = getNumGames(numAgents);
        send(() -> externalServer.setNumGames(ID, numGames));
        log(numGames + " to run");
        games = new SnakeGame[numGames];
        int agentPos = 0;
        for (int gameId = 0; gameId < numGames; gameId++) {
            List<Agent> players = new ArrayList<>(agentsPerGame);
            //add 4 players to a game. If there aren't 4 left, then add as many as possible.
            for (int j = 0; j < agentsPerGame && (agentPos < numAgents); j++) {
                players.add(agents.get(agentPos));
                agentPos++;
            }
            final int j = gameId;
            send(() -> externalServer.setGameDetails(ID, j, config.getGameDuration(), config.getGameWidth(), config.getGameHeight(),
                    players.stream().map(agent -> ((SnakeAgent) agent).getId()).collect(Collectors.toList())));

            SnakeGame game;
            switch (LocalConfig.getInstance().getGameMode()) {
                case GROW:
                    game =  new NotifyingSnakeGame(ID, gameId, players);
                    break;
                case SURVIVE:
                    game = new NotifyingTronGame(ID, gameId, players);
                    break;
                default:
                    throw new UnsupportedOperationException("Mode " + LocalConfig.getInstance().getGameMode() + " not yet implemented");
            }
            games[gameId] = game;
            int slaveId = gameId % slaves.size();
            final Slave server = slaves.get(slaveId);
            remoteManager.add(server, gameId, players);
        }
        remoteManager.start();
    }

    @Override
    public void putResult(int gameId, List<Integer> results) throws RemoteException {
        for (int k = 0; k < results.size(); ++k) {
            this.results.add(results.get(k), gameId, k);
        }
        log("Completed game " + gameId + " on slave");
        remoteManager.notify(gameId);
    }

    @Override
    public void setExternalServer(Path path) throws RemoteException {
        externalServerPath = path;
        try {
            log("Setting ext server...");
            Registry registry = LocateRegistry.getRegistry(externalServerPath.getHost(), externalServerPath.getPort());
            externalServer = (ExternalServer) registry.lookup(ExternalServer.class.getSimpleName());
            send(() -> externalServer.setConfig(ID, LocalConfig.getInstance().toString()));
            log("Got external server " + path.getHost() + ":" + path.getPort());
        } catch (RemoteException | NotBoundException e) {
            log(e);
            externalServer = null;
        }
    }


    public void reorderAgents() {
        int agentsPerGame = LocalConfig.getInstance().getSnakes();
        //this sorts within each game, keeping the overall ordering of games in tact
        for (int i = agentsPerGame - 1; i < agents.size() - 1; i = i + agentsPerGame) {
            SnakeAgent loser = agents.get(i);
            agents.remove(i);
            agents.add(i + 1, loser);
        }
    }

    public void scoreAgents() {
        if (agents != null) {
            int score = agents.size() - 1;
            for (int i = 0; i < agents.size(); i++) {
                SnakeAgent a = agents.get(i);
                if (a != null) {
                    agents.get(i).incrementScore(score);
                }
                score--;
            }
        }
    }

    private void send(CheckedRunnable call) {
        if (externalServer == null) {
            return;
        }
        synchronized (externalServer) {
            try {
                call.run();
            } catch (Exception e) {
                log(e);
            }
        }
    }
}


@FunctionalInterface
interface CheckedRunnable {
    void run() throws Exception;
}