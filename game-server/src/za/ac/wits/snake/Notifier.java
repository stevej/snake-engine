package za.ac.wits.snake;

import za.ac.wits.snake.comms.ExternalServer;

/**
 * Created by Steve James on 21-Jun-16.
 */
public interface Notifier {

    void onCompleted();

    void setOnCompleted(final Runnable onCompleted);

    void attachServer(final ExternalServer recorder);


}
