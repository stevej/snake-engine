package za.ac.wits.snake;

import com.sun.istack.internal.Nullable;
import za.ac.wits.snake.agent.Agent;
import za.ac.wits.snake.agent.SnakeAgent;
import za.ac.wits.snake.comms.Master;
import za.ac.wits.snake.comms.Slave;
import za.ac.wits.snake.comms.ExternalServer;
import za.ac.wits.snake.config.LocalConfig;
import za.ac.wits.snake.io.AgentDB;
import za.ac.wits.snake.io.Path;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/**
 * Created by Steve James on 11-May-16.
 */
public class GameWorker implements Slave, Serializable {

    //Damn GC tryin' to collect my server!!
    private static Slave slaveReference;

    private static final long serialVersionUID = 42l;

    private static final String REGISTRY = GameCoordinator.class.getSimpleName();

    public static final int MAX_THREADS = 10;

    private final Master master;

    private ExternalServer recorder;

    private transient CountDownLatch close;

    private final transient ScheduledExecutorService periodicScheduler = Executors.newScheduledThreadPool(1);

    private final transient ExecutorService threadPool = Executors.newFixedThreadPool(MAX_THREADS);

    private static final boolean LOG = true;

    private static void log(String string) {
        if (LOG) {
            System.out.println(string);
        }
    }

    public GameWorker(Master master) {
        this.master = master;
        close = new CountDownLatch(1);
        periodicScheduler.scheduleAtFixedRate(() -> {
            try {
                master.isAlive();
            } catch (RemoteException e) {
                exit();
            }
        }, 20, 20, TimeUnit.SECONDS);

    }

    public static void main(String[] args) {

        Registry registry;
        try {
            switch (args.length) {
                case 0:
                    registry = LocateRegistry.getRegistry(GameCoordinator.DEFAULT_ADDRESS, GameCoordinator.DEFAULT_PORT);
                    break;
                case 1:
                    registry = LocateRegistry.getRegistry(args[0], GameCoordinator.DEFAULT_PORT);
                    break;
                case 2:
                default:
                    registry = LocateRegistry.getRegistry(args[0], Integer.parseInt(args[1]));
                    break;
            }
            Master stub = (Master) registry.lookup(REGISTRY);
            GameWorker runner = new GameWorker(stub);
            slaveReference = (Slave) UnicastRemoteObject.exportObject(runner, 0);
            stub.register(runner);
            runner.await();
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }

    }

    private void await() {
        try {
            close.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void execute(final int leagueId, final int gameId, String pathToDB, File configFile, List<Integer> agentIds, @Nullable final Path serverPath) throws RemoteException {

        if (serverPath != null) {
            try {
                Registry registry = LocateRegistry.getRegistry(serverPath.getHost(), serverPath.getPort());
                recorder = (ExternalServer) registry.lookup(ExternalServer.class.getSimpleName());
            } catch (NotBoundException | RemoteException e) {
            }
        }

        threadPool.execute(() -> {
            try {
                log("Told to execute");
                AgentDB db = new AgentDB(pathToDB);
                NotifyingSnakeGame game = buildGame(leagueId, gameId, db, configFile, agentIds);
                game.attachServer(recorder);
                log("Game Built. Running...");
                game.run();
                log("Game finished");
                master.isAlive();
                List<Agent> agents = game.getAgents();
                master.putResult(gameId, agents.stream().map(agent ->
                        (SnakeAgent) agent)
                        .sorted()
                        .map(agent -> agent.getId())
                        .collect(Collectors.toList()));
            } catch (RuntimeException e) {
                close.countDown();
            } catch (IOException e) {
                close.countDown();
            }
        });


    }

    @Override
    public void shutdown() throws RemoteException {
        close.countDown();
    }

    @Override
    public boolean isAlive() throws RemoteException {
        return true;
    }

    private void exit() {
        close.countDown();
        //force exit
        System.exit(1);
    }

    private synchronized NotifyingSnakeGame buildGame(int leagueId, int gameId, AgentDB saveFile, File configFile, List<Integer> agentIds) throws IOException {
        if (configFile != null && configFile.isFile()) {
            ((LocalConfig) LocalConfig.getInstance()).load(configFile);
        }

        List<SnakeAgent> agents = saveFile.getAll();
        List<Agent> res = agents.stream().filter(agent -> agentIds.contains(agent.getId())).collect(Collectors.toList());
        switch (LocalConfig.getInstance().getGameMode()) {
            case GROW:
                return new NotifyingSnakeGame(leagueId, gameId, res);
            case SURVIVE:
                return new NotifyingTronGame(leagueId, gameId, res);
            default:
                throw new UnsupportedOperationException("Mode " + LocalConfig.getInstance().getGameMode() + " not yet implemented");
        }
    }

}
