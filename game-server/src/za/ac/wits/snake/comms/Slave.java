package za.ac.wits.snake.comms;

import com.sun.istack.internal.Nullable;
import za.ac.wits.snake.io.Path;

import java.io.File;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 * Created by Steve James on 28-Apr-17.
 */
public interface Slave extends Remote {

    void execute(int leagueId, int gameId, String pathToDB, File configFile, List<Integer> agentIds, @Nullable Path serverPath) throws RemoteException;

    void shutdown() throws RemoteException;

    boolean isAlive() throws RemoteException;
}
