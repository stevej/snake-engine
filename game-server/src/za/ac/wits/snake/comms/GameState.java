package za.ac.wits.snake.comms;

import java.io.Serializable;

/**
 * Created by Steve James on 07-Apr-16.
 * <p>
 * A class that represents the message that is passed from the game servers to the collecting server.
 */
public class GameState implements Serializable {

    private static final long serialVersionUID = 42l;

    public final long index;
    public final String state;
    public final long timeCreated;

    private static final long EXPIRATION_PERIOD = 30 * 1000;

    /**
     * Creates a new game state
     *
     * @param index the index of the game state - usually the timestep
     * @param state a string representing the game at that particular time.
     */
    public GameState(long index, String state, long timeCreated) {
        this.index = index;
        this.state = state;
        this.timeCreated = timeCreated;
    }

    /**
     * Creates a new game state
     *
     * @param index the index of the game state - usually the timestep
     * @param state a string representing the game at that particular time.
     */
    public GameState(long index, String state) {
        this(index, state, System.currentTimeMillis());
    }

    public boolean hasExpired() {
        return System.currentTimeMillis() - timeCreated > EXPIRATION_PERIOD;
    }
}
