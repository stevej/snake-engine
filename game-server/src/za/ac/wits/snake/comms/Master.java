package za.ac.wits.snake.comms;

import za.ac.wits.snake.io.Path;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 * Created by Steve James on 11-May-16.
 * <p>
 * An interface for a master process. Slaves register with the master, which then farms out activities to the slaves
 */
public interface Master extends Remote {

    /**
     * Register a slave with the master
     *
     * @param slave the slave to register
     * @throws RemoteException
     */
    void register(Slave slave) throws RemoteException;

    /**
     * Whether the master is alive
     *
     * @return if the master is running
     * @throws RemoteException
     */
    boolean isAlive() throws RemoteException;

    /**
     * Sets the result of a game
     *
     * @param gameId  the game ID
     * @param results the IDs of the agents, in the order they finished
     */
    void putResult(int gameId, List<Integer> results) throws RemoteException;

    /**
     * Sets the path to the external RMI server.
     *
     * @param path the IP address and port of the external server that interfaces with users.
     * @throws RemoteException
     */
    void setExternalServer(final Path path) throws RemoteException;
}
