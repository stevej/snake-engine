package za.ac.wits.snake.comms;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.List;

/**
 * Created by Steve on 11-May-16.
 * <p>
 * An interface for a server that records states of the game. The games will send their states to this server, which can
 * then be queried by users. This isolates the instances that actually run the games from the load that will occur when
 * multiple connections are made requesting data.
 */
public interface ExternalServer extends Remote {

    /**
     * Takes in a set of game states.
     *
     * @param gameId the game ID that the states belong to
     * @param states the actual states
     * @throws RemoteException
     */
    void addStates(int leagueId, int gameId, final Collection<GameState> states) throws RemoteException;

    void setNumGames(int leagueId, int numGames) throws RemoteException;

    void setAgents(int leagueId, final List<String> agents) throws RemoteException;

    void setGameDetails(int leagueId, int gameId, int gameDuration, int width, int height, final List<Integer> players) throws RemoteException;

    void setConfig(int leagueId, String config) throws RemoteException;

}
