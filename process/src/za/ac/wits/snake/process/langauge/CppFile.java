/**
 * Copyright (c) 2016 Steven James
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package za.ac.wits.snake.process.langauge;

import java.io.IOException;

/**
 * A file representing a single C++ file
 *
 * @author Steve James <SD.James@outlook.com>
 */
public class CppFile extends CompilableFile {

    public CppFile(String userId, String directory) {
        super(userId, directory, Language.CPP);
    }

    @Override
    public String[] getExecCommand() {
        return new String[]{getExecutablePath()};
    }

    @Override
    protected void compile(String sourceCodePath, String objectFilePath) throws IOException {
        ProcessBuilder pb = new ProcessBuilder("g++",
                "-std=c++11",
                "-pedantic",
                "-O2",
                sourceCodePath,
                "-o",
                objectFilePath);
        Process process = pb.start();
        try {
            int exitCode = process.waitFor();
            if (exitCode != 0) {
                throw new IOException("Error when compiling. Exit code " + exitCode);
            }
        } catch (InterruptedException ex) {
            throw new IOException(ex);
        }
    }

}
