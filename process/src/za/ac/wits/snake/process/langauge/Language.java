/**
 * Copyright (c) 2016 Steven James
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
package za.ac.wits.snake.process.langauge;

/**
 *
 * @author Steve James {@literal <SD.James@outlook.com>}
 */
public enum Language {

    NONE(""),
    JAVA("jar"),
    PYTHON("py"),
    CPP("cpp", "o");

    private final String extension;
    private final String compiledExtension;

    Language(final String extension) {
        this.extension = extension;
        this.compiledExtension = extension;
    }

    Language(final String extension, final String compiledExtension) {
        this.extension = extension;
        this.compiledExtension = compiledExtension;
    }

    public String getExtension() {
        return extension;
    }

    public String getCompiledExtension() {
        return compiledExtension;
    }

    public static Language get(String extension) {
        for (Language language : values()) {
            if (language.getExtension().equals(extension)) {
                return language;
            }
        }
        return NONE;
    }

}
