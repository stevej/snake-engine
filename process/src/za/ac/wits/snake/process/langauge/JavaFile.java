/**
 * Copyright (c) 2016 Steven James
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package za.ac.wits.snake.process.langauge;

import java.io.File;
import java.io.IOException;
import java.util.StringJoiner;
import java.util.jar.JarFile;

/**
 * A file representing a JAR file
 *
 * @author Steve James {@literal <SD.James@outlook.com>}
 */
public class JavaFile extends ProgramFile {

    /**
     * Creates a new Java file
     *
     * @param userId    the user's ID
     * @param directory the directory of the file
     */
    public JavaFile(String userId, String directory) {
        super(userId, directory, Language.JAVA);
    }

    @Override
    protected void save(String filepath, byte[] source) throws IOException {
        //we encode Java in Base64, so must decode first
        super.save(filepath, decode(source));
    }

    @Override
    public String[] getExecCommand() {
        try {
            JarFile jarFile = new JarFile(new File(getPath()));
            String classPath = jarFile.getManifest().getMainAttributes().getValue("Class-Path");

            if (classPath != null && !classPath.isEmpty()) {
                String mainClass = jarFile.getManifest().getMainAttributes().getValue("Main-Class");

                StringJoiner sj = new StringJoiner(File.pathSeparator);
                sj.add(getPath());
                File dir = new File(directory);
                sj.add(new File(dir.getParentFile(), "lib/Snake2017.jar").getAbsolutePath());
                sj.add(dir.getAbsolutePath());


                return new String[]{
                        "java",
                        "-cp",
                        sj.toString(),
                        mainClass};
            }
        } catch (IOException e) {

        }
        return new String[]{
                "java",
                "-jar",
                getPath()};
    }

}
