/**
 * Copyright (c) 2016 Steven James
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
package za.ac.wits.snake.process.langauge;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;

/**
 * A base class for all programming language files
 *
 * @author Steve James {@literal <SD.James@outlook.com>}
 */
public abstract class ProgramFile {

    //saveDir, language, userId, extension
    protected static final String PATH = "%s/%s_%s.%s";

    protected final String userId;
    protected final String directory;
    protected final Language language;
    protected String filepath;

    /**
     * Base-64 decode an array
     *
     * @param bytes the array of bytes
     * @return the decoded array
     */
    public static byte[] decode(final byte[] bytes) {
        return Base64.getDecoder().decode(bytes);
    }

    /**
     * Base-64 encode an array
     *
     * @param bytes the array of bytes
     * @return the encoded array
     */
    public static byte[] encode(final byte[] bytes) {
        return Base64.getEncoder().encode(bytes);
    }

    /**
     * Creates a new file file
     *
     * @param userId the user's ID
     * @param directory the directory of the file
     * @param source the source code
     * @param language the language
     * @return the program file for the given source and language
     * @throws java.io.IOException the exception thrown if the source code could
     * not be saved
     */
    public static ProgramFile create(String userId, String directory, byte[] source, Language language) throws IOException {
        ProgramFile file;
        switch (language) {
            case JAVA:
                file = new JavaFile(userId, directory);
                break;
            case PYTHON:
                file = new PythonFile(userId, directory);
                break;
            case CPP:
                file = new CppFile(userId, directory);
                break;
            default:
                throw new IllegalArgumentException("Need to specify a language");
        }

        String filepath = String.format(PATH, directory, language.name().toLowerCase(), userId, language.getExtension());
        file.save(filepath, source);
        return file;
    }

    /**
     * Creates a new program file
     *
     * @param userId the user's ID
     * @param directory the directory of the file
     * @param language the language
     */
    protected ProgramFile(String userId, String directory, Language language) {
        this.userId = userId;
        this.directory = directory;
        this.language = language;
    }

    /**
     * Saves the source to file
     *
     * @param filepath the path to the file
     * @param source the source code
     * @throws java.io.IOException the exception thrown if the source code could
     * not be saved
     */
    protected void save(String filepath, byte[] source) throws IOException {
        Path path = Paths.get(filepath);
        Files.write(path, source);
        this.filepath = filepath;
    }

    /**
     * Get the path of the file that is to be executed
     *
     * @return the file path
     */
    public String getPath() {
        return filepath;
    }

    /**
     * Get a list of commands that execute the given file through a command-line
     * interface
     *
     * @return the executing commands
     */
    public abstract String[] getExecCommand();

}
