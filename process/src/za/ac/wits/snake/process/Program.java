/**
 * Copyright (c) 2016 Steven James
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package za.ac.wits.snake.process;

import org.apache.commons.io.input.ReversedLinesFileReader;

import java.io.*;
import java.lang.ProcessBuilder.Redirect;
import java.nio.charset.Charset;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * An abstraction of a program that is run and whose I/O streams are monitored.
 *
 * @author Steve James {@literal <SD.James@outlook.com>}
 */
public class Program implements Runnable {

    private Process process;
    private final String[] execString;

    private boolean running = false;
    private ProgramReader reader;
    private ProgramWriter writer;

    private final File errFile;
    private final File logFile;

    private long startTime = -1L;
    private long totalTime = -1L;

    /**
     * Creates a new program
     *
     * @param execString the command to start the program
     * @param outLog     the path to the logging file
     * @param errLog     the path to the error log
     */
    public Program(String[] execString, String outLog, String errLog) {
        this.logFile = new File(outLog);
        this.errFile = new File(errLog);
        this.execString = execString;
    }

    @Override
    public void run() {
        startTime = -1L;
        totalTime = -1L;
        running = false;

        ProcessBuilder pb = new ProcessBuilder(execString);
        pb.redirectError(Redirect.appendTo(errFile));
        pb.redirectOutput(Redirect.PIPE);
        try {
            process = pb.start();

            reader = new ProgramReader(new BufferedReader(new InputStreamReader(process.getInputStream())), logFile);
            writer = new ProgramWriter(new BufferedWriter(new OutputStreamWriter(process.getOutputStream())), 1000);

            reader.start();
            writer.start();

        } catch (IOException ex) {
            running = false;
            close();
            return;
        }
        running = true;
        startTime = System.currentTimeMillis();
        try {
            process.waitFor();
        } catch (InterruptedException ex) {
            process.destroy();
        }
        totalTime = (System.currentTimeMillis() - startTime);
        running = false;
        close();
    }

    /**
     * Checks if the program is running
     *
     * @return if the program is running
     */
    public boolean isRunning() {
        if (!running || process == null) {
            return false;
        }
        try {
            process.exitValue();
        } catch (IllegalThreadStateException e) {
            return true;
        }
        return false;
    }

    /**
     * Shuts down the program and all associated resources
     */
    public synchronized void close() {
        consumeIfNotNull(Process::destroy, process);
        consumeIfNotNull(ProgramReader::close, reader);
        consumeIfNotNull(ProgramWriter::close, writer);
        //keep only the last 700 lines of the log files
        trim(errFile, 700);
        trim(logFile, 700);
    }

    /**
     * Trim a file to the last N lines only
     *
     * @param file     the file
     * @param numLines the number of last lines to keep
     */
    private void trim(File file, int numLines) {
        StringBuilder sb = new StringBuilder();
        try (ReversedLinesFileReader fr = new ReversedLinesFileReader(file, Charset.defaultCharset())) {
            String line;
            int i = 0;
            while ((line = fr.readLine()) != null && i++ < numLines) {
                sb.append(line).append(System.lineSeparator());
            }
        } catch (IOException e) {
        }
        String content = sb.reverse().toString();
        if (!content.isEmpty()) {
            try (FileWriter fw = new FileWriter(file)) {
                fw.write(content);
            } catch (IOException e) {
            }
        }
    }

    /**
     * Get the time the program has been running
     *
     * @return the uptime of the program, in milliseconds
     */
    public long getUptime() {
        if (totalTime != -1L) {
            return totalTime;
        }
        if (startTime != -1L) {
            return System.currentTimeMillis() - startTime;
        }
        return -1L;
    }

    /**
     * Provide input to the program
     *
     * @param input the input string
     */
    public void provideInput(String input) {
        input = input.replace("\r", "");
        if (running) {
            writer.write(input);
        }
    }

    /**
     * Close the program's input stream
     */
    public void endInput() {
        if (running) {
            writer.close();
        }
    }

    /**
     * Get the output of the program
     *
     * @return the last line printed by the program
     */
    public String getOutput() {
        return returnIfNotNull(ProgramReader::getLastLine, reader, "");
    }

    /**
     * Helper method that executes if the object is not null
     *
     * @param <T>      the type of object
     * @param consumer the method to execute
     * @param object   the object that the consumer is applied to, if it is not null
     */
    static <T> void consumeIfNotNull(Consumer<T> consumer, T object) {
        if (object != null) {
            consumer.accept(object);
        }
    }

    /**
     * Helper method that executes if the object is not null
     *
     * @param <T>          the type of object
     * @param <R>          the return type
     * @param function     the method to execute
     * @param object       the object that the function is applied to, if it is not null
     * @param defaultValue the value returned if the object is null
     * @return the result of the function, or {@code defaultValue} if the object is null
     */
    static <T, R> R returnIfNotNull(Function<T, R> function, T object, R defaultValue) {
        if (object != null) {
            return function.apply(object);
        }
        return defaultValue;
    }

}
