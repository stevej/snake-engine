/**
 * Copyright (c) 2016 Steven James
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
package za.ac.wits.snake.process;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.logging.Level;
import java.util.logging.Logger;
import static za.ac.wits.snake.process.Program.consumeIfNotNull;

/**
 * A thread that writes to the program
 *
 * @author Steve James {@literal <SD.James@outlook.com>}
 */
public class ProgramWriter extends Thread {

    private final BufferedWriter writer;
    private final Deque<String> buffer;
    private final int maxBuffer;

    private int bufferSize = 0;
    private boolean quit = false;

    /**
     * Creates a new writer
     *
     * @param writer the writer that pipes to the program's input stream
     * @param maxBuffer the maximum buffer size
     */
    public ProgramWriter(BufferedWriter writer, int maxBuffer) {
        this.writer = writer;
        buffer = new ArrayDeque<>();
        this.maxBuffer = maxBuffer;
    }

    @Override
    public void run() {
        while (!quit) {
            if (bufferSize > 0) {
                String string = buffer.poll();
                bufferSize -= string.length();
                try {
                    writer.write(string);
                    writer.flush();
                } catch (IOException ex) {
                    System.err.println("Couldn't write to agent: "  + ex.getMessage());
                    interrupt();
                    quit = true;
                }
            }
            try {
                Thread.sleep(5L);
            } catch (InterruptedException ex) {

            }
        }
    }

    /**
     * Write a string to the program
     * @param string the string to send
     */
    public void write(String string) {
        buffer.add(string);
        bufferSize += string.length();
        if (buffer.size() > maxBuffer) {
            bufferSize -= buffer.poll().length();
        }
    }

    /**
     * Shuts down the writer and all associated resources
     */
    public void close() {
        quit = true;
        consumeIfNotNull((bufferedWriter) -> {
            try {
                bufferedWriter.close();
            } catch (IOException ex) {
                //Logger.getLogger(ProgramWriter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }, writer);
        interrupt();
    }

}
