/**
 * Copyright (c) 2016 Steven James
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
package za.ac.wits.snake.process;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A thread that reads from the program
 *
 * @author Steve James {@literal <SD.James@outlook.com>}
 */
public class ProgramReader extends Thread {

    private BufferedReader reader;
    private boolean alive = false;
    private char[] chars;
    private StringBuilder accum;
    private int lineCapacity = 10000;

    private boolean logSeparator = false;
    private int maxFileBytes = 2000000;
    private int currentlyWritten = 0;
    private boolean loggingEnabled = false;
    private boolean closed = false;

    private String currOutput = "";
    private BufferedWriter logFileWriter;

    /**
     * Creates a new reader
     *
     * @param reader the reader that reads from the program's output stream
     * @param logFile the file to log to
     */
    public ProgramReader(BufferedReader reader, File logFile) {
        chars = new char[lineCapacity];
        accum = new StringBuilder(lineCapacity);
        this.reader = reader;
        setDaemon(true);

        if (logFile == null) {
            loggingEnabled = false;
            return;
        }
        loggingEnabled = true;
        if (logFile.exists() && logFile.length() > maxFileBytes) {
            logFile.delete();
        }
        try {
            logFileWriter = new BufferedWriter(new FileWriter(logFile, true));
        } catch (IOException ex) {
            loggingEnabled = false;
        }
    }

    @Override
    public void run() {
        alive = true;
        while (alive) {
            if (!doRead()) {
                break;
            }
        }
        close();
    }

    private boolean doRead() {
        if (!alive) {
            return false;
        }

        try {
            if (reader.ready()) {
                String line = "";
                int numChars = reader.read(chars);
                if (numChars == -1) {
                    close();
                    return false;
                }
                for (int i = 0; i < numChars; i++) {
                    if ((chars[i] != '\n') /*&& (chars[i] != '\r')*/) {
                        accum.append(chars[i]);
                    } else {
                        line = line + accum.toString() + chars[i];
                        accum.setLength(0);
                        addOutput(line);
                        line = "";
                    }
                    if (accum.capacity() == 0) {
                        addLog("You exceeded the line capacity of " + lineCapacity + " characters with: " + accum.toString());
                        line = line + accum.toString();
                        accum.setLength(0);
                        addOutput(line);
                        line = "";

                    }
                }
            }
        } catch (IOException ex) {
            return false;
        }
        return true;
    }

    private void addOutput(String line) {
        if (loggingEnabled && line.length() > 4 && line.substring(0, 3).equals("log")) {
            addLog(line.substring(4));
        } else {
            currOutput = line;
        }
    }

    /**
     * Get the output of the program
     *
     * @return the last line printed by the program
     */
    public String getLastLine() {
        return currOutput;
    }

    private void addLog(String logLine) {
        if (!loggingEnabled) {
            return;
        }
        currentlyWritten += logLine.getBytes().length;
        if (currentlyWritten > maxFileBytes) {
            return;
        }
        try {
            if (!logSeparator) {
                logFileWriter.write("==============================Start of Game==============================\r\n");
                logFileWriter.write("These lines are only displayed if the log was written to during the game.\r\n");
                logSeparator = true;
            }
            logFileWriter.write(logLine);
        } catch (IOException ex) {
            Logger.getLogger(ProgramReader.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Shuts down the reader and all associated resources
     */
    public synchronized void close() {
        try {
            if (closed) {
                return;
            }
            closed = true;
            alive = false;

            logFileWriter.write("===============================End of Game===============================\r\n");
            if (currentlyWritten > maxFileBytes) {
                logFileWriter.write("\r\n\r\n Output Too Long: truncated");
            }
            logFileWriter.close();
            reader.close();

        } catch (IOException ex) {
            Logger.getLogger(ProgramReader.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

}
